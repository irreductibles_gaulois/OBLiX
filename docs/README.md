# OBLiX Documentation



## Principle ##

The documentation works thanks to [Doxygen](https://www.doxygen.nl/index.html) + [Breathe](https://breathe.readthedocs.io/en/latest/) + [Sphinx](https://www.sphinx-doc.org/en/master/).

Doxygen is used to read the documentation for the code source directly. Then, for Sphinx to interpret this documentation, a module called Breathe is used.

The overall workflow is given in the following graph:

```mermaid
graph TB

  XML --> SubGraphBreath
  SubGraphBreath --> SubGraphSphinx
  subgraph "Breathe"
  SubGraphBreath[XML to Sphinx]
  end
  
  subgraph "Doxygen"
  SourceCode(Documented code) --> GenerateXML[XML Generator]
  GenerateXML --> XML(XML documentation)
  end

  

  subgraph "Sphinx"
  SphinxRSTFiles(RST Documentation for Sphinx) --> SubGraphSphinx
  SubGraphSphinx[Generate HTML files]
  end
  SubGraphSphinx --> HTMLDoc(HTML documentation)	
```


## Building the documentation ##

### Requirements ###

To build the documentation, you need the following: 

- python3 and pip
- Doxygen
- Breathe
- Sphinx
- furo


### Run CMake ###

When running the `cmake` command, make sure that the option `BUILD_DOCUMENTATION` is set to `YES`.
You can check this with `cmake-gui` or with the following command:

```sh
mkdir build
cd build
cmake -DOBLiX_BUILD_DOCUMENTATION=ON ..
```

Then, in your `build` folder, you will find a `docs` folder showing that the documentation is ready to be built.

### Run make ###

In order to build the documentation, inside your `build` folder, you can run `make`. This will build all your project.

To **specifically** build the documentation, run inside the `build` folder:

```sh
make documentation
```

### Documentation ###

Your documentation must now lies in `build/docs/html`. You can open the `build/docs/html/index.html` in your browser.

## Adding new documentation ##

1. To make documentation for your code, youj ust need to put it directly into your source files.
2. Make sure that the part of the file will appear somewhere in `docs/source/xxx.rst` with the [Breathe](https://breathe.readthedocs.io/en/latest/) syntax.
3. You can also customize the documentation and add additionnal pages thanks to [Sphinx](https://www.sphinx-doc.org/en/master/).

Please refer to the documentations of Doxygen, Sphinx and Breathe.




