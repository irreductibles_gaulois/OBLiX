==========
Quickstart
==========

Priority Paradigm
-----------------

1. Modularity: Can be used as a module or include modules
2. Flexibility:
3. Rapidity: Computational efficiency, CPU/GPU scalability
4. Simplicity:
5. Generality:
