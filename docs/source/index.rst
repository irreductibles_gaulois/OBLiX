.. OBLiX documentation master file, created by
   sphinx-quickstart on Tue Apr 12 14:28:14 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

=====
OBLiX
=====

OBLiX stands for **Object Library eXecutor**. 

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :hidden:

   self
   quickstart
   build
   
.. toctree::
   :maxdepth: 2
   :caption: Source code
   :hidden:

   code

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
