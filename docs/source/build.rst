========
Building
========

Targets
-------

.. note::
   Availability of some targets depends on availability certain executables (e.g. clang-format for format target)
   :code:`make help` to list all targets available.
