# First, generate XML with Doxygen
find_package(Doxygen REQUIRED)

set(DOXYGEN_GENERATE_HTML NO)
set(DOXYGEN_GENERATE_XML YES)

doxygen_add_docs(
    doxygen
    "${PROJECT_SOURCE_DIR}/src;${PROJECT_SOURCE_DIR}/include"
    COMMENT "Generate documentation"
)

# Now build sphinx with Doxygen files
find_package(Sphinx REQUIRED)

set(SPHINX_SOURCE ${CMAKE_CURRENT_SOURCE_DIR}/source)
set(SPHINX_BUILD ${CMAKE_CURRENT_BINARY_DIR}/html)

add_custom_target(sphinx ALL
                  COMMAND
                  ${SPHINX_EXECUTABLE} -b html
		  # Tell Breathe where to find the Doxygen output
                  -Dbreathe_projects.OBLiX=${CMAKE_CURRENT_BINARY_DIR}/xml
                  ${SPHINX_SOURCE} ${SPHINX_BUILD}
                  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
                  COMMENT "Generating documentation with Sphinx")
add_dependencies(sphinx doxygen)

# Add the `documentation` target
add_custom_target(documentation)
add_dependencies(documentation sphinx)
