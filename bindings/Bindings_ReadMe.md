# How To: Create bindings with SWIG.

## Step 1 and 2: python builds wrappers (with swig) and shared library (with gcc)
   python setup.py build_ext -i

## Step 1 alternative: swig creates wrappers
	swig -c++ -python -I../include -v py_OBLiX.i

## Step 2 alternative: gcc builds a shared library
   g++ -shared -fPIC ./py_OBLiX_wrap.cxx ./OBLiX.cpp -I/home/toto/miniconda3/envs/OBLiX/include/python3.7m/ -o _py_OBLiX.so -lstdc++
