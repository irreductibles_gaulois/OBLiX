#!/usr/bin/env python3

"""
setup.py file for SWIG OBLiX interface.

python setup.py build_ext -i
"""

# from distutils.core import setup, Extension
from setuptools import setup, Extension


baseRun_mod = Extension('_py_OBLiX',
			sources=['py_OBLiX.i'], # 'file.cpp',
			swig_opts=['-c++', '-I../include', '-I../external/includes_standalones',
					 '-py3'],
			include_dirs = ['../external/includes_standalones', '../include'],
			extra_compile_args= ['-shared', '-std=c++17'],
			)

setup (name = 'OBLiX',
		version = '0.1',
		author      = "Romain NOËL",
		description = """Simple swig example from docs""",
		ext_modules = [baseRun_mod],
		py_modules = ["OBLiX"],
		)

# EoF
