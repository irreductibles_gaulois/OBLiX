// File : interface.i

%module py_OBLiX // name of the python module that will be generated.

%{ // Supplementary preprocessor declarations.
   #include <cereal/cereal.hpp>
   #include <cereal/archives/json.hpp>
   #include <cereal/archives/xml.hpp>
   #include <cereal/types/optional.hpp>
   #include <cereal/types/vector.hpp>
   #include "cxxopts.hpp" // prompt command option parser
   #include <spdlog/spdlog.h> // SpdLog
   #include "spdlog/fmt/ostr.h" // must be included to log object

   #include "OBLiX/shareLog.hpp" // spdlog
   #include "OBLiX/usefulTools.hpp"
   #include "OBLiX/version.hpp"
   #include "OBLiX/mapInOut.hpp"
   #include "OBLiX/baseX.hpp"
   #include "OBLiX/OBLiX.hpp"

   #include "example_module.hpp"

   void redef_map_factory(){
      // clean map factory
      std::cout << "redef_map_factory in" << std::endl;
      OBLiX::get_factory().clear_all_builders();

      // Repopulated your mapFactory.
      OBLiX::get_factory().register_builder("BaseRun", OBLiX::unique_creator<OBLiX::BaseX>);
      OBLiX::get_factory().register_builder("Circle", OBLiX::unique_creator<ClassCircle>);
      OBLiX::get_factory().register_builder("Continue", OBLiX::unique_creator<OBLiX::ContinueBRC>);
      OBLiX::get_factory().register_builder("Rectangle", OBLiX::unique_creator<ClassRectangle>);

      OBLiX::get_factory().print_builders();
      return;
   };
%}

// List of function, variables that will be included in the python module.
// 

// 
%include <std_string.i>
%include <std_map.i>
%include <std_vector.i>
%include <std_shared_ptr.i>
#define __attribute__(unused)

// Include the header
%ignore OBLiX::DataInOut::type;  //
%ignore OBLiX::MapInOutClass::get_type_index;  //
%include "OBLiX/mapInOut.hpp"

//  Warning this is a home-made swig recipe.
// %shared_ptr(OBLiX::BaseX);
%include "std_unique_ptr.i"
wrap_unique_ptr(BaseXUniquePtr, OBLiX::BaseX);
// %nodefaultdtor OBLiX::BaseX;
// %unique_ptr(OBLiX::BaseX);
%ignore OBLiX::mapResultName2Object; 
// %template(map_string_BRC) std::map<std::string, std::unique_ptr<BaseX>>;
// %template(map_string_BRC) std::map<std::string, BaseXUniquePtr>;

// 
%include "OBLiX/baseX.hpp"

// 
%ignore OBLiX::kRegisteredBaseRun;
%ignore OBLiX::kRegisteredContinue;
%shared_ptr(spdlog::logger);
%include "OBLiX/OBLiX.hpp"

#include "example_module.hpp"
void redef_map_factory(){ };
