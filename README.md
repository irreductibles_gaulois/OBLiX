# OBLiX
OBLiX (Object Library eXecutor)

Current version: 1.0.1.2

<!-- BADGES -->
<p align="center">
  <a href="https://gitlab.com/irreductibles_gaulois/OBLiX/-/commits/master"><img src="https://gitlab.com/irreductibles_gaulois/OBLiX/badges/master/coverage.svg"/></a>
  <a href="https://gitlab.com/irreductibles_gaulois/OBLiX/-/commits/master"><img src="https://gitlab.com/irreductibles_gaulois/OBLiX/badges/master/pipeline.svg"/></a>
  <a href="https://gitlab.com/irreductibles_gaulois/OBLiX/-/blob/master/LICENSE.txt"><img src="https://img.shields.io/badge/License-BSD_3--Clause-blue.svg"/></a>
  <a href="https://irreductibles_gaulois.gitlab.io/OBLiX/"><img src="https://img.shields.io/badge/Documentation-online-green"/></a>
  <a href="https://gitlab.com/irreductibles_gaulois/OBLiX/-/blob/master/Authors.md"><img src="https://www.repostatus.org/badges/latest/active.svg"/></a>
</p>
<!-- [![coverage report](https://gitlab.com/irreductibles_gaulois/OBLiX/badges/master/coverage.svg)](https://gitlab.com/irreductibles_gaulois/OBLiX/-/commits/master)
-->


## What is OBLiX ?

OBLiX is simple C++ framework library allowing both code coupling and generic execution from input file (in different format: xml, json and maybe other in the future) or CLI (command line interface).
Thus, OBLiX is design to guarantee complex code (like those in numerical simulations) reproducibility in heterogenous situations.

### Code coupling
To enable generic code coupling 

### Generic execution from input file

### Reproducibility

### Priority Paradigm
   1. Modularity: Can be used as a module or include modules
   2. Flexibility: 
   3. Rapidity: Computational efficiency, CPU/GPU scalability
   4. Simplicity:  
   5. Generality: 


## Quick start


## How to install from master folder
    mkdir ./build
    cd ./build
    cmake ../
    make install

### Targets

*Note:* Availability of some targets depends on availability certain executables (e.g. clang-format for *format* target)
`make help` to list all targets available.

* Build
    * *all* (the default if no target is provided)
    * *clean*
    * *install* - install binaries into *CMAKE_INSTALL_PREFIX*
    * *OBLiXprog* - build OBLiX main binary executable file
    * *OBLiXprog-run* - build, install and run OBLiXprog binary (for your convenience)
    * *OBLiXprog-gdb* - build, install and run OBLiXprog binary in gdb (for your convenience)
    * *run* - alias for OBLiX-run (in order to keep it short)
* Testing
    * *testall* - run whole test suite (see test/CMakeLists.txt)
    * *integration* - build and run integration tests only (see test/CMakeLists.txt)
    * *perf* - run whole test suite (see test/CMakeLists.txt), but more verbose
    * *unit* - build and run unit tests only (see test/CMakeLists.txt)
    * *coverage* - call coverage test
    * *gdb* - run target executable with gdb (does not work from ninja, needs gdb)
* Releases
    * *package* - builds packages (zip,deb,rpm,windows installer depending on OS)
* Static analysis
    * *format* - run clang-format on all source files
    * *tidy* - run clang static analysis on all sources
    * *sanitize* - call sanitizer and check for leak memory
    * *cppcheck* - call cppcheck on all files (another static analysis)
* Miscellaneous
    * *doc* - build documentation (if doxygen is available)
        * docs can be found in `build_dir/doc/doc/index.html`
        * if you want them to be a part of install package, uncomment section at the end of `doc/CMakeLists.txt`
* External
    * *update* - update all external sources/projects

./OBLiX_UnitTestAll --gtest_filter=FooTest.* Runs everything in test suite FooTest 

### CMAKE variables

* `-DCMAKE_INSTALL_PREFIX` - location for installation
* `-DVERSION_HOST` - build machine name, see version.h.in
* `-DCMAKE_BUILD_TYPE` - for build type (release, debug, fast, static, fastatic)
* `-DMEASURE_ALL=ON/OFF` - Measure time of all compilations, best used with single threaded build. Needs `time` command (unix).
* `-DUSE_GOLD_LINKER=ON/OFF` - whether to link with Gold linker.

## Features
- Documentation with Doxygen and Sphinx (with theme Breathe)
- Unit testing with GTest, integration testing and performance test cases
- Cross-platforms installation Docker testing: Debian, RedHat, ARM, Windows, MacOS (.?[iu]n[ui]x = linux or unix )


## Tests

For detailed instructions on how to compile and run the tests, please refer to the [README](./testing/README.md) in the testing directory.

## Documentation

You can refer to the online documentation at [this link](https://irreductibles_gaulois.gitlab.io/OBLiX/)

For detailed instruction and information on how to generate the documentation, please refer to the [README](./docs/README.md) in the docs directory.

