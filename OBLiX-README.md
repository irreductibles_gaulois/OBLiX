# OBLiX
OBLiX (Object Library eXecutor)

<!-- DO NOT ERASED IT ! the following line is used by Performance TestCase database. -->
Current version: 1.0.1.2


## What is OBLiX ?
    OBLiX is simple C++ framework library allowing both code coupling and generic execution from input file (in different format: xml, json and maybe other in the future) or CLI (command line interface).
    Thus, OBLiX is design to guarantee complex code (like those in numerical simulations) reproducibility in heterogenous situations.


## How to install from master folder
   mkdir ./build
   cd ./build
   cmake ../
   make all
   make install
 

## License 
    BSD-3