## Litte CMake macro to modify the compile_command.json to make it compatible 
##   with Cland-tidy.

# Get the path of the building directory
# get_filename_component(PARENT_DIR ${CMAKE_BINARY_DIR} DIRECTORY)
# Dump the content of the file in a buffer string.
FILE(READ ${CMAKE_CURRENT_BINARY_DIR}/compile_commands.json BUFFER)

# string(REGEX REPLACE <match-regex> <replace-expr> <out-var> <input>...)
# replace -I/home/.../external/... by -isystem/home/.../external/...
#   this is to avoid clang to look in these folder for analysis. 
string(REGEX REPLACE "([ \t\r\n]-I)([^ \t\r\n]*[/\\]external[^ \t\r\n]*[ \t\r\n])" " -isystem\\2 " NEWBUFFER "${BUFFER}")

# remove option -fprofile which is unknown by clang.
string(REGEX REPLACE "[ \t\r\n]-fprofile-abs-path[ \t\r\n]" "" BUFFER "${NEWBUFFER}")

# Write out the modified buffer.
file(WRITE ${CMAKE_CURRENT_BINARY_DIR}/compile_commands.json "${BUFFER}")
