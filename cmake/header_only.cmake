#-------------------------------------------------------------------------------
# PROJECT executable setup

# Setup main executable to create.
add_library(${PROJECT_LIB_NAME}_header_only INTERFACE
	 ${ICON_RESOURCE}
	 ${CMAKE_BINARY_DIR}/include/OBLiX/version.hpp
	)

# add the binary tree to the search path for include files.
target_include_directories(${PROJECT_LIB_NAME}_header_only INTERFACE
	$<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/include>
	$<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>
)
target_include_directories(${PROJECT_LIB_NAME}_header_only INTERFACE
	$<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/external/includes_standalones>
	$<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>
)
target_include_directories(${PROJECT_LIB_NAME}_header_only INTERFACE
	$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
	$<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>
)


#-------------------------------------------------------------------------------
# Compilation Options

# Enable C++17 for this part of the project
target_compile_features(${PROJECT_LIB_NAME}_header_only INTERFACE cxx_std_17)

if ( CMAKE_BUILD_TYPE STREQUAL "Debug" )
	 if(MSVC)
		  target_compile_options(${PROJECT_LIB_NAME}_header_only INTERFACE -W3 -WX)
	 else()
		  target_compile_options(${PROJECT_LIB_NAME}_header_only INTERFACE -Wall -Wextra -Wshadow -Wunused -pedantic)
	 endif()
endif()

if(APPLE)
	 set_target_properties(${PROJECT_LIB_NAME}_header_only PROPERTIES MACOSX_BUNDLE_BUNDLE_NAME "${PROJECT_LIB_NAME}_header_only")
	 set_target_properties(${PROJECT_LIB_NAME}_header_only PROPERTIES MACOSX_BUNDLE_BUNDLE_GUI_IDENTIFIER "com.${PROJECT_LIB_NAME}_header_only.${PROJECT_LIB_NAME}_header_only")
	 set_target_properties(${PROJECT_LIB_NAME}_header_only PROPERTIES MACOSX_BUNDLE_ICON_FILE ${PROJECT_LIB_NAME}_header_only.icns)
	 set_target_properties(${PROJECT_LIB_NAME}_header_only PROPERTIES MACOSX_BUNDLE_INFO_PLIST ${PROJECT_SOURCE_DIR}/packaging/MacOSXBundleInfo.plist.in)
	 set_source_files_properties(${PROJECT_SOURCE_DIR}/packaging/${PROJECT_LIB_NAME}_header_only.icns PROPERTIES MACOSX_PACKAGE_LOCATION "Resources")
endif()


#------------------------------------------------------------------------------
# Libraries dependencies

# Thread
# find_package (Threads REQUIRED) #threading
# target_link_libraries(${PROJECT_LIB_NAME}_header_only INTERFACE Threads::Threads)

# SpdLog
# target_link_libraries( ${PROJECT_LIB_NAME}_header_only spdlog::spdlog_header_only ) 

# # CxxOpts
# target_link_libraries( ${PROJECT_LIB_NAME}_header_only cxxopts )

# # RapidJSON
# target_link_libraries( ${PROJECT_LIB_NAME}_header_only rapidjson )

# # RapidXML
# target_link_libraries( ${PROJECT_LIB_NAME}_header_only rapidxml )

# # Cereal
# target_link_libraries( ${PROJECT_LIB_NAME}_header_only cereal )


#------------------------------------------------------------------------------
# Configure files

target_include_directories(${PROJECT_LIB_NAME}_header_only INTERFACE 
	$<BUILD_INTERFACE:${CMAKE_BINARY_DIR}/include>
	$<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>
)



#------------------------------------------------------------------------------
## Add subdirectories
# add_subdirectory(labodither)
# target_link_libraries(${PROJECT_LIB_NAME}_header_only dithering_lib)


#------------------------------------------------------------------------------
# Debug/Sanitize macros

# Using macro from cmake/debugTools.cmake included on the top
# Provides OBLiXprog-run and OBLiXprog-dbg
include(debugTools)
addRunAndDebugTargets(${PROJECT_LIB_NAME}_header_only)


#------------------------------------------------------------------------------
# Define installation rules

# 
add_library( ${PROJECT_LIB_NAME}::${PROJECT_LIB_NAME}_header_only ALIAS ${PROJECT_LIB_NAME}_header_only )

# Provides install directory variables as defined by the GNU Coding Standards.
include(GNUInstallDirs)

# Copy header files (as it is a header only target).
install( DIRECTORY ${PROJECT_SOURCE_DIR}/include/ 
	DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}" 
	PATTERN "example_module.hpp" EXCLUDE
	)
install( DIRECTORY ${CMAKE_BINARY_DIR}/include/ DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}" )
install( DIRECTORY ${PROJECT_SOURCE_DIR}/external/includes_standalones/ DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}" )


# 
install(TARGETS ${PROJECT_LIB_NAME}_header_only
	EXPORT ${PROJECT_NAME}_Targets
	BUNDLE DESTINATION "."
	ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
	LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
	RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
)


# Copy MINGW needed libraries for building on windows
if(MINGW)
	 message(STATUS "MinGW detected")
	 get_filename_component(GCC_PATH ${CMAKE_C_COMPILER} PATH)
	 if(${GCC_PATH} MATCHES "mingw64/bin")
		  set(libgcc "libgcc_s_seh-1.dll") #64bit
		  # ----------------------------
		  # Solution to compile with Windows, we don't really know why but it works
		  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O1") #we can compile with -O1, -O2 and -O3. We can also add -flto
		  set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -std=c++17")
		  message(STATUS "  64bit dlls is building")
	 else()
		  set(libgcc "libgcc_s_dw2-1.dll") #32bit
		  message(STATUS "  32bit dlls is building")
	 endif()

	 install(FILES ${GCC_PATH}/${libgcc}
		  ${GCC_PATH}/libwinpthread-1.dll
		  ${GCC_PATH}/libstdc++-6.dll
		  DESTINATION ./bin/
	 )
endif(MINGW)

  

# EoF