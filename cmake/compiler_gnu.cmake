# GCC compiler options
option(GCC_PROFILING "Add options to profile the code" OFF)


if(USE_GNU_COMPILER)

   set(GCC_BASE_C_FLAGS "-Wno-unknown-pragmas")
   set(GCC_BASE_CXX_FLAGS "-O3 -Wno-unknown-pragmas")
   set(GCC_OPTION_C_FLAGS "")
   set(GCC_OPTION_CXX_FLAGS "")
   set(GCC_INTERACTIVE_C_FLAGS "" CACHE STRING "C flags to add to the gcc compiler")
   set(GCC_INTERACTIVE_CXX_FLAGS "" CACHE STRING "C++ flags to add to the g++ compiler")
   set(GCC_FINAL_C_FLAGS "")
   set(GCC_FINAL_CXX_FLAGS "")

   if(GCC_PROFILING)
      set(GCC_OPTION_C_FLAGS "${GCC_OPTION_C_FLAGS} -g -pg -fno-omit-frame-pointer")
      set(GCC_OPTION_CXX_FLAGS "${GCC_OPTION_CXX_FLAGS} -g -pg -fno-omit-frame-pointer")
   endif()

   set(GCC_FINAL_C_FLAGS "${GCC_BASE_C_FLAGS} ${GCC_OPTION_C_FLAGS} ${GCC_INTERACTIVE_C_FLAGS}")
   set(GCC_FINAL_CXX_FLAGS "${GCC_BASE_CXX_FLAGS} ${GCC_OPTION_CXX_FLAGS} ${GCC_INTERACTIVE_CXX_FLAGS}")

   set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${GCC_FINAL_C_FLAGS}")
   set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${GCC_FINAL_CXX_FLAGS}")
endif()


