#
# Project settings
#

  # include(CMakeDependentOption)
  # # cmake_dependent_option(<variable> "<help_text>" <default> <conditions> <fallback>)
  # cmake_dependent_option(BP_BUILD_TESTS
  #   # By default we want tests if CTest is enabled
  #   "Enable ${PROJECT_NAME} project tests targets" ON
  #   # But if BUILD_TESTING is not true, set BP_BUILD_TESTS to OFF
  #   "BUILD_TESTING" OFF
  # )

  option(${PROJECT_NAME}_BUILD_EXECUTABLE "Build the project as an executable, rather than a library." ${${PROJECT_NAME}_MASTER_PROJECT})
  option(${PROJECT_NAME}_BUILD_HEADERS_ONLY "Build the project as a header-only library." ON)
  option(${PROJECT_NAME}_BUILD_STATIC "Build the project as a static library." OFF)
  option(${PROJECT_NAME}_BUILD_SHARED "Build the project as a shared library." OFF)
  
 
#
# Compiler options
#
  # option(${PROJECT_NAME}_WARNINGS_AS_ERRORS "Treat compiler warnings as errors." OFF)
  option(${PROJECT_NAME}_SWIG_BINDING "Build a swig binding from the project." OFF)


#
# Unit testing
#
  # Currently supporting: GoogleTest.
  option(${PROJECT_NAME}_TESTING "Enable tests for the projects (from the `testing` subfolder)." OFF)

  # Deactivate by default the installation of Gtest.
  option(INSTALL_GTEST "Install Googletest's GTest?" OFF)
  option(INSTALL_GMOCK "Install Googletest's GMock?" OFF)
  option(${PROJECT_NAME}_USE_GTEST "Use the GoogleTest project for creating unit tests." ON)
  # option(${PROJECT_NAME}_USE_GOOGLE_MOCK "Use the GoogleMock project for extending the unit tests." OFF)
  option(${PROJECT_NAME}_CODE_COVERAGE "Enable code coverage through GCC." OFF)


# Static analyzers
#
  # Currently supporting: Clang-Tidy, Cppcheck.
  option(${PROJECT_NAME}_LINTER "Enable static analysis with Clang-Tidy, Clang-format and Pylint." OFF)
  # option(RUN_TIDY "internal option to use clang_tidy" OFF)
  option(${PROJECT_NAME}_CPPCHECK "Enable static analysis with Cppcheck." OFF)
  option(${PROJECT_NAME}_SANITIZER "Enable static analysis with Sanitizer." OFF)


#
# Documention
#
  option(${PROJECT_NAME}_BUILD_DOCUMENTATION "Build the associated documentation" OFF)


#
# Miscelanious options
#

  # Be nice to visual studio
  set_property(GLOBAL PROPERTY USE_FOLDERS ON)

  # Be nice and export compile commands by default, this is handy for clang-tidy
  # and for other tools.
  set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
  

  # Use gold linker to speed up linking time, see cmake/useGoldLinker.cmake
  option(${PROJECT_NAME}_USE_GOLDLINKER "Wheather the gold linker should be used" ON)
  if (${PROJECT_NAME}_USE_GOLDLINKER)
    include(useGoldLinker)
  endif()

  # Helpful option enable build profiling to identify slowly compiling files
  option(${PROJECT_NAME}_MEASURE_ALL "When enabled all commands will be passed through time command" OFF)
  if(${PROJECT_NAME}_MEASURE_ALL)
      set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE "time")
  endif()

  # Set default install location to dist folder in build dir
  # we do not want to install to /usr by default
  if (CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
      set (CMAKE_INSTALL_PREFIX "${CMAKE_BINARY_DIR}/dist" CACHE PATH
          "Install path prefix, prepended onto install directories." FORCE )
  endif()

  # Export all symbols when building a shared library
  if(${PROJECT_NAME}_BUILD_STATIC)
    set(CMAKE_WINDOWS_EXPORT_ALL_SYMBOLS OFF)
    set(CMAKE_CXX_VISIBILITY_PRESET hidden)
    set(CMAKE_VISIBILITY_INLINES_HIDDEN 1)
  endif()

  option(${PROJECT_NAME}_ENABLE_LTO "Enable Interprocedural Optimization, aka Link Time Optimization (LTO)." OFF)
  if(${PROJECT_NAME}_ENABLE_LTO)
    include(CheckIPOSupported)
    check_ipo_supported(RESULT result OUTPUT output)
    if(result)
      set(CMAKE_INTERPROCEDURAL_OPTIMIZATION TRUE)
    else()
      message(SEND_ERROR "IPO is not supported: ${output}.")
    endif()
  endif()


  option(${PROJECT_NAME}_ENABLE_CCACHE "Enable the usage of Ccache, in order to speed up rebuild times." ON)
  find_program(CCACHE_FOUND ccache)
  if(CCACHE_FOUND)
    set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE ccache)
    set_property(GLOBAL PROPERTY RULE_LAUNCH_LINK ccache)
  endif()


#
# External lib option
#

  # # Compulsory libs
  # option(${PROJECT_NAME}_SPDLOG "Use the spdlog library (PATH/Prebin/download)" ON)
  # option(${PROJECT_NAME}_SPDLOG "Use the spdlog library (PATH/Prebin/download)" ON)

  # # Optional libs
  # option(${PROJECT_NAME}_USE_SPDLOG "Use the spdlog library (PATH/Prebin/download)" ON)
  # option(${PROJECT_NAME}_USE_SPDLOG "Use the spdlog library (PATH/Prebin/download)" ON)
  # option(${PROJECT_NAME}_USE_SPDLOG "Use the spdlog library (PATH/Prebin/download)" ON)


#
# Package managers
#
  # Currently supporting: Conan, Vcpkg.
  # option(${PROJECT_NAME}_ENABLE_CONAN "Enable the Conan package manager for this project." OFF)
  # option(${PROJECT_NAME}_ENABLE_VCPKG "Enable the Vcpkg package manager for this project." OFF)

  set(CPACK_PACKAGE_NAME "${PROJECT_NAME}")
  set(CPACK_PACKAGE_VENDOR "${PROJECT_NAME} developers")
  set(CPACK_PACKAGE_DESCRIPTION "${PROJECT_DESCRIPTION}")
  set(CPACK_DEBIAN_PACKAGE_NAME "${CPACK_PACKAGE_NAME}")
  set(CPACK_RPM_PACKAGE_NAME "${CPACK_PACKAGE_NAME}")
  set(CPACK_PACKAGE_HOMEPAGE_URL "${PROJECT_HOMEPAGE_URL}")
  set(CPACK_PACKAGE_MAINTAINER "${CPACK_PACKAGE_VENDOR}")
  set(CPACK_DEBIAN_PACKAGE_MAINTAINER "${CPACK_PACKAGE_MAINTAINER}")
  set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_CURRENT_SOURCE_DIR}/LICENSE.txt")
  set(CPACK_RESOURCE_FILE_README "${CMAKE_CURRENT_SOURCE_DIR}/README.md")

  set(CPACK_DEBIAN_PACKAGE_NAME "lib${PROJECT_NAME}-dev")
  set(CPACK_DEBIAN_PACKAGE_DEPENDS "libc6-dev")
  set(CPACK_DEBIAN_PACKAGE_SUGGESTS "cmake, pkg-config, pkg-conf")

  set(CPACK_RPM_PACKAGE_NAME "lib${PROJECT_NAME}-devel")
  set(CPACK_RPM_PACKAGE_SUGGESTS "${CPACK_DEBIAN_PACKAGE_SUGGESTS}")

  set(CPACK_DEB_COMPONENT_INSTALL ON)
  set(CPACK_RPM_COMPONENT_INSTALL ON)
  set(CPACK_NSIS_COMPONENT_INSTALL ON)
  set(CPACK_DEBIAN_COMPRESSION_TYPE "xz")

  option(${PROJECT_NAME}_INSTALL "Generate the install target" ${${PROJECT_NAME}_MASTER_PROJECT})


#
# Print recap
#

  option(${PROJECT_NAME}_RECAP_OPTS "Print a recap of the options status." ON)

  function(printRecap)
    if (${PROJECT_NAME}_RECAP_OPTS)
      message(STATUS "")
      message(STATUS "${PROJECT_NAME} RECAP OPTIONS:")
      message(STATUS "------------------------------")
      message(STATUS "${PROJECT_NAME}_RECAP_OPTS : ${${PROJECT_NAME}_RECAP_OPTS}")
      message(STATUS "${PROJECT_NAME}_BUILD_EXECUTABLE : ${${PROJECT_NAME}_BUILD_EXECUTABLE}")
      message(STATUS "${PROJECT_NAME}_BUILD_HEADERS_ONLY : ${${PROJECT_NAME}_BUILD_HEADERS_ONLY}")
      message(STATUS "${PROJECT_NAME}_BUILD_STATIC : ${${PROJECT_NAME}_BUILD_STATIC}")
      message(STATUS "${PROJECT_NAME}_BUILD_SHARED : ${${PROJECT_NAME}_BUILD_SHARED}") 
      message(STATUS "${PROJECT_NAME}_BUILD_DOCUMENTATION : ${${PROJECT_NAME}_BUILD_DOCUMENTATION}")
      message(STATUS "${PROJECT_NAME}_INSTALL : ${${PROJECT_NAME}_INSTALL}")
      message(STATUS "${PROJECT_NAME}_TESTING : ${${PROJECT_NAME}_TESTING}")
      message(STATUS "${PROJECT_NAME}_CODE_COVERAGE : ${${PROJECT_NAME}_CODE_COVERAGE}")
      message(STATUS "INSTALL_GTEST : ${INSTALL_GTEST}")
      message(STATUS "INSTALL_GMOCK : ${INSTALL_GMOCK}")
      # 
      message(STATUS "${PROJECT_NAME}_SWIG_BINDING : ${${PROJECT_NAME}_SWIG_BINDING}")
      message(STATUS "${PROJECT_NAME}_LINTER : ${${PROJECT_NAME}_LINTER}")
      message(STATUS "${PROJECT_NAME}_CPPCHECK : ${${PROJECT_NAME}_CPPCHECK}")
      message(STATUS "${PROJECT_NAME}_SANITIZER : ${${PROJECT_NAME}_SANITIZER}")
      message(STATUS "${PROJECT_NAME}_ENABLE_LTO : ${${PROJECT_NAME}_ENABLE_LTO}")
      message(STATUS "${PROJECT_NAME}_USE_GOLDLINKER : ${${PROJECT_NAME}_USE_GOLDLINKER}")
      message(STATUS "${PROJECT_NAME}_ENABLE_CCACHE : ${${PROJECT_NAME}_ENABLE_CCACHE}")
      message(STATUS "${PROJECT_NAME}_MEASURE_ALL : ${${PROJECT_NAME}_MEASURE_ALL}")
      # message(STATUS "RUN_TIDY : ${RUN_TIDY}") # TO SUPPRESS
      # message(STATUS "DO_CLANG_TIDY : ${DO_CLANG_TIDY}") 
      
      message(STATUS "------------------------------")
    endif()
  endfunction(printRecap)