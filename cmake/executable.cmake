#-------------------------------------------------------------------------------
# PROJECT executable setup

# Setup main executable to create.
add_executable(${PROJECT_EXECUTABLE_NAME} MACOSX_BUNDLE
    ${ICON_RESOURCE}
    ${CMAKE_BINARY_DIR}/include/OBLiX/version.hpp
    ${EXAMPLE_SRCS})

# add the binary tree to the search path for include files.
target_include_directories(${PROJECT_EXECUTABLE_NAME} PUBLIC
	$<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/include>
	$<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>
)
target_include_directories(${PROJECT_EXECUTABLE_NAME} PUBLIC
	$<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/external/includes_standalones>
	$<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>
)
target_include_directories(${PROJECT_EXECUTABLE_NAME} PUBLIC
	$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
	$<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>
)


#-------------------------------------------------------------------------------
# Compilation Options

# Enable C++17 for this part of the project
target_compile_features(${PROJECT_EXECUTABLE_NAME} PRIVATE cxx_std_17)

if ( CMAKE_BUILD_TYPE STREQUAL "Debug" )
    if(MSVC)
        target_compile_options(${PROJECT_EXECUTABLE_NAME} PRIVATE -W3 -WX)
    else()
        target_compile_options(${PROJECT_EXECUTABLE_NAME} PRIVATE -Wall -Wextra -Wshadow -Wunused -pedantic)
    endif()
endif()

if(APPLE)
    set_target_properties(${PROJECT_EXECUTABLE_NAME} PROPERTIES MACOSX_BUNDLE_BUNDLE_NAME "${PROJECT_EXECUTABLE_NAME}")
    set_target_properties(${PROJECT_EXECUTABLE_NAME} PROPERTIES MACOSX_BUNDLE_BUNDLE_GUI_IDENTIFIER "com.${PROJECT_EXECUTABLE_NAME}.${PROJECT_EXECUTABLE_NAME}")
    set_target_properties(${PROJECT_EXECUTABLE_NAME} PROPERTIES MACOSX_BUNDLE_ICON_FILE ${PROJECT_EXECUTABLE_NAME}.icns)
    set_target_properties(${PROJECT_EXECUTABLE_NAME} PROPERTIES MACOSX_BUNDLE_INFO_PLIST ${PROJECT_SOURCE_DIR}/packaging/MacOSXBundleInfo.plist.in)
    set_source_files_properties(${PROJECT_SOURCE_DIR}/packaging/${PROJECT_EXECUTABLE_NAME}.icns PROPERTIES MACOSX_PACKAGE_LOCATION "Resources")
endif()


#------------------------------------------------------------------------------
# Libraries dependencies

# Thread
# find_package (Threads REQUIRED) #threading
# target_link_libraries(${PROJECT_EXECUTABLE_NAME} PRIVATE Threads::Threads)

# SpdLog
# target_link_libraries( ${PROJECT_EXECUTABLE_NAME} spdlog::spdlog_header_only ) 

# # CxxOpts
# target_link_libraries( ${PROJECT_EXECUTABLE_NAME} cxxopts )

# # RapidJSON
# target_link_libraries( ${PROJECT_EXECUTABLE_NAME} rapidjson )

# # RapidXML
# target_link_libraries( ${PROJECT_EXECUTABLE_NAME} rapidxml )

# # Cereal
# target_link_libraries( ${PROJECT_EXECUTABLE_NAME} cereal )


#------------------------------------------------------------------------------
# Configure files
# configure version.hpp in with selected version
target_include_directories(${PROJECT_EXECUTABLE_NAME} PUBLIC
	$<BUILD_INTERFACE:${CMAKE_BINARY_DIR}/include>
	$<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>
)


#------------------------------------------------------------------------------
## Add subdirectories
# add_subdirectory(labodither)
# target_link_libraries(${PROJECT_EXECUTABLE_NAME} dithering_lib)


#------------------------------------------------------------------------------
# Debug/Sanitize macros

# Using macro from cmake/debugTools.cmake included on the top
# Provides OBLiXprog-run and OBLiXprog-dbg
include(debugTools)
addRunAndDebugTargets(${PROJECT_EXECUTABLE_NAME})
# short convenience target from cmake/debugTools.cmake
add_custom_target(run DEPENDS ${PROJECT_EXECUTABLE_NAME}-run)


#------------------------------------------------------------------------------
# Define installation rules

install(TARGETS ${PROJECT_EXECUTABLE_NAME}
    BUNDLE DESTINATION "."
    RUNTIME DESTINATION bin/OBLiX)

# Copy MINGW needed libraries for building on windows
if(MINGW)
    message(STATUS "MinGW detected")
    get_filename_component(GCC_PATH ${CMAKE_C_COMPILER} PATH)
    if(${GCC_PATH} MATCHES "mingw64/bin")
        set(libgcc "libgcc_s_seh-1.dll") #64bit
        # ----------------------------
        # Solution to compile with Windows, we don't really know why but it works
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O1") #we can compile with -O1, -O2 and -O3. We can also add -flto
        set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -std=c++17")
        message(STATUS "  64bit dlls is building")
    else()
        set(libgcc "libgcc_s_dw2-1.dll") #32bit
        message(STATUS "  32bit dlls is building")
    endif()

    install(FILES ${GCC_PATH}/${libgcc}
        ${GCC_PATH}/libwinpthread-1.dll
        ${GCC_PATH}/libstdc++-6.dll
        DESTINATION ./bin/
    )
endif(MINGW)

# To clone OBLiXprog.exe in OBLiX/bin
install(DIRECTORY ${PROJECT_SOURCE_DIR}/bin/
        DESTINATION bin/OBLiX  
        PATTERN "__py*" EXCLUDE 
        PATTERN ".py*" EXCLUDE 
        PATTERN "*.py" )

# 
install(TARGETS ${PROJECT_EXECUTABLE_NAME}
    EXPORT ${PROJECT_NAME}_Targets
    BUNDLE DESTINATION "."
    ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
    RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
)

# EoF