# Helper macro for creating convenient targets
find_program(GDB_PATH gdb)

# Adds -run and -dbg targets
macro(addRunAndDebugTargets TARGET)
	add_custom_target(${TARGET}-run
		WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
		USES_TERMINAL
		DEPENDS ${TARGET}
		COMMAND ./${TARGET})

	# convenience run gdb target
	if(GDB_PATH)
		add_custom_target(${TARGET}-gdb
			WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
			USES_TERMINAL
			DEPENDS ${TARGET}
			COMMAND ${GDB_PATH} ./${TARGET})
	endif()
endmacro()