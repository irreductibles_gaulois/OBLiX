#------------------------------------------------------------------------------
# Usefull for adding header only libraries
# Example usage:
#
#     ExternalHeaderOnly_Add("Catch"
#         "https://github.com/catchorg/Catch2.git" "origin/master" "single_include/catch2")
#
# Use with:
#     target_link_libraries(unittests Catch)
# This will add the INCLUDE_FOLDER_PATH to the `unittests` target.

macro(ExternalHeaderOnly_Add LIBNAME REPOSITORY GIT_TAG INCLUDE_FOLDER_PATH)
	ExternalProject_Add(
		${LIBNAME}_download
		PREFIX ${CMAKE_CURRENT_SOURCE_DIR}/libs_sources/${LIBNAME}_download
		SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/libs_sources/${LIBNAME}_download"
		GIT_REPOSITORY ${REPOSITORY}
		# For shallow git clone (without downloading whole history)
		GIT_SHALLOW 1
		# For point at certain tag
		GIT_TAG ${GIT_TAG}
		#disables auto update on every build
		UPDATE_DISCONNECTED 1
		#disable following
		CONFIGURE_COMMAND "" 
		BUILD_COMMAND "" 
		INSTALL_DIR "" 
		INSTALL_COMMAND ""
		CONFIGURE_HANDLED_BY_BUILD ON
		CMAKE_ARGS
      	-DCMAKE_INSTALL_PREFIX:PATH=${CMAKE_BINARY_DIR}/external/libs_export_install/${LIBNAME}
		)
	# special target
	add_custom_target(${LIBNAME}_update
		COMMENT "Updated ${LIBNAME}"
		WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/libs_sources/${LIBNAME}_download
		COMMAND ${GIT_EXECUTABLE} fetch --recurse-submodules
		COMMAND ${GIT_EXECUTABLE} reset --hard ${GIT_TAG}
		COMMAND ${GIT_EXECUTABLE} submodule update --init --force --recursive --remote --merge
		DEPENDS ${LIBNAME}_download)

	set(${LIBNAME}_SOURCE_DIR ${CMAKE_BINARY_DIR}/libs_sources/${LIBNAME}_download/)
	add_library(${LIBNAME}_interface INTERFACE)
	add_dependencies(${LIBNAME}_interface ${LIBNAME}_download)
	add_dependencies(update ${LIBNAME}_update)
	target_include_directories(${LIBNAME}_interface SYSTEM INTERFACE ${CMAKE_BINARY_DIR}/external/libs_sources/${LIBNAME}_download/${INCLUDE_FOLDER_PATH})
endmacro()

#------------------------------------------------------------------------------
# This command will clone git repo during cmake setup phase, also adds 
# ${LIBNAME}_update target into general update target.
# Example usage:
#
#   ExternalDownloadNowGit(cpr https://github.com/finkandreas/cpr.git origin/master)
#   add_subdirectory(${cpr_SOURCE_DIR})
#

macro(ExternalDownloadNowGit LIBNAME REPOSITORY GIT_TAG)

	set(${LIBNAME}_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/libs_sources/${LIBNAME}_download/)

	# clone repository if not done
	if(IS_DIRECTORY ${${LIBNAME}_SOURCE_DIR})
		message(STATUS "Already downloaded: ${REPOSITORY}")
	else()
		message(STATUS "Clonning: ${REPOSITORY}")
		execute_process(
			WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
			COMMAND ${GIT_EXECUTABLE} clone --recursive ${REPOSITORY} libs_sources/${LIBNAME}_download
			)
		# switch to target TAG and update submodules
		execute_process(
			WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/libs_sources/${LIBNAME}_download
			COMMAND ${GIT_EXECUTABLE} reset --hard ${GIT_TAG}
			COMMAND ${GIT_EXECUTABLE} submodule update --init --force --recursive --remote --merge
			)
	endif()

	# special update target
	add_custom_target(${LIBNAME}_update
		COMMENT "Updated ${LIBNAME}"
		WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/libs_sources/${LIBNAME}_download
		COMMAND ${GIT_EXECUTABLE} fetch --recurse-submodules
		COMMAND ${GIT_EXECUTABLE} reset --hard ${GIT_TAG}
		COMMAND ${GIT_EXECUTABLE} submodule update --init --force --recursive --remote --merge)
	# Add this as dependency to the general update target
	add_dependencies(update ${LIBNAME}_update)
endmacro()

