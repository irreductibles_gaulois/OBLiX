#!/usr/bin/env python

"""
	Cobertura XML report merger from
	https://github.com/gammu/gammu/blob/master/contrib/coveragehelper/merge-cobertura.py

	Written for merging OpenCppCoverage reports, thus not completely supporting all
	Cobertura XML attributes, only line coverage which OpenCppCoverage generates.
	Anything else is simply discarded in the process.

	Copyright (C) 2018 Michal Cihar <michal@cihar.com>
"""

import sys

try:
	from xml.etree.ElementTree import Element, ElementTree, SubElement, iterparse
except ImportError:
	from xml.etree.ElementTree import Element, ElementTree, SubElement, iterparse

HEADER = """<?xml version="1.0"?>
<!DOCTYPE coverage SYSTEM "http://cobertura.sourceforge.net/xml/coverage-04.dtd">
"""


def log(message=""):
	"""Log message to stderr"""
	sys.stderr.write(message)
	sys.stderr.write("\n")


def read_files(names):
	"""Read coverage data from input files"""
	result = {}
	lines = {}
	outfile = None
	log("Reading files:")

	for filename in names:
		log(f"* {filename}")
		for event, elem in iterparse(filename, events=("start", "end")):
			if elem.tag == "class" and event == "start":
				outfile = elem.get("filename")
				lines = result.get(outfile, {})
			elif elem.tag == "line" and event == "end":
				line = elem.get("number")
				lines[line] = lines.get(line, 0) + int(elem.get("hits"))
			elif elem.tag == "class" and event == "end":
				result[outfile] = lines
	log()

	return result


def get_line_rates(data):
	"""Calculate line hit rates from raw coverage data"""
	result = {}
	totalLines = 0
	totalHits = 0
	log("Counting line rates:")
	for item in data:
		lines = len(data[item])
		hits = sum(1 for x in data[item].values() if x)
		result[item] = 1.0 * hits / lines
		log(f" * {item} = {result[item]} ({hits} / {lines})")
		totalLines += lines
		totalHits += hits

	result["_"] = 1.0 * totalHits / totalLines
	result["_hits"] = totalHits
	result["_lines"] = totalLines

	return result


def write_data(data, handle):
	"""Write Cobertura XML for coverage data"""
	lineRates = get_line_rates(data)

	log("Generating output...")
	root = Element("coverage")
	root.set("line-rate", str(lineRates["_"]))
	root.set("branch-rate", "0")
	root.set("complexity", "0")
	root.set("branches-covered", "0")
	root.set("branches-valid", "0")
	root.set("timestamp", "0")
	root.set("lines-covered", str(lineRates["_hits"]))
	root.set("lines-valid", str(lineRates["_lines"]))
	root.set("version", "0")

	sources = SubElement(root, "sources")
	source = SubElement(sources, "source")
	source.text = "c:"

	packages = SubElement(root, "packages")
	package = SubElement(packages, "package")
	package.set("name", "OBLiX")
	package.set("line-rate", str(lineRates["_"]))
	package.set("branch-rate", "0")
	package.set("complexity", "0")
	classes = SubElement(package, "classes")

	separator= '/'
	if sys.platform.startswith('linux') :
		pass
	elif sys.platform.startswith('win') :
		separator = '\\'
	else :
		print('WARNING: Untested OS')
		separator=''

	for item in data:
		obj = SubElement(classes, "class")
		try:
			obj.set("name", item.rsplit(separator, 1)[1])
		except IndexError:
			obj.set("name", item.rsplit(separator, 1)[0])
		obj.set("filename", item)
		obj.set("line-rate", str(lineRates[item]))
		obj.set("branch-rate", "0")
		obj.set("complexity", "0")
		SubElement(obj, "methods")
		lines = SubElement(obj, "lines")
		for line in sorted(data[item], key=lambda x: int(x)): # pylint: disable=unnecessary-lambda
			obj = SubElement(lines, "line")
			obj.set("number", line)
			obj.set("hits", str(data[item][line]))

	tree = ElementTree(root)

	handle.write(HEADER)

	tree.write(handle, xml_declaration=False, encoding='unicode')


def main(filesIn, fileOut):
	"""Command line interface"""

	result = read_files(filesIn)

	if fileOut == sys.stdout :
		write_data(result, sys.stdout)
	else:
		with open(fileOut, "w", encoding='utf-8') as handle:
			write_data(result, handle)
	# return

if __name__ == "__main__":
	import os

	# Find chRootProject path.
	# pylint: disable=no-else-break
	_PREV_PATH = None
	_CALLING_DIR = os.getcwd()
	while True :
		if (os.path.exists('./OBLiX-README.md')) and (os.getcwd()!='/'):
			CHROOT_PATH=os.getcwd()
			break
		elif _PREV_PATH==os.getcwd() :
			print('ERROR: the chRootProject folder was not found...')
			os.chdir(_CALLING_DIR)
			sys.exit(3)
			break
		else:
			_PREV_PATH=os.getcwd()
			os.chdir('../')
	os.chdir(_CALLING_DIR) # Go back to the call directory.
	del _PREV_PATH
	# del _CALLING_DIR
	# pylint: enable=no-else-break

	files2merge = [CHROOT_PATH+'/build/coveragePY.xml', CHROOT_PATH+'/build/coverageCXX.xml']
	fileOutput = CHROOT_PATH+'/build/coverage.xml' #'./coverage.xml' #sys.stdout
	main(files2merge, fileOutput)

# EoF
