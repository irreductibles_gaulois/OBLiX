#------------------------------------------------------------------------------
# Clang and gcc sanitizers
if (${PROJECT_NAME}_SANITIZER)
	if (CMAKE_CXX_COMPILER_ID MATCHES "Clang" OR "${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
		#  message(STATUS "  + ADDRESS_SANITIZER                     ${ADDRESS_SANITIZER}")
		add_compile_options(-fsanitize=address -fno-omit-frame-pointer)
		link_libraries(-fsanitize=address -fno-omit-frame-pointer)
		
		#  message(STATUS "  + UB_SANITIZER                          ${UB_SANITIZER}")
		add_compile_options(-fsanitize=undefined)
		link_libraries(-fsanitize=undefined)
		
		#  message(STATUS "  + THREAD_SANITIZER                      ${THREAD_SANITIZER}")
		add_compile_options(-fsanitize=thread)
		link_libraries(-fsanitize=thread)

		# Clang only
		if (CMAKE_CXX_COMPILER_ID MATCHES "Clang")
			option(MEMORY_SANITIZER "description" OFF)
			message(STATUS "  + MEMORY_SANITIZER                      ${MEMORY_SANITIZER}")
			if(MEMORY_SANITIZER)
				add_compile_options(-fsanitize=memory -fno-omit-frame-pointer)
				link_libraries(-fsanitize=memory -fno-omit-frame-pointer)
			endif()
		endif()
	else()
		message(WARNING "Sanitizer option is on but the compiler ${CMAKE_CXX_COMPILER_ID} is not compatible.")
	endif()
endif()


#--------------------------------------------------------------------
# list_intersection(retval list_a list_b)
# return items that are in both lists
function(list_intersection retval list_a list_b)
	set(intersection "")
	list(REMOVE_DUPLICATES list_a)
	list(REMOVE_DUPLICATES list_b)

	foreach(item ${list_a})
		list(FIND list_b ${item} index)
		if(NOT index EQUAL -1)
			list(APPEND intersection ${item})
		endif()
	endforeach()
	set(${retval} ${intersection} PARENT_SCOPE)
endfunction()

	
#------------------------------------------------------------------------------
# Other Checking targets - formating, static analysis, cppcheck, tidy and linting
macro(addCheckingTargets)
	
	# Get all cpp and hpp and py files
	file(GLOB_RECURSE ALL_SOURCE_FILES 	${PROJECT_SOURCE_DIR}/*.cxx 
													${PROJECT_SOURCE_DIR}/*.cpp 
													${PROJECT_SOURCE_DIR}/*.cc 
													${PROJECT_SOURCE_DIR}/*.c 
	)
	file(GLOB_RECURSE ALL_HEADER_FILES 	${PROJECT_SOURCE_DIR}/*.hxx
													${PROJECT_SOURCE_DIR}/*.hpp 
													${PROJECT_SOURCE_DIR}/*.hh 
													${PROJECT_SOURCE_DIR}/*.h
	)

	file(GLOB_RECURSE ALL_PY_FILES ${PROJECT_SOURCE_DIR}/*.py )
	
	# remove files that are not ours.
	list(FILTER ALL_SOURCE_FILES EXCLUDE REGEX "${PROJECT_SOURCE_DIR}/external/.*" )
	list(FILTER ALL_HEADER_FILES EXCLUDE REGEX "${PROJECT_SOURCE_DIR}/external/.*" )
	list(FILTER ALL_PY_FILES EXCLUDE REGEX "${PROJECT_SOURCE_DIR}/external/.*" )

	list(FILTER ALL_SOURCE_FILES EXCLUDE REGEX "${PROJECT_SOURCE_DIR}/docs/source/.*" )
	list(FILTER ALL_HEADER_FILES EXCLUDE REGEX "${PROJECT_SOURCE_DIR}/docs/source/.*" )
	list(FILTER ALL_PY_FILES EXCLUDE REGEX "${PROJECT_SOURCE_DIR}/docs/source/.*" )

	list(FILTER ALL_SOURCE_FILES EXCLUDE REGEX "${PROJECT_BINARY_DIR}/.*" )
	list(FILTER ALL_HEADER_FILES EXCLUDE REGEX "${PROJECT_BINARY_DIR}/.*" )
	list(FILTER ALL_PY_FILES EXCLUDE REGEX "${PROJECT_BINARY_DIR}/.*" )
	
	# execute_process is already piped !!
	# Command to execute:	# git status --porcelain | grep -o '[^ ]\+$'
	execute_process(#COMMAND bash "-c" "git status --porcelain | tr '\n' ' ' | sed  's/M\|MM \|?? \|  /X/g';"
						COMMAND git status --porcelain 
						COMMAND tr "\n" " " 
						COMMAND sed "s/M  \\|MM \\|T \\|A \\|D \\|R \\|C \\|U \\|?? \\|  //g" 
						OUTPUT_VARIABLE MODIF_FILES_STR 
						RESULT_VARIABLE RC_MODIF_FILES
						OUTPUT_STRIP_TRAILING_WHITESPACE
	)
	string(REPLACE " " ";" MODIF_FILES "${MODIF_FILES_STR}")
	list(TRANSFORM MODIF_FILES PREPEND "${PROJECT_SOURCE_DIR}/")


	# Get the intersections of cpp|hpp|py and modified files
	list_intersection(modif_sources "${MODIF_FILES}" "${ALL_SOURCE_FILES}")
	list_intersection(modif_header "${MODIF_FILES}" "${ALL_HEADER_FILES}")
	list_intersection(modif_py "${MODIF_FILES}" "${ALL_PY_FILES}")

	# cpp check static analysis
	if (${PROJECT_NAME}_CPPCHECK)
		find_program(CPPCHECK_PATH cppcheck)
		if(CPPCHECK_PATH)
			message(STATUS "cppcheck - Found")
			add_custom_target(
					cppcheck
					COMMAND ${CPPCHECK_PATH}
					--enable=warning,performance,portability,information,missingInclude
					--std=c++17
					--template=gcc
					--verbose
					--quiet
					${ALL_SOURCE_FILES}
			)
		else()
			message(WARNING "${PROJECT_NAME}_CPPCHECK option is ON but the program was not found !")
		endif()
	endif()

	# Static analysis via clang-tidy target
	# We check for program, since when it is not here, target makes no sense
	if (${PROJECT_NAME}_LINTER)
		find_program(TIDY_PATH clang-tidy NAMES "clang-tidy" DOC "Path to clang-tidy executable" PATHS /usr/local/Cellar/llvm/*/bin)
		if(TIDY_PATH)
			message(STATUS "clang-tidy - Found: ${TIDY_PATH}")
			
			# if (RUN_TIDY)
			# 	message(STATUS "RUN_TIDY is ON !")
			# 	set(DO_CLANG_TIDY "${TIDY_PATH}" "-header-filter='${PROJECT_SOURCE_DIR}/*'" "-p=./") #"-checks=*,-clang-analyzer-alpha.*") 3
			# 	set(CMAKE_CXX_CLANG_TIDY  clang-tidy; -header-filter='${PROJECT_SOURCE_DIR}/*'; )

			# 	set( PREVIOUS_CMAKE_MESSAGE_LOG_LEVEL "${CMAKE_MESSAGE_LOG_LEVEL}" )
			# 	message(STATUS "DO_CLANG_TIDY = ${DO_CLANG_TIDY}")
			# 	message(STATUS "CMAKE_CXX_CLANG_TIDY = ${CMAKE_CXX_CLANG_TIDY}")
			# 	message(STATUS "PREVIOUS_CMAKE_MESSAGE_LOG_LEVEL = ${PREVIOUS_CMAKE_MESSAGE_LOG_LEVEL}")
			# 	message(STATUS "CMAKE_MESSAGE_LOG_LEVEL = ${CMAKE_MESSAGE_LOG_LEVEL} !")
			# else()
			# 	message(STATUS "RUN_TIDY is : ${RUN_TIDY}!")
			# 	set(DO_CLANG_TIDY "") 
			# 	set( PREVIOUS_CMAKE_MESSAGE_LOG_LEVEL "${CMAKE_MESSAGE_LOG_LEVEL}" )
			# endif()
			
			add_custom_target(tidy
				COMMAND ${CMAKE_COMMAND} -P "${PROJECT_SOURCE_DIR}/cmake/scriptRegEx4Tidy.cmake"
				COMMAND echo "CMAKE: file ${CMAKE_BINARY_DIR}/compile_commands.json modification updated !"

				# Header are not needed since the option header-filter already check them.
				# The option -p is the where to find the compile_commands.json file.
				COMMAND echo "COMMAND: ${TIDY_PATH} -header-filter=${PROJECT_SOURCE_DIR}/* {ALL_SOURCE_FILES} -p=./" #${ALL_HEADER_FILES} 
				COMMAND ${TIDY_PATH} -header-filter='${PROJECT_SOURCE_DIR}/*' ${ALL_SOURCE_FILES} -p=${CMAKE_BINARY_DIR}/ 

				# COMMAND echo "${CMAKE_COMMAND} --build ${CMAKE_BINARY_DIR} --target clean"
				# COMMAND ${CMAKE_COMMAND} --build ${CMAKE_BINARY_DIR} --target clean
				# COMMAND echo "${CMAKE_COMMAND} ${PROJECT_SOURCE_DIR} -DRUN_TIDY=ON"
				# COMMAND ${CMAKE_COMMAND} ${PROJECT_SOURCE_DIR} -DRUN_TIDY=ON #-DCMAKE_MESSAGE_LOG_LEVEL="WARNING"
				# COMMAND echo "${CMAKE_COMMAND} --build ${CMAKE_BINARY_DIR}"
				# COMMAND ${CMAKE_COMMAND} --build ${CMAKE_BINARY_DIR}
				# COMMAND ${CMAKE_COMMAND} ${PROJECT_SOURCE_DIR} -DRUN_TIDY=OFF -DCMAKE_MESSAGE_LOG_LEVEL="${PREVIOUS_CMAKE_MESSAGE_LOG_LEVEL}" 
				
				COMMAND echo "CLANG-TIDY: finished !"
			)

			add_custom_target(tidy-diff
				COMMENT "TIDY-DIFF: only for modified files."
				COMMAND ${CMAKE_COMMAND} -P "${PROJECT_SOURCE_DIR}/cmake/scriptRegEx.cmake"
				COMMAND echo "CMAKE: file ${CMAKE_BINARY_DIR}/compile_commands.json modification updated !" 
				COMMAND echo "COMMAND: ${TIDY_PATH} -header-filter=.* {modif_sources} {modif_header} -p=./" #${ALL_HEADER_FILES} 
				COMMAND if [ -z ${modif_sources}${modif_header} ]\; then echo "[skipped] No files to Tidy !" \; else ${TIDY_PATH} -header-filter='.*' ${modif_sources} ${modif_header} -p=./ \; fi \;
				COMMAND echo "CLANG-TIDY{-diff}: finished !"
			)
		else()
			message(WARNING "${PROJECT_NAME}_LINTER option is ON but the program clang-tidy was not found !")
		endif()



		# run clang-format on all files
		find_program(FORMAT_PATH clang-format)
		if(FORMAT_PATH)
			message(STATUS "clang-format - Found ")
			add_custom_target(format
				COMMAND echo "COMMAND: ${FORMAT_PATH} -style=file -i {ALL_SOURCE_FILES} {ALL_HEADER_FILES}"
				COMMAND ${FORMAT_PATH} -style=file -i ${ALL_SOURCE_FILES} ${ALL_HEADER_FILES} 
				COMMAND echo "CLANG-FORMAT: finished !"
			)

			add_custom_target(format-diff
				COMMAND echo "COMMAND: ${FORMAT_PATH} --dry-run -Werror -style=file -i {modif_sources} {modif_header}"
				COMMAND if [ -z ${modif_sources}${modif_header} ]\; then echo "[skipped] No files to Format !" \; else ${FORMAT_PATH} --dry-run -Werror -style=file -i ${modif_sources} ${modif_header} \; fi \;	
				COMMAND echo "CLANG-FORMAT{-diff}: finished !"
			)
		else()
			message(WARNING "${PROJECT_NAME}_LINTER option is ON but the program clang-format was not found !")
		endif()


		# run clang-format on all files
		find_program(PYLINT_PATH pylint)
		if(PYLINT_PATH)
			message(STATUS "pylint - Found ")
			add_custom_target(pylint
				COMMAND echo "COMMAND: ${PYLINT_PATH} --rcfile=${PROJECT_SOURCE_DIR}/src/.pylintrc {ALL_PY_FILES} "
				COMMAND ${PYLINT_PATH} -d duplicate-code --rcfile=${PROJECT_SOURCE_DIR}/src/.pylintrc ${ALL_PY_FILES} 
				COMMAND echo "Pyhon Linter: finished !"
			)

			add_custom_target(pylint-diff
				COMMAND echo "COMMAND: ${PYLINT_PATH} -d duplicate-code --rcfile=${PROJECT_SOURCE_DIR}/src/.pylintrc {modif_py}"
				COMMAND if [ -z ${modif_py} ]\; then echo "[skipped] No files to Pylint !" \; else ${PYLINT_PATH} -d duplicate-code --rcfile=${PROJECT_SOURCE_DIR}/src/.pylintrc ${modif_py} \; fi \;
				COMMAND echo "PyLint{-diff}: finished !"
			)
		else()
			message(WARNING "${PROJECT_NAME}_LINTER option is ON but the program pylint was not found !")
		endif()
	endif()

endmacro()
