// C++ program to execute commands oject for simulations.
/**
 * \file main.cpp
 * \brief Program Name: OBLiX (OBject Library eXecutor)
 * \author Romain NOËL (romain.noel@univ-eiffel.fr) & coauthors
 * \version 0.1.a
 * \date 1 september 2021
 *
 * Program to execute ...
 */

#include <iostream> // std::cout
#include <algorithm> // std::max
#include "OBLiX/OBLiX.hpp"

#include "example_module.hpp"


/**
 * @brief initialize the map factory for all the BaseX child that can be
 * 	executed.
 *
 * @fn redef_map_factory function
 * @param[inout] mapFactory is filled with class names as key and the unique_ptr
 * 	creator function as associated element.
 * @return void.
 */
void redef_map_factory() {
	// clean map factory
	OBLiX::get_factory().clear_all_builders();

	// Repopulated your mapFactory.
	OBLiX::get_factory().register_builder("BaseRun", OBLiX::unique_creator<OBLiX::BaseX>);
	OBLiX::get_factory().register_builder("Circle", OBLiX::unique_creator<ClassCircle>);
	OBLiX::get_factory().register_builder("Continue", OBLiX::unique_creator<OBLiX::ContinueBRC>);
	OBLiX::get_factory().register_builder("Rectangle", OBLiX::unique_creator<ClassRectangle>);

	return;
} // end redef_map_factory


/**
 * @brief main function that execute the OBLiX lib. It reads the argc and argv
 * 	given when the main.exe is called. If a input file is given, it execute
 * it, otherwise in goes to a CLI mode.
 *
 * @param argc argument count: the number of strings pointed to by argv.
 * @param argv argument vector: strings containing the arguments given.
 * @return int error code: an error occurred if the returned code is non-zero.
 */
int main(int argc, const char *argv[]) {
	// Initialize variables
	int errorCode = -1;
	int errorCodeTemp = -1;

	// Beginning of main.
	std::cout << "YOUR MAIN: Init." << std::endl;

	// Parse the arguments from main. This is needed to capture the input file (if none OBLiX goes in CLI mode).
	errorCodeTemp = OBLiX::parse_args(argc, argv);
	errorCode = std::max(errorCode, errorCodeTemp);

	// Setup the logger according to the input arguments.
	errorCodeTemp = OBLiX::setup_logger();
	errorCode = std::max(errorCode, errorCodeTemp);

	// Get the pointer logger from the previous setup.
	auto pLog = shareLog::get_plog();

	// Rearrange the BaseRun map factory if wanted.
	// OBLiX::get_factory().erase_builder("Continue"); // remove some element of the factory.
	// redef_map_factory(); // redefine your own brew of factory.
	// OBLiX::get_factory().print_builders(); // print all element present in the factory.

	// Read the input (file or CLI) and execute
	errorCodeTemp = OBLiX::read_and_run();
	errorCode = std::max(errorCode, errorCodeTemp);

	// End of the MAIN.
	pLog->info("YOUR MAIN finished with return code {}.", errorCode);
	return (errorCode);
} // end main.

// EoF
