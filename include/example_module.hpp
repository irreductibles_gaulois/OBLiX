/**
 * @file fake_module.hpp
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2021-09-24
 *
 * @copyright Copyright (c) 2021
 *
 */

// #ifdef __cplusplus
// extern "C" {
// #endif

#ifndef FAKE_MODULE_HPP
#define FAKE_MODULE_HPP

#include "OBLiX/baseX.hpp"
#include <vector>
#include <cereal/types/vector.hpp>


// extern MapInOutClass mapResultName2Object;
// extern std::map<std::string, CreatorU> mapCreatorUnique;


//
class ClassRectangle : public OBLiX::Adapt<ClassRectangle> {
 private:
	/* data */
 public:
	std::string get_classname() const override { return "Rectangle"; };
	std::shared_ptr<double> length = std::make_shared<double>(0.1);
	std::shared_ptr<double> width = std::make_shared<double>();
	std::shared_ptr<double> perimeter = std::make_shared<double>();
	std::shared_ptr<double> area = std::make_shared<double>();

	ClassRectangle() {
		mapIn.set_store_address("length", length, true);
		mapIn.set_store_address("width", width);
		mapIn.new_with_type<std::vector<double>>("center");
		mapOut.set_store_address("perimeter", perimeter);
		mapOut.set_store_address("area", area);
	}

	int run() override {
		auto pLog = shareLog::get_plog();
		*perimeter.get() = 2. * (*length.get()) + 2. * (*width.get());
		*area.get() = (*length.get()) * (*width.get());
		pLog->warn("I am a Rectangle with length {} and width {}", *length.get(), *mapIn.get<double>(std::string("width")));
		pLog->warn("So a Rectangle with perimeter {} and area {}", *perimeter.get(), *mapOut.get<double>("area"));
		return 0;
	}

	virtual ~ClassRectangle(){}; // Destructor
};
// Register the new class in the factory
#ifndef SWIG
static const bool kRegisteredRectangle = OBLiX::get_factory().register_builder("Rectangle", OBLiX::unique_creator<ClassRectangle>);
#else
extern bool kRegisteredRectangle;
#endif


// Option A to derived from a BaseRunAdapt<T> class, override manually the functions
class ClassCircle : public OBLiX::Adapt<ClassCircle> {
 private:
	/* data */
 public:
	std::string get_classname() const override { return "Circle"; };

	ClassCircle() {
		mapIn.new_with_type<double>("radius");
		mapIn.new_with_type<std::vector<double>>("center");
	}

	int init() override {
		mapOut.new_with_type<double>("perimeter");
		mapOut.new_with_type<double>("area");
		return 0;
	};

	int run() override {
		auto pLog = shareLog::get_plog();
		double radius = *mapIn.get<double>("radius");
		*mapOut.get<double>("perimeter") = 3.14 * 2. * radius;
		*mapOut.get<double>("area") = 3.14 * radius * radius;
		pLog->warn("I am a Circle with radius {}, perimeter {} and area {}", *mapIn.get<double>("radius"),
			 *mapOut.get<double>("perimeter"), *mapOut.get<double>("area"));
		pLog->warn("Circle State init {} running {} finished {}", isInit, isRunning, isFinished);
		return 0;
	}

	// Vtable function to replace the template virtual overriding.
	// clang-format off
		virtual void downcast_serialize(cereal::XMLInputArchive& 	ar, OBLiX::BaseX* p) override {OBLiX::Downcaster<ClassCircle> down;	down.serializer(ar, p);};
		virtual void downcast_serialize(cereal::XMLOutputArchive& 	ar, OBLiX::BaseX* p) override {OBLiX::Downcaster<ClassCircle> down;	down.serializer(ar, p);};
		virtual void downcast_serialize(cereal::JSONInputArchive& 	ar, OBLiX::BaseX* p) override {OBLiX::Downcaster<ClassCircle> down;	down.serializer(ar, p);};
		virtual void downcast_serialize(cereal::JSONOutputArchive&	ar, OBLiX::BaseX* p) override {OBLiX::Downcaster<ClassCircle> down;	down.serializer(ar, p);};
	// clang-format on

	virtual BaseX *clone() const override { return new ClassCircle(*this); }

	virtual ~ClassCircle(){}; // Destructor
};
// Register the new class in the factory
#ifndef SWIG
static const bool kRegisteredCircle = OBLiX::get_factory().register_builder("Circle", OBLiX::unique_creator<ClassCircle>);
#else
extern bool kRegisteredCircle;
#endif


// Option B to derived from a BaseRunAdapt<T> class, split the common part into another object without the BaseRunAdapt<T>
class FakeRectangle {
	//
 private:
	/* data */
 public:
	double length = 0.;
	double width = 0.;
	std::vector<double> center = {0., 0.};

	~FakeRectangle(){};
};


class ClassCircleBis : public OBLiX::Adapt<ClassCircleBis>, public FakeRectangle {
	//
 private:
	/* data */
 public:
	std::string get_classname() const override { return "Circle"; };
	double radius = 0.;
	std::vector<double> center = {0., 1.};

	int run() override {
		auto pLog = shareLog::get_plog();
		pLog->warn("I am a CircleFAKE with radius {}, width {}", radius, width);
		return 0;
	}

	virtual ~ClassCircleBis(){}; // Destructor
};


class ClassCable : public OBLiX::Adapt<ClassCable> {
	//
 private:
	/* data */
 public:
	std::string get_classname() const override { return "Cable"; };

	std::shared_ptr<double> length = std::make_shared<double>();
	std::shared_ptr<double> thickness = std::make_shared<double>();
	std::shared_ptr<double> min_tension = std::make_shared<double>();
	std::shared_ptr<double> max_tension = std::make_shared<double>();
	std::shared_ptr<double> weight = std::make_shared<double>();
	std::shared_ptr<double> optimal_tension = std::make_shared<double>();

	ClassCable() {
		mapIn.set_store_address("length", length);
		mapIn.set_store_address("thickness", thickness);
		mapIn.set_store_address("min_tension", min_tension);
		mapIn.set_store_address("max_tension", max_tension);
		mapIn.set_store_address("weight", weight);
		mapOut.set_store_address("optimal_tension", optimal_tension);
	}

	int run() override {
		auto pLog = shareLog::get_plog();
		*optimal_tension.get() = 0.5 * (*mapIn.get<double>("min_tension") + *mapIn.get<double>("max_tension"));

		pLog->warn("I am a Cable with length {}, thickness {}, min_tension {}, max_tension {}, weight {} and optimal_tension {}",
			 *mapIn.get<double>("length"), *mapIn.get<double>("thickness"), *mapIn.get<double>("min_tension"),
			 *mapIn.get<double>("max_tension"), *mapIn.get<double>("weight"), *mapOut.get<double>("optimal_tension"));
		
		return 0;
	}

	virtual ~ClassCable(){}; // Destructor
};
// Register the new class in the factory
#ifndef SWIG
static const bool kRegisteredClassCable = OBLiX::get_factory().register_builder("Cable", OBLiX::unique_creator<ClassCable>);
#else
extern bool kRegisteredClassCable;
#endif




#endif // FAKE_MODULE_HPP
// #ifdef __cplusplus
// } // end extern "C"
// #endif

// EoF