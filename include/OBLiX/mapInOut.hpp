/**
 * @file BaseX.hpp
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2021-09-24
 *
 * @copyright Copyright (c) 2021
 *
 */

// #ifdef __cplusplus
// extern "C" {
// #endif

// Guard
#ifndef MAPIOCLASS_HPP
#define MAPIOCLASS_HPP

// Standard libraries
#include <exception>
#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <typeindex>
#include <typeinfo>

// External libraries
#include <cereal/cereal.hpp>
#include <cereal/archives/json.hpp>
#include <cereal/archives/xml.hpp>
#include <cereal/types/optional.hpp>

// NOLINTNEXTLINE
namespace OBLiX {

class DataInOut {
 public:
	std::shared_ptr<void> data = nullptr;
	std::type_index type = typeid(nullptr);
	size_t sizeT = sizeof(nullptr);
	bool hasTypeInitialized = false;
	bool hasPtrInitialized = false;
	bool isOptional = false;

	virtual bool serialize_or_throw(
		 cereal::JSONOutputArchive &ar __attribute__((unused)), const char *charName __attribute__((unused))) {
		std::cout << "serialize_or_throw from DataInOut function." << std::endl;
		return true;
	}

	virtual bool serialize_or_throw(
		 cereal::JSONInputArchive &ar __attribute__((unused)), const char *charName __attribute__((unused))) {
		std::cout << "serialize_or_throw from DataInOut function." << std::endl;
		return true;
	}

	virtual bool serialize_or_throw(
		 cereal::XMLOutputArchive &ar __attribute__((unused)), const char *charName __attribute__((unused))) {
		std::cout << "serialize_or_throw from DataInOut function." << std::endl;
		return true;
	}

	virtual bool serialize_or_throw(
		 cereal::XMLInputArchive &ar __attribute__((unused)), const char *charName __attribute__((unused))) {
		std::cout << "serialize_or_throw from DataInOut function." << std::endl;
		return true;
	}

	virtual DataInOut *clone() const { return new DataInOut(*this); }

	virtual ~DataInOut(){};
};


class ExceptionOptionalNVPnotFound : public std::exception {
	virtual const char *what() const throw() { return "The function make_optional_nvp did not find the input string."; }
};


template <typename T>
class DataInOutType : public DataInOut {

	bool serialize_or_throw(cereal::JSONOutputArchive &ar, const char *charName) {
		// std::cout<<"serialize_or_throw JSONOutputArchive from DataInOutType<"<<typeid(T).name()<<"> with name "<<charName<<std::endl;
		if (!isOptional) {
			ar(cereal::make_nvp(charName, *static_cast<T *>(data.get())));
		} else {
			bool isPresent = cereal::make_optional_nvp(ar, charName, *static_cast<T *>(data.get()));
			// bool isPresent = true;
			if (!isPresent) {
				ExceptionOptionalNVPnotFound exceptionNVP;
				throw exceptionNVP;
				return false;
			}
		}
		return true;
	}

	bool serialize_or_throw(cereal::JSONInputArchive &ar, const char *charName) {
		// std::cout<<"serialize_or_throw JSONInputArchive from DataInOutType<"<<typeid(T).name()<<"> with name "<<charName<<std::endl;
		if (!isOptional) {
			ar(cereal::make_nvp(charName, *static_cast<T *>(data.get())));
		} else {
			bool isPresent = cereal::make_optional_nvp(ar, charName, *static_cast<T *>(data.get()));
			// bool isPresent = true;
			if (!isPresent) {
				ExceptionOptionalNVPnotFound exceptionNVP;
				throw exceptionNVP;
				return false;
			}
		}
		return true;
	}

	bool serialize_or_throw(cereal::XMLOutputArchive &ar, const char *charName) {
		// std::cout<<"serialize_or_throw XMLOutputArchive from DataInOutType<"<<typeid(T).name()<<"> with name "<<charName<<std::endl;
		if (!isOptional) {
			ar(cereal::make_nvp(charName, *static_cast<T *>(data.get())));
		} else {
			bool isPresent = cereal::make_optional_nvp(ar, charName, *static_cast<T *>(data.get()));
			// bool isPresent = true;
			if (!isPresent) {
				ExceptionOptionalNVPnotFound exceptionNVP;
				throw exceptionNVP;
				return false;
			}
		}
		return true;
	}

	bool serialize_or_throw(cereal::XMLInputArchive &ar, const char *charName) {
		// std::cout<< "serialize_or_throw XMLInputArchive from DataInOutType<"<<typeid(T).name()<<"> with name "<<charName<<std::endl;
		if (!isOptional) {
			ar(cereal::make_nvp(charName, *static_cast<T *>(data.get())));
		} else {
			bool isPresent = cereal::make_optional_nvp(ar, charName, *static_cast<T *>(data.get()));
			// bool isPresent = true;
			if (!isPresent) {
				ExceptionOptionalNVPnotFound exceptionNVP;
				throw exceptionNVP;
				return false;
			}
		}
		return true;
	}
};


class MapInOutClass {
 private:
	std::map<std::string, std::shared_ptr<DataInOut>> m_mapioM;

 public:
	template <typename T>
	void set_copy_data(std::string key, T *var, bool optional = false) {
		if (m_mapioM.count(key) > 0) {
			std::string buf("The MapInOut already have the key: ");
			buf.append(key);
			throw std::invalid_argument(buf);
		}
		std::shared_ptr<DataInOutType<T>> dataIoPtr(new DataInOutType<T>);
		dataIoPtr->type = typeid(*var);
		dataIoPtr->data = std::make_shared<T>();
		*dataIoPtr->data = *var;
		dataIoPtr->sizeT = sizeof(T);
		dataIoPtr->isOptional = optional;
		dataIoPtr->hasTypeInitialized = true;
		dataIoPtr->hasPtrInitialized = true;
		m_mapioM.insert(std::pair<std::string, std::shared_ptr<DataInOutType<T>>>(key, dataIoPtr));
	}

	template <typename T>
	void set_store_address(std::string key, T *var, bool optional = false) {
		if (m_mapioM.count(key) > 0) {
			std::string buf("The MapInOut already have the key: ");
			buf.append(key);
			throw std::invalid_argument(buf);
		}
		std::shared_ptr<DataInOutType<T>> dataIoPtr(new DataInOutType<T>);
		dataIoPtr->type = typeid(*var);
		dataIoPtr->data = std::make_shared<T>();
		dataIoPtr->data.reset(var);
		dataIoPtr->sizeT = sizeof(T);
		dataIoPtr->isOptional = optional;
		dataIoPtr->hasTypeInitialized = true;
		dataIoPtr->hasPtrInitialized = true;
		m_mapioM.insert(std::pair<std::string, std::shared_ptr<DataInOutType<T>>>(key, dataIoPtr));
	}

	template <typename T>
	void set_copy_data(std::string key, std::shared_ptr<T> var, bool optional = false) {
		if (m_mapioM.count(key) > 0) {
			std::string buf("The MapInOut already have the key: ");
			buf.append(key);
			throw std::invalid_argument(buf);
		}
		std::shared_ptr<DataInOutType<T>> dataIoPtr(new DataInOutType<T>);
		dataIoPtr->type = typeid(*var.get());
		dataIoPtr->data = std::make_shared<T>(*var.get());
		// *dataIoPtr->data = ;
		dataIoPtr->sizeT = sizeof(T);
		dataIoPtr->isOptional = optional;
		dataIoPtr->hasTypeInitialized = true;
		dataIoPtr->hasPtrInitialized = true;
		m_mapioM.insert(std::pair<std::string, std::shared_ptr<DataInOutType<T>>>(key, dataIoPtr));
	}

	template <typename T>
	void set_store_address(std::string key, std::shared_ptr<T> var, bool optional = false) {
		if (m_mapioM.count(key) > 0) {
			std::string buf("The MapInOut already have the key: ");
			buf.append(key);
			throw std::invalid_argument(buf);
		}
		std::shared_ptr<DataInOutType<T>> dataIoPtr(new DataInOutType<T>);
		dataIoPtr->type = typeid(*var.get());
		dataIoPtr->data = var;
		dataIoPtr->sizeT = sizeof(T);
		dataIoPtr->isOptional = optional;
		dataIoPtr->hasTypeInitialized = true;
		dataIoPtr->hasPtrInitialized = true;
		m_mapioM.insert(std::pair<std::string, std::shared_ptr<DataInOutType<T>>>(key, dataIoPtr));
	}

	template <typename T>
	void new_with_type(std::string key, bool optional = false) {
		if (m_mapioM.count(key) > 0) {
			std::string buf("The MapInOut already have the key: ");
			buf.append(key);
			throw std::invalid_argument(buf);
		}
		std::shared_ptr<DataInOutType<T>> dataIoPtr(new DataInOutType<T>);
		dataIoPtr->type = typeid(T);
		dataIoPtr->data = std::make_shared<T>();
		dataIoPtr->sizeT = sizeof(T);
		dataIoPtr->isOptional = optional;
		dataIoPtr->hasTypeInitialized = true;
		dataIoPtr->hasPtrInitialized = false;
		m_mapioM.insert(std::pair<std::string, std::shared_ptr<DataInOutType<T>>>(key, dataIoPtr));
	}

	template <typename T>
	void change_address(std::string key, T *var) {
		if (m_mapioM.count(key) == 0) {
			std::string buf("The MapInOut does not have the key: ");
			buf.append(key);
			throw std::invalid_argument(buf);
		}
		std::type_index a = m_mapioM.at(key).get()->type;
		std::type_index b = std::type_index(typeid(T));
		if (a == b) {
			m_mapioM.at(key).get()->data.reset(var); // change the address to the one given.
		} else {
			std::string buf("You are asking for setting a type ");
			buf.append(typeid(T).name());
			buf.append(" while the type behind the key ");
			buf.append(key);
			buf.append(" is ");
			buf.append(m_mapioM.at(key).get()->type.name());
			throw std::invalid_argument(buf);
		}
	}

	template <typename T>
	void change_address(std::string key, std::shared_ptr<T> var) {
		if (m_mapioM.count(key) == 0) {
			std::string buf("The MapInOut does not have the key: ");
			buf.append(key);
			throw std::invalid_argument(buf);
		}
		std::type_index a = m_mapioM.at(key).get()->type;
		std::type_index b = std::type_index(typeid(T));
		if (a == b) {
			m_mapioM.at(key).get()->data = var; // change the address to the one given.
		} else {
			std::string buf("You are asking for setting a type ");
			buf.append(typeid(T).name());
			buf.append(" while the type behind the key ");
			buf.append(key);
			buf.append(" is ");
			buf.append(m_mapioM.at(key).get()->type.name());
			throw std::invalid_argument(buf);
		}
	}

	template <typename T>
	void copy_data(std::string key, T *var) { // insert que la data
		if (m_mapioM.count(key) == 0) {
			std::string buf("The MapInOut does not have the key: ");
			buf.append(key);
			throw std::invalid_argument(buf);
		}
		std::type_index a = m_mapioM.at(key).get()->type;
		std::type_index b = std::type_index(typeid(T));
		if (a == b) {
			*((T *)m_mapioM.at(key).get()->data.get()) = *var; // copy
		} else {
			std::string buf("You are asking for setting a type ");
			buf.append(typeid(T).name());
			buf.append(" while the type behind the key ");
			buf.append(key);
			buf.append(" is ");
			buf.append(m_mapioM.at(key).get()->type.name());
			throw std::invalid_argument(buf);
		}
	}

	int change_address_svptr(std::string key, std::shared_ptr<void> inputPtr) {
		int errorCode = -1;
		if (m_mapioM.count(key) == 0) {
			std::string buf("The MapInOut does not have the key: ");
			buf.append(key);
			throw std::invalid_argument(buf);
			errorCode = 1;
			return errorCode;
		}
		m_mapioM.at(key).get()->data = inputPtr; // copy the address
		errorCode = 0;
		return errorCode;
	}

	int copy_data_svptr(std::string key, std::shared_ptr<void> inputPtr) {
		int errorCode = -1;
		if (m_mapioM.count(key) == 0) {
			std::string buf("The MapInOut does not have the key: ");
			buf.append(key);
			throw std::invalid_argument(buf);
			errorCode = 1;
			return errorCode;
		}
		// *m_mapioM.at(key).get()->data.get() = *inputPtr.get(); // copy the data from an address to another
		// cout << key << " sizeof "<< m_mapioM.at(key).get()->sizeT  << endl;
		memcpy(m_mapioM.at(key).get()->data.get(), inputPtr.get(), m_mapioM.at(key).get()->sizeT);
		errorCode = 0;
		return errorCode;
	}

	template <typename T>
	T *get(std::string key) {
		if (m_mapioM.count(key) == 0) {
			std::string buf("The MapInOut does not have the key: ");
			buf.append(key);
			throw std::invalid_argument(buf);
		}
		// if (!m_mapioM.at(key).get()->data){
		// 	cout << "GET ptr is not a valid pointer." << key << endl;
		// }
		std::type_index a = m_mapioM.at(key).get()->type;
		std::type_index b = std::type_index(typeid(T));
		if (a == b) {
			T *object = static_cast<T *>(m_mapioM.at(key).get()->data.get());
			return object;
		} else {
			std::string buf("You are asking for getting a type ");
			buf.append(typeid(T).name());
			buf.append(" while the type behind the key ");
			buf.append(key);
			buf.append(" is ");
			buf.append(m_mapioM.at(key).get()->type.name());
			throw std::invalid_argument(buf);
		}
	}

	std::shared_ptr<void> get_void_ptr(std::string key) {
		if (m_mapioM.count(key) == 0) {
			std::string buf("The MapInOut does not have the key: ");
			buf.append(key);
			throw std::invalid_argument(buf);
		}
		return m_mapioM.at(key).get()->data;
	}

	std::string get_type_str(std::string key) {
		if (m_mapioM.count(key) == 0) {
			std::string buf("The MapInOut does not have the key: ");
			buf.append(key);
			throw std::invalid_argument(buf);
		}
		return m_mapioM.at(key).get()->type.name();
	}

	std::type_index get_type_index(std::string key) {
		if (m_mapioM.count(key) == 0) {
			std::string buf("The MapInOut does not have the key: ");
			buf.append(key);
			throw std::invalid_argument(buf);
		}
		return m_mapioM.at(key).get()->type;
	}

	bool has_type_initialized(std::string key) {
		if (m_mapioM.count(key) == 0) {
			std::string buf("The MapInOut does not have the key: ");
			buf.append(key);
			throw std::invalid_argument(buf);
		}
		return m_mapioM.at(key).get()->hasTypeInitialized;
	}

	bool has_ptr_initialized(std::string key) {
		if (m_mapioM.count(key) == 0) {
			std::string buf("The MapInOut does not have the key: ");
			buf.append(key);
			throw std::invalid_argument(buf);
		}
		return m_mapioM.at(key).get()->hasPtrInitialized;
	}

	bool is_optional(std::string key) {
		if (m_mapioM.count(key) == 0) {
			std::string buf("The MapInOut does not have the key: ");
			buf.append(key);
			throw std::invalid_argument(buf);
		}
		return m_mapioM.at(key).get()->isOptional;
	}

	size_t get_type_size(std::string key) {
		if (m_mapioM.count(key) == 0) {
			std::string buf("The MapInOut does not have the key: ");
			buf.append(key);
			throw std::invalid_argument(buf);
		}
		return m_mapioM.at(key).get()->sizeT;
	}

	int get_size_map() { return m_mapioM.size(); }

	template <class Archive>
	bool ask_to_serialize(Archive &ar, std::string key) {
		// std::cout << "ask2serialize" << typeid(ar).name() << std::endl;
		const char *charName = key.c_str();
		// verifications ...
		bool isFound = m_mapioM.at(key).get()->serialize_or_throw(ar, charName);
		return isFound;
	}

	int count(std::string key) { return m_mapioM.count(key); }

	std::map<std::string, std::shared_ptr<DataInOut>>::iterator begin() { return m_mapioM.begin(); }

	std::map<std::string, std::shared_ptr<DataInOut>>::iterator end() { return m_mapioM.end(); }

	~MapInOutClass(){};
};

} // namespace OBLiX

#endif // MAPIOCLASS_HPP

// #ifdef __cplusplus
// } // end extern "C"
// #endif

// EoF