// C++ program to execute commands oject for simulations.
/*
	// Library Name: OBLiX (OBject Library eXecutor)
	Version: 0.0.a
	Date: 1/02/2021
	Description:
	Input: XML, JSON file or commands in terminal mode
	Authors:
		Romain NOËL (romain.noel@univ-eiffel.fr)
	License:
		BSD 2-Clause License
		It implies
*/

// #ifdef __cplusplus
// extern "C" {
// #endif

#ifndef OBLIX_HPP
#define OBLIX_HPP

// Import modules.
#include <algorithm> // std::find
#include <fstream> // std::ifstream file
#include <iostream> // std::cout
#include <map> // std::map
#include <sstream> // std::streamstring
#include <string> // std::string
#include <tuple> // std::tuple
#include <vector> // std::vector

// libraries
#undef CEREAL_XML_STRING_VALUE
#define CEREAL_XML_STRING_VALUE "OBLiX_XML_InputFile" // Cereal
#include <cereal/archives/xml.hpp>
#include <cereal/archives/json.hpp>
#include <cereal/types/vector.hpp>
#include "cxxopts.hpp" // prompt command option parser
#include "OBLiX/shareLog.hpp" // OBLiX + spdlog # Have to be before spdlog/fmt/ostr
#include "spdlog/fmt/ostr.h" // must be included to log object

// Home-made modules
#include "OBLiX/usefulTools.hpp"
#include "OBLiX/version.hpp"
#include "OBLiX/baseX.hpp"


// NOLINTNEXTLINE
namespace OBLiX {

/*! List of ResultName attributes from the baseX::mapResultName2Object. */
std::vector<std::string> listResultName;

/**
 * @brief Execute a list of runnable objects from an Cereal archive.
 *
 * @tparam Archive
 * @param arch
 * @return int error code: an error occurred if the returned code is non-zero.
 */
template <class Archive>
int exec_from_archive(Archive &arch) {
	//
	int errorCode = 0;
	std::shared_ptr<spdlog::logger> pLog = shareLog::get_plog();

	//
	pLog->info("Reading input.");
	// Initialize and scroll down the input file.
	std::map<int, std::tuple<std::string, std::string>> mapInput; // mapInput (id, className, resultName)    // , std::string  //, flag
	int i = 1;
	while (true) {
		OBLiX::BaseX dummyParentObject;
		const auto namePtr = arch.getNodeName();

		if (!namePtr)
			break;

		std::string className = namePtr;
		arch(dummyParentObject);
		// Check that the resultName is unique.
		if (std::find(listResultName.begin(), listResultName.end(), dummyParentObject.resultName) != listResultName.end()) {
			pLog->error("exec_from_archive: the resultName {} is not unique.", dummyParentObject.resultName);
			pLog->error("exec_from_archive: Please give a unique resultName for "
							"every input object.");
			errorCode = 1;
			return (errorCode);
		} else {
			listResultName.push_back(dummyParentObject.resultName);
			mapInput[i] = std::make_tuple(className, dummyParentObject.resultName);
			++i;
		} // endif
	} // end while

	// Loop over the mapInput entries to catch the associated object.
	pLog->info("Creating list of object to run.");
	for (auto const &[key, val] : mapInput) {
		// OBLiX::mapResultName2Object["resultName"]=mapCreatorUnique["className"]();
		OBLiX::mapResultName2Object[std::get<1>(val)] = OBLiX::get_factory().build_new(std::get<0>(val));
		auto p = OBLiX::mapResultName2Object[std::get<1>(val)].get();
		p->downcast_serialize(arch, p);
		// print in debug the input object received.
		pLog->debug("Object received with no.{}, class {}, name {}: \n {}", key, std::get<0>(val), std::get<1>(val),
			 OBLiX::FmtWrap<OBLiX::BaseX>(p));
	} // end for

	// Loop over the mapInput entries to catch the associated object.
	pLog->info("Initializing the objects in the list to run.");
	for (auto const &[key, val] : mapInput) {
		// Check the order of execution
		// if (key == key) {
		// Do nothing for now, since the order is fake !!
		// };
		// get pointer and execute the object function.
		auto p = OBLiX::mapResultName2Object[std::get<1>(val)].get();
		errorCode = p->init_base();
	} // end for

	// Loop over the mapInput entries to execute the associated object.
	pLog->info("Start to eXecute the list of object. Let's go !");
	for (auto const &[key, val] : mapInput) {
		// Check the order of execution
		// if (key == key) {
		// Do nothing for now, since the order is fake !!
		// };
		// get pointer and execute the object function.
		auto p = OBLiX::mapResultName2Object[std::get<1>(val)].get();
		errorCode = p->run_base();
	} // end for

	pLog->info("End of the list of object eXecutions with returnCode {}.", errorCode);
	return (errorCode);
} // end exec_from_archive

/**
 * @brief Take a list of runnable object from a file (xml, json, yaml), turn it
 * 	into an cereal archive, then and pass it to 'exec_from_archive' function.
 *
 * @param fileName the file name (must ends with .xml or .json or .yaml).
 * @return int error code: an error occurred if the returned code is non-zero.
 */
int exec_from_file(std::string fileName) {
	int errorCode = 0;
	auto pLog = shareLog::get_plog();

	std::ifstream is(fileName, std::ios::binary);
	if (!is) {
		std::cout << "error: opening file " << fileName << std::endl;
	}

	if (endsWith(fileName, ".xml")) { // XML
		pLog->trace("XML input file detected.");
		cereal::XMLInputArchive arch(is);
		errorCode = exec_from_archive(arch);
	} else if (endsWith(fileName, ".json")) { // JSON
		pLog->trace("JSON input file detected.");
		cereal::JSONInputArchive arch(is);
		errorCode = exec_from_archive(arch);
	}
	// else if (endsWith(fileName, ".yaml"))
	// { // YAML
	// 	cereal::YAMLInputArchive arch(is);
	// }
	else {
		pLog->error("Unknown input file extension:\n "
						"\t file: {} not ending with .xml or .json !",
			 fileName);
		errorCode = 11;
	}

	return (errorCode);
} // end exec_from_file

/**
 * @brief Function that listen instruction from prompt and try to turn it into a
 * 	 Cereal archive before to pass it to 'exec_from_archive' function.
 *
 * @return int error code: an error occurred if the returned code is non-zero.
 */
int exec_from_prompt() {
	//

	// Init variables.
	int errorCode = 0;
	static bool s_continueFlag = true;
	std::string line;
	std::string buffer = "";
	unsigned counterNodeChild = 0;
	std::string nodeTag = "";
	bool continueXML = false;
	bool continueJSON = false;
	bool continueYAML = false;
	bool firstInput = true;
	bool firstCatch = false;
	std::string preLine = " > ";
	auto pLog = shareLog::get_plog();

	//
	pLog->info("** Entering prompt mode **:\n"
				  "   please type 'exit' to quit OBLiX at any moment, \n"
				  "    or 'readfile <path>' to execute a OBLiX input file,\n"
				  "    otherwise start by precising the format you will use for "
				  "instructions ('xml', 'json' or 'yaml').\n"
				  "    After choosing a format you can change using 'changeformat "
				  "<format>'.");

	// for (std::string line; std::getline(std::cin, line);) {
	while (s_continueFlag) { // getline until finished.
		std::cout << preLine;
		std::getline(std::cin, line);
		// std::cout << line << std::endl;

		if (line == "exit") { // switch (made of if) to read keywords
			s_continueFlag = false;
			firstCatch = true;
		} else if (startsWith(line, "readfile")) {
			firstCatch = true;
			std::string fileName = line.substr(9); // get from position to the end
			std::ifstream is(fileName);
			if (is.good()) {
				errorCode = exec_from_file(fileName);
			} else {
				pLog->error("The file '{}' is not valid (eofbit, failbit, badbit etc).", fileName);
			} // end if good
		} else if (startsWith(line, "changeformat")) {
			firstCatch = true;
			std::string formatName = line.substr(12);
			if (endsWith(formatName, "xml")) {
				continueXML = true;
				continueJSON = false;
				continueYAML = false;
				buffer = "";
				preLine = "x| ";
				counterNodeChild = 0;
			} else if (endsWith(formatName, "json")) {
				continueXML = false;
				continueJSON = true;
				continueYAML = false;
				buffer = "";
				preLine = "j| ";
				counterNodeChild = 0;
			} else if (endsWith(formatName, "yaml")) {
				continueXML = false;
				continueJSON = false;
				continueYAML = true;
				buffer = "";
				preLine = "y| ";
				counterNodeChild = 0;
			} else {
				pLog->error("The format '{}' is not valid (xml, json, yaml).", formatName);
			} // end if format
		} // end if primary

		if (firstCatch) {
			continue;
		} else if ((line == "xml" && firstInput) || continueXML) { // secondary if.
			// std::cout << "I got XML" << std::endl	;
			if (firstInput) { //
				firstInput = false;
				preLine = "x| "; // std::string(counterNodeChild, " | " ); // repeat as deep as necessary
				continueXML = true;
				continue;
			}

			if (counterNodeChild == 0) { // new node
				if (line.find("<") == std::string::npos) {
					pLog->error("expecting an XML node starting with <tag>.");
					continue;
				}
				unsigned first = line.find("<");
				unsigned last = line.find(">");
				if (line.substr(first + 1, first + 2) == "?") {
					continue; // if the special <?xml tag is found skip.
				}
				counterNodeChild += 1;
				nodeTag = "</" + line.substr(first + 1, last - first); // +">" not needed.
				std::cout << "nodeTag:" << nodeTag << std::endl;
			} // end if new node

			buffer.append(line);

			if (line.find(nodeTag) != std::string::npos) { // end of node
				std::cout << "nodeTag FOUND" << nodeTag << std::endl;
				counterNodeChild -= 1;

				// cout<< "buffer"<< buffer <<endl;
				if (!startsWith(buffer, "<?xml version=")) { // if the special XML beginning tag is not present insert it
					buffer.insert(0, "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
				}

				// cout<< "buffer"<< buffer <<endl;
				if (!startsWith(buffer, CEREAL_XML_STRING_VALUE)) { // if the special XML CEREAL tag is not present insert it
					std::string extraMarker;
					extraMarker = "\n<";
					extraMarker.append(CEREAL_XML_STRING_VALUE).append(">\n");
					unsigned first = buffer.find(">");
					buffer.insert(first + 1, extraMarker);
					extraMarker = "\n</";
					extraMarker.append(CEREAL_XML_STRING_VALUE).append(">");
					buffer.append(extraMarker);
				}
				// cout<< "buffer"<< buffer <<endl;
				std::stringstream ss(buffer);
				buffer = "";
				nodeTag = "";
				cereal::XMLInputArchive arch(ss);
				errorCode = exec_from_archive(arch);
			} // end if end of the node
		} // end if xml
		else if ((line == "json" && firstInput) || continueJSON) {
			// std::cout << "I got JSON" << std::endl;
			if (firstInput) { //
				firstInput = false;
				preLine = "j| "; // std::string(counterNodeChild, " | " ) ; //repeat as deep as necessary
				continueJSON = true;
				continue;
			}

			if (counterNodeChild == 0) { // new node
				if (line.find("{") == std::string::npos) {
					pLog->error("expecting a JSON node starting with '{'.");
					continue;
				}
			} // end if new node
			if (nodeTag == "" && counterNodeChild <= 1 && line.find(":") != std::string::npos) {
				// get the tag is not before
				unsigned first = line.find(":");
				nodeTag = line.substr(0, first);
			}

			buffer.append(line);

			// if (line.find("{")!= std::string::npos ){ //
			// 	counterNodeChild+=1;
			// }
			int numItems1 = std::count(line.begin(), line.end(), '{');
			counterNodeChild += numItems1;
			// if (line.find("}")!= std::string::npos ){ //
			// 	counterNodeChild-=1;
			// }
			numItems1 = std::count(line.begin(), line.end(), '}');
			counterNodeChild -= numItems1;

			if (counterNodeChild == 0 && buffer != "") { // end of node
				std::cout << "nodeTag FOUND:" << nodeTag << std::endl;

				// // cout<< "buffer"<< buffer <<endl;
				numItems1 = buffer.find("{");
				std::string subString = buffer.substr(0, numItems1);
				if (subString.find(":") != std::string::npos) { // if there is ':' before '{' it means the root node is not present.
					buffer.insert(0, "{");
					buffer.append("}");
				}

				std::cout << "buffer:" << buffer << std::endl;
				std::stringstream ss(buffer);
				buffer = "";
				nodeTag = "";
				cereal::JSONInputArchive arch(ss);
				errorCode = exec_from_archive(arch);
			} // end if end of the node
		} // end if json
		else if ((line == "yaml" && firstInput) || continueYAML) {
			std::cout << "I got YAML" << std::endl;
		} // end if yaml
		// else if ((line == "toml" && firstInput) || continueTOML) {
		// 	std::cout << "I got TOML" << std::endl;
		// } // end if yaml
		else {
			pLog->error("The command '{}' is unknown.", line);
		} // end (if) switch read keywords
	}

	pLog->info("** Exit prompt mode **");
	return (errorCode);
} // end exec_from_prompt


struct InternalVariables {
	// Init variables.
	std::string inputFileName;
	std::string logFileName = "OBLiX.log";
	int logLevel = 2;
	int consoleLevel = 2; // -1= off, 0= critical, err,	warn,	info,	debug, 5= trace
	bool inputFileIsGiven = false;
	bool noLogFile = false;
	bool flagVersion = false;
} oblixInterns;


// NOLINTNEXTLINE
int parse_args(int argc, const char *argv[]) {
	// Initialize variables
	int errorCode = -1;

	// Parse prompt options.
	try {
		cxxopts::Options options(argv[0], " -example: oblix -i input.xml -l my_log.txt -v");
		options.positional_help("[optional args]").show_positional_help();

		options.set_width(77).set_tab_expansion().add_options() //.
			 ("i,input",
				  "Input file containing object/command to run."
				  "\n The file format should be in XML, JSON or YAML.",
				  cxxopts::value<std::string>(), "IN_FILE") //.
			 ("o,output", "Output file.", cxxopts::value<std::string>()->default_value("a.out"), "BIN") //.
			 ("l,log", "Log file", cxxopts::value<std::string>()->default_value("OBLiX.log"), "LOG") //.
			 ("L,loglevel",
				  "Level of verbosity for the log file."
				  "\n(off=-1, critical=0, err=1, warn=2, info=3, debug=4, trace= 5)",
				  cxxopts::value<int>(), "N") //.
			 ("P,promptlevel", "Level of verbosity for the prompt terminal.", cxxopts::value<int>(), "N") //.
			 ("n,nolog", "Disable the log file.")("h,help", "Print help.") // .
			 ("V,version", "Print version (requires verbosity info at least).") // .
			 ("v,verbose", "The more, the more verbosely the run is (default is warning).") // .
			 ("d,debug", "Enable debugging: it is equivalent to option -vv.") // a bool parameter
			 ("q,quiet", "Run in silence mode.") // a bool parameter
			 ;

		auto resultParse = options.parse(argc, argv);

		if (resultParse.count("help")) {
			std::cout << options.help({"", "Group"}) << std::endl;
			exit(0);
		}

		if (resultParse.count("input")) {
			oblixInterns.inputFileName = resultParse["input"].as<std::string>();
			oblixInterns.inputFileIsGiven = true;
		}

		if (resultParse.count("output")) {
			std::cout << "Output = " << resultParse["output"].as<std::string>() << std::endl;
		}

		if (resultParse.count("log")) {
			oblixInterns.logFileName = resultParse["log"].as<std::string>();
		}

		if (resultParse.count("nolog")) {
			oblixInterns.noLogFile = true;
		}

		if (resultParse.count("version")) {
			oblixInterns.flagVersion = true;
		}

		if (resultParse.count("debug")) {
			oblixInterns.consoleLevel = 4;
			oblixInterns.logLevel = 4;
		}

		if (resultParse.count("quiet")) {
			oblixInterns.consoleLevel = 0;
			oblixInterns.logLevel = 0;
		}

		if (resultParse.count("verbose")) {
			oblixInterns.consoleLevel = oblixInterns.consoleLevel + resultParse.count("verbose");
			oblixInterns.logLevel = oblixInterns.logLevel + resultParse.count("verbose");
		}

		if (resultParse.count("loglevel")) {
			oblixInterns.logLevel = resultParse["loglevel"].as<int>();
		}

		if (resultParse.count("promptlevel")) {
			oblixInterns.consoleLevel = resultParse["promptlevel"].as<int>();
		}

		std::cout << "Arguments remain = " << argc << std::endl;

		auto arguments = resultParse.arguments();
		std::cout << "Saw " << arguments.size() << " arguments" << std::endl;
	} catch (const cxxopts::OptionException &e) {
		std::cerr << "ERROR parsing options: " << e.what() << std::endl;
		exit(1);
	}
	return errorCode;
} // end OBLiX_parse_args


// NOLINTNEXTLINE
int setup_logger() {
	// Initialize variables
	int errorCode = -1;

	// Setup logger
	if (oblixInterns.noLogFile) { // no file_sink
		auto consoleSink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
		consoleSink->set_pattern("[%H:%M:%S pid.%P:%n.%@][%^%=8l%$] %v");
		switch (oblixInterns.consoleLevel) {
		case -1:
			consoleSink->set_level(spdlog::level::off);
			break;
		case 0:
			consoleSink->set_level(spdlog::level::critical);
			break;
		case 1:
			consoleSink->set_level(spdlog::level::err);
			break;
		case 2:
			consoleSink->set_level(spdlog::level::warn);
			break;
		case 3:
			consoleSink->set_level(spdlog::level::info);
			break;
		case 4:
			consoleSink->set_level(spdlog::level::debug);
			break;
		case 5:
			consoleSink->set_level(spdlog::level::trace);
			break;
		default:
			std::cerr << "ERROR consoleLevel: " << oblixInterns.consoleLevel << " is not in range [-1,5]" << std::endl;
			break;
		}

		spdlog::logger logger("OBLiX", {consoleSink});
		logger.set_level(spdlog::level::trace); // to do not bypass the previous setting
		// register it if you need to access it globally
		auto pLog = shareLog::setup_logger(logger);
	} else { // noLogFile=false
		auto consoleSink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
		consoleSink->set_pattern("[%H:%M:%S pid.%P:%n.%@][%^%=8l%$] %v");
		switch (oblixInterns.consoleLevel) {
		case -1:
			consoleSink->set_level(spdlog::level::off);
			break;
		case 0:
			consoleSink->set_level(spdlog::level::critical);
			break;
		case 1:
			consoleSink->set_level(spdlog::level::err);
			break;
		case 2:
			consoleSink->set_level(spdlog::level::warn);
			break;
		case 3:
			consoleSink->set_level(spdlog::level::info);
			break;
		case 4:
			consoleSink->set_level(spdlog::level::debug);
			break;
		case 5:
			consoleSink->set_level(spdlog::level::trace);
			break;
		default:
			std::cerr << "ERROR consoleLevel: " << oblixInterns.consoleLevel << " is not in range [-1,5]" << std::endl;
			break;
		}

		auto fileSink = std::make_shared<spdlog::sinks::basic_file_sink_mt>(oblixInterns.logFileName, true);
		fileSink->set_pattern("[%H:%M:%S.%e pid.%P:%n.%@][%^%=8l%$] %v");
		// %H = hours 24h format
		// %M = minutes
		// %S = second
		// %e = millisecond (%f micro, %F nano)
		// %P = Process ID (%t thread)
		// %n = Logger's name (here is OBLiX)
		// %@ = Source file and line (use SPDLOG_TRACE(..), SPDLOG_INFO(...) etc. instead of spdlog::trace(...)
		// %^ = start color range
		// %$ = end color range
		// %=8l = log level with a centered length of 8
		// %v = The actual text to log
		switch (oblixInterns.logLevel) {
		case -1:
			fileSink->set_level(spdlog::level::off);
			break;
		case 0:
			fileSink->set_level(spdlog::level::critical);
			break;
		case 1:
			fileSink->set_level(spdlog::level::err);
			break;
		case 2:
			fileSink->set_level(spdlog::level::warn);
			break;
		case 3:
			fileSink->set_level(spdlog::level::info);
			break;
		case 4:
			fileSink->set_level(spdlog::level::debug);
			break;
		case 5:
			fileSink->set_level(spdlog::level::trace);
			break;
		default:
			std::cerr << "ERROR fileloglevel: " << oblixInterns.logLevel << " is not in range [-1,5]" << std::endl;
			break;
		}

		spdlog::logger logger("OBLiX", {consoleSink, fileSink});
		logger.set_level(spdlog::level::trace); // to do not bypass the previous setting
		// register it if you need to access it globally
		auto pLog = shareLog::setup_logger(logger);
	}
	errorCode = 0;
	return errorCode;
} // end OBLiX_setup_logger


// NOLINTNEXTLINE
int read_and_run() {
	// Initialize variables
	int errorCode = -1;

	// Get the pointer logger from the previous setup.
	auto pLog = shareLog::get_plog();

	// Now start in a try loop.
	try {

		pLog->info("OBLiX Read & Run: (Start)");

		if (oblixInterns.flagVersion) { // if --version options is given print versions.
			print_version();
		}

		if (oblixInterns.inputFileIsGiven) { // run file or prompt
			errorCode = exec_from_file(oblixInterns.inputFileName);
		} else { //
			errorCode = exec_from_prompt();
		}

		// End of the OBLiX_read_and_run.
		pLog->info("OBLiX Read & Run: (End) {}.", errorCode);

	} catch (const std::bad_alloc &e) {
		pLog->error("Bad Alloc:{}", e.what());
	} catch (const std::exception &e) {
		pLog->error(e.what());
	} catch (std::string &s) {
		pLog->error(s);
	} catch (...) {
		pLog->error("Unknown exception in OBLiX Read & Run! ");
	} // end try

	return (errorCode);
} // end OBLiX_read_and_run


/**
 * @brief
 *
 * @class Class enabling the fall back on CLI mode after executing a file after
 * 	executing main from file mode.
 */
class ContinueBRC : public OBLiX::Adapt<ContinueBRC> {
 public:
	std::string get_classname() const { return "Continue"; };

	template <class Archive>
	void serialize(Archive &ar) {
		ar(CEREAL_NVP(resultName));
	}

	int run() {
		int errorCode = 0;
		errorCode = exec_from_prompt();
		return (errorCode);
	}

	~ContinueBRC(){}; // Destructor
};

// Register continue and base.
#ifndef SWIG
static const bool kRegisteredBaseRun = OBLiX::get_factory().register_builder("BaseRun", OBLiX::unique_creator<OBLiX::BaseX>);
static const bool kRegisteredContinue = OBLiX::get_factory().register_builder("Continue", OBLiX::unique_creator<OBLiX::ContinueBRC>);
#else
extern bool kRegisteredBaseRun;
extern bool kRegisteredContinue;
#endif

} // end namespace OBLiX
#endif // OBLIX_HPP
// #ifdef __cplusplus
// } // end extern "C"
// #endif

// EoF