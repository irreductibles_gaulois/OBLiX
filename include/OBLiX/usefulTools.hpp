/**
 * @file usefulTools.cpp
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2021-09-09
 *
 * @copyright Copyright (c) 2021
 *
 */


// #ifdef __cplusplus
// extern "C" {
// #endif

#ifndef USEFUL_TOOLS_HPP
#define USEFUL_TOOLS_HPP

#include <string>
#include <filesystem>
#include <iostream>

/**
 * @brief
 *
 * @param a
 * @param b
 * @return true
 * @return false
 */
// NOLINTNEXTLINE
bool cmpCaseInsens(const std::string &a, const std::string &b) {
	// Case insensitive Compare string.
	unsigned int sz = a.size();
	if (b.size() != sz)
		return false;
	for (unsigned int i = 0; i < sz; ++i)
		if (tolower(a[i]) != tolower(b[i]))
			return false;
	return true;
}

/**
 * @brief
 *
 * @param str
 * @param suffix
 * @return true
 * @return false
 */
// NOLINTNEXTLINE
bool endsWith(const std::string &str, const std::string &suffix) {
	return str.size() >= suffix.size() && 0 == str.compare(str.size() - suffix.size(), suffix.size(), suffix);
}

/**
 * @brief
 *
 * @param str
 * @param prefix
 * @return true
 * @return false
 */
// NOLINTNEXTLINE
bool startsWith(const std::string &str, const std::string &prefix) {
	return str.size() >= prefix.size() && 0 == str.compare(0, prefix.size(), prefix);
}

/**
 * @brief
 *
 * @param str
 * @param width
 * @param showEllipsis
 * @return std::string
 */
// NOLINTNEXTLINE
std::string truncateStr(std::string str, size_t width, bool showEllipsis) {
	if (str.length() > width) {
		if (showEllipsis)
			return str.substr(0, width) + "...";
		else
			return str.substr(0, width);
	}
	return str;
}

#endif // USEFULTOOLS_HPP

// #ifdef __cplusplus
// } // end extern "C"
// #endif
