/**
 * @file baseX.hpp
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2021-09-24
 *
 * @copyright Copyright (c) 2021
 *
 */

// #ifdef __cplusplus
// extern "C" {
// #endif

#ifndef BASE_X_HPP
#define BASE_X_HPP

#include <exception>
#include <map>

#include <cereal/cereal.hpp>
#include <cereal/archives/json.hpp>
#include <cereal/archives/xml.hpp>
#include <cereal/types/optional.hpp>

#include "OBLiX/shareLog.hpp" // SpdLog
#include "OBLiX/mapInOut.hpp"

// NOLINTNEXTLINE
namespace OBLiX {

// interface, definition latter in the header
inline int link_string_to_mapio(MapInOutClass *map, std::string key, std::string val);
template <class Archive>
inline bool serialize_mapin_or_posttreat(
	 Archive &ar, const char *charName, MapInOutClass *map, std::map<std::string, std::string> *saveString);

/**
 * @class
 *
 * @brief
 *
 */
class BaseX {
 private:
	/* data */
 public:
	std::string resultName = "unaffected yet";
	MapInOutClass mapOut;
	MapInOutClass mapIn;
	std::map<std::string, std::string> mapIO2postreat;
	bool isInit = false;
	bool isRunning = false;
	bool isFinished = false;
	virtual std::string get_classname() const { return "BaseX"; };

	int run_base() {
		// say we are running.
		int errorCode = -1;
		isRunning = true;
		// exec the polymorphic run function (main function)
		errorCode = run();
		// say we stopped running.
		isFinished = true;
		isRunning = false;
		return errorCode;
	};

	virtual int run() {
		auto pLog = shareLog::get_plog();
		pLog->warn("Run BaseX");
		return 0;
	};

	template <class Archive>
	void serialize(Archive &ar) {
		ar(CEREAL_NVP(resultName));
	}

	// template <class Archive>
	virtual void downcast_serialize(cereal::XMLInputArchive &ar __attribute__((unused)), BaseX *pointer __attribute__((unused))) {
		auto pLog = shareLog::get_plog();
		pLog->warn("downcast_serialize XMLInputArchive Parent! ");
		return;
	};
	virtual void downcast_serialize(cereal::XMLOutputArchive &ar __attribute__((unused)), BaseX *pointer __attribute__((unused))) {
		auto pLog = shareLog::get_plog();
		pLog->warn("downcast_serialize XMLOutputArchive Parent! ");
		return;
	};
	virtual void downcast_serialize(cereal::JSONInputArchive &ar __attribute__((unused)), BaseX *pointer __attribute__((unused))) {
		auto pLog = shareLog::get_plog();
		pLog->warn("downcast_serialize JSONInputArchive Parent! ");
		return;
	};
	virtual void downcast_serialize(cereal::JSONOutputArchive &ar __attribute__((unused)), BaseX *pointer __attribute__((unused))) {
		auto pLog = shareLog::get_plog();
		pLog->warn("downcast_serialize JSONOutputArchive Parent! ");
		return;
	};

	int init_base() {
		// get local variables.
		int errorCode = -1;
		int tempErrorCode = -1;
		// Finish to initialize the mapIn
		errorCode = init_input();
		// Continue with the "user/developper" derived init (for mapOut and internal variables).
		tempErrorCode = init();
		errorCode = std::max(errorCode, tempErrorCode);
		// Say the init is done.
		isInit = true;
		return 0;
	};

	int init_input() {
		auto pLog = shareLog::get_plog();
		pLog->trace("init_input BaseX: {}.", resultName);
		for (auto const &[key, val] : mapIO2postreat) {
			pLog->trace("init_input mapIO2postreat: key={}, value={}.", key, val);
			link_string_to_mapio(&mapIn, key, val);
		} // end for
		// pLog->debug("init_input BaseX: {}.", mapIO2postreat);
		isInit = true;
		return 0;
	};

	virtual int init() { return 0; };

	virtual BaseX *clone() const { return new BaseX(*this); }

	virtual ~BaseX(){}; // Destructor
};

/**
 * @struct
 * @brief
 *
 */
// Template object used to create a virtual table for serializations that require a downcasting.
template <typename Derived>
struct Downcaster {
	template <class Archive>
	void serializer(Archive &arch, BaseX *pointer) {
		// std::cout << "downcast serial "<< pointer->get_classname() <<"."<< std::endl;
		const std::type_info &currTyp = typeid(*pointer); // store the typeid of the given pointer
		if (currTyp == typeid(Derived)) {
			Derived *newpointer = static_cast<Derived *>(pointer); // downcast
			arch(cereal::make_nvp(pointer->get_classname(), *newpointer)); // Serialize from the archive.
		} else {
			auto pLog = shareLog::get_plog();
			pLog->warn("downcast {}: bad typeid !!", pointer->get_classname());
		}

		return;
	}

	virtual ~Downcaster(){};
};

/**
 * @brief
 *
 * @tparam OStream
 * @param os
 * @param obj
 * @return OStream&
 */
template <typename OStream>
OStream &operator<<(OStream &os, const BaseX &obj) {
	// write obj to stream
	{
		cereal::JSONOutputArchive arr(os); // cereal::XMLOutputArchive arr(os);
		arr(obj);
	}
	return os;
}

/**
 * @brief
 *
 * @tparam IStream
 * @param is
 * @param obj
 * @return IStream&
 */
template <typename IStream>
IStream &operator>>(IStream &is, BaseX &obj) {
	// read obj from stream
	// if( /* no valid object of T found in stream */ )
	// 	is.setstate(std::ios::failbit);
	cereal::JSONInputArchive arr(is);
	arr(obj);
	return is;
}

/**
 * @brief
 *
 * @tparam OStream
 * @param os
 * @param obj
 * @return OStream&
 */
template <typename OStream>
OStream &operator<<(OStream &os, const BaseX *obj) {
	// write obj to stream
	cereal::JSONOutputArchive arr(os);
	BaseX *objCopy = obj->clone(); // clone to remove the const away.
	objCopy->downcast_serialize(arr, objCopy);
	return os;
}

/**
 * @brief
 *
 * @tparam IStream
 * @param is
 * @param obj
 * @return IStream&
 */
template <typename IStream>
IStream &operator>>(IStream &is, BaseX *obj) {
	// read obj from stream
	cereal::JSONInputArchive arr(is);
	obj->downcast_serialize(arr, obj);
	return is;
}

/**
 * @class
 * @brief
 *
 */
// Because spdlog use fmt and since version 8.0 fmt does not support pointer input,
// it is necessary to add a object to wrap the pointer and implement how fmt should deal with the pointer.
template <typename T>
class FmtWrap {
 public:
	explicit FmtWrap(const T *p) : m_p(p) {}

	template <typename OStream>
	friend OStream &operator<<(OStream &os, const FmtWrap wrapper) {
		return os << "{fmt} wrapper " << wrapper.m_p;
	}

 private:
	const T *m_p;
};

/**
 * @class
 *
 * @brief
 *
 */
// The Curiously Recurring Template Pattern (CRTP)
template <class T>
struct Adapt : public BaseX {
	// methods within Base can use template to access members of Derived
	std::string get_classname() const override { return "Adapt"; };

	// template <class Archive>
	// void serialize(Archive &ar) {
	// 	ar(CEREAL_NVP(resultName));
	// }

	template <class Archive>
	void serialize(Archive &ar) {
		// std::cout << "derived in" << isInit << isFinished << isRunning << std::endl;
		ar(CEREAL_NVP(resultName));
		// std::cout<< "out of OPTIONAL " << CEREAL_OPTIONAL_NVP(ar, resultName) << std::endl;

		CEREAL_OPTIONAL_NVP(ar, isInit);
		CEREAL_OPTIONAL_NVP(ar, isRunning);
		CEREAL_OPTIONAL_NVP(ar, isFinished);
		for (auto const &[key, val] : mapIn) {
			// std::cout << "serialize "<< typeid(ar).name() <<" "<< resultName <<" : "<< key << std::endl;
			serialize_mapin_or_posttreat(ar, key.c_str(), &mapIn, &mapIO2postreat);
		} // end for
		// std::cout << "derived out" << std::endl;
	}

	// Vtable function to replace the template virtual overriding.
	// clang-format off
	virtual void downcast_serialize(cereal::XMLInputArchive& 	ar, BaseX* p) override {Downcaster<T> down;	down.serializer(ar, p);};
	virtual void downcast_serialize(cereal::XMLOutputArchive& 	ar, BaseX* p) override {Downcaster<T> down;	down.serializer(ar, p);};
	virtual void downcast_serialize(cereal::JSONInputArchive& 	ar, BaseX* p) override {Downcaster<T> down;	down.serializer(ar, p);};
	virtual void downcast_serialize(cereal::JSONOutputArchive& 	ar, BaseX* p) override {Downcaster<T> down;	down.serializer(ar, p);};
	// clang-format on

	int run() override {
		auto pLog = shareLog::get_plog();
		pLog->warn("I am a BaseRunAdapt");
		return 0;
	}

	// int init_io() {
	// 	auto pLog = shareLog::get_plog();
	// 	pLog->warn("I am the init function");
	// 	std::cout << resultName << " mapIO2postreat size="<< mapIO2postreat.size() << endl;
	// 	return 0;
	// }

	using BaseX::BaseX;
	virtual BaseX *clone() const override { return new T(static_cast<T const &>(*this)); }

	virtual ~Adapt(){}; // Destructor
};


/*! Map of BaseX object store by resultName attributes. */
// extern
inline std::map<std::string, std::unique_ptr<BaseX>> mapResultName2Object;

// class ExceptionOptionalNVPnotFound: public exception (in MapInOutClass.hpp)

template <class Archive>
inline bool serialize_mapin_or_posttreat(
	 Archive &ar, const char *charName, MapInOutClass *map, std::map<std::string, std::string> *saveString) {
	// Get the logger.
	auto pLog = shareLog::get_plog();

	// THIS HAS TO GO IN THE SERIALIZE FUNCTION !!!
	// // Check if the map entry is initialized
	// if(map->isInitialised(nameVar) == false) {
	// 	// if it is not initialized, initialize it but warn the user !
	// 	pLog->warn("serialize_mapin_or_posttreat: the variable {} was not initialized in the mapInOut.", nameVar); //, map);
	// 	map->setType<T>(nameVar);
	// }

	//
	// const char* charName = nameVar.c_str();
	std::string str2try = "xxUninitializedStringXx";
	try {
		map->ask_to_serialize(ar, charName);
	} catch (cereal::RapidJSONException &e) { //
		// std::cerr << "serialize_mapin_or_posttreat: initialised catch: " << e.what() << '\n';
		if (!map->is_optional(charName)) {
			ar(cereal::make_nvp(charName, str2try));
		} else {
			bool isStillPresent = cereal::make_optional_nvp(ar, charName, str2try);
			if (!isStillPresent) {
				return true; // it should be ok since it a optional field
			}
		}
		if (str2try == "xxUninitializedStringXx") {
			pLog->error("serialize_mapin_or_posttreat: the variable {} has not found.", charName);
			return false;
		}
		saveString->insert(std::make_pair(charName, str2try));
		return true;
		// } catch (cereal::RapidXMLException& e) { // XML ERROR
		// 	// std::cerr << "serialize_mapin_or_posttreat: initialised catch: " << e.what() << '\n';
		// 	if (!isOptional) {
		// 		ar(cereal::make_nvp(charName, str2try));
		// 	} else {
		// 		bool isStillPresent = cereal::make_optional_nvp(ar, charName, str2try);
		// 		if (!isStillPresent){
		// 			return true; // it should be ok since it a optional field
		// 		}
		// 	}
		// 	if (str2try == "xxUninitializedStringXx"){
		// 		pLog->error("serialize_mapin_or_posttreat: the variable {} has not found.", charName);
		// 		return false;
		// 	}
		// 	saveString->insert(std::make_pair(charName, str2try));
		// 	return true;
	} catch (const ExceptionOptionalNVPnotFound &e) {
		bool isStillPresent = cereal::make_optional_nvp(ar, charName, str2try);
		if (!isStillPresent) {
			return true; // it should be ok since it a optional field
		}
		if (str2try == "xxUninitializedStringXx") {
			pLog->error("serialize_mapin_or_posttreat: the variable {} has not found.", charName);
			return false;
		}
		saveString->insert(std::make_pair(charName, str2try));
	} catch (const std::exception &e) {
		std::cout << e.what() << std::endl;
		std::cout << " error in if " << std::endl;
		throw;
	}
	return true;
}


/**
 * @brief
 *
 * @param str
 * @return any
 */
inline int link_string_to_mapio(MapInOutClass *map, std::string key, std::string val) {
	// Get the logger.
	auto pLog = shareLog::get_plog();

	// init variables
	int errorCode = -1;

	// check if map[key] exists.
	if (map->count(key) == 0) {
		pLog->error("The given Map does not have the key: {}", key);
		errorCode = 1100;
		// throw invalid_argument( buf );
		return errorCode;
	}

	// get the resultName object
	size_t found1a = val.find("linked2resultName(");
	size_t found1b = val.find("copyOfResultName(");
	size_t found2 = val.find(")");
	std::string resultNameOtherObj;
	std::string typeOfLink;

	if ((found1a != std::string::npos) && (found1b == std::string::npos)) {
		typeOfLink = "link";
		if (found2 != std::string::npos) {
			resultNameOtherObj = val.substr(found1a + 18, found2 - found1a - 18);
		} else {
			pLog->error("link_string_to_mapio: Unable to find the closing brace ')'. key: {}, val: {}.", key, val);
			errorCode = 1300;
		}
	} else if ((found1a == std::string::npos) && (found1b != std::string::npos)) {
		typeOfLink = "copy";
		if (found2 != std::string::npos) {
			resultNameOtherObj = val.substr(found1b + 17, found2 - found1b - 17);
		} else {
			pLog->error("link_string_to_mapio: Unable to find the closing brace ')'. key: {}, val: {}.", key, val);
			errorCode = 1300;
		}
	} else {
		pLog->error(
			 "link_string_to_mapio: the variable string to post treat does not contain any recognizable keyword. key: {}, val: {}. \n Example: linked2resultName(toto).mapIn[width]",
			 key, val);
		errorCode = 1200;
		return errorCode;
	}

	// Get the map type (In or Out).
	size_t found3a = val.find(".mapIn");
	size_t found3b = val.find(".mapOut");
	std::string mapIoOType;
	if ((found3a != std::string::npos) && (found3b == std::string::npos)) {
		mapIoOType = val.substr(found3a + 1, 5);
	} else if ((found3a == std::string::npos) && (found3b != std::string::npos)) {
		mapIoOType = val.substr(found3b + 1, 6);
	} else {
		pLog->error("link_string_to_mapio: Unable to determine if it is a mapIn or a mapOut. key: {}, val: {}.", key, val);
		errorCode = 1400;
		return errorCode;
	}

	// Get the key in the map.
	size_t found4 = val.find("[");
	size_t found5 = val.find("]");
	std::string mapKey;
	if ((found4 != std::string::npos) && (found5 != std::string::npos)) {
		mapKey = val.substr(found4 + 1, found5 - found4 - 1);
	} else {
		pLog->error("link_string_to_mapio: Unable to find the bracket associated to the map entrance. key: {}, val: {}.", key, val);
		errorCode = 1500;
		return errorCode;
	}

	// Check that the resultName is accessible
	if (mapResultName2Object.count(resultNameOtherObj) == 0) {
		pLog->error("The mapResultName2Object does not have the entrance key: {}", resultNameOtherObj);
		errorCode = 1600;
		// throw invalid_argument( buf );
		return errorCode;
	}

	// Get the the resultName chosen
	auto brcPtr = mapResultName2Object.at(resultNameOtherObj).get();

	// Get the mapIO of the resultName chosen
	MapInOutClass *mapPtr;
	if (mapIoOType == "mapIn") {
		mapPtr = &brcPtr->mapIn;
	} else if (mapIoOType == "mapOut") {
		mapPtr = &brcPtr->mapOut;
	} else {
		pLog->error("Invalid mapType captured (not mapIn nor mapOut): {}", mapIoOType);
		errorCode = 1700;
		// throw invalid_argument( buf );
		return errorCode;
	}

	// Check that type are compatibles.
	if (map->get_type_index(key) != mapPtr->get_type_index(mapKey)) {
		pLog->error("link_string_to_mapio: you are trying to link a type {} with a type: {}, respectively from {} and {}",
			 map->get_type_str(key), mapPtr->get_type_str(mapKey), key, mapKey);
		errorCode = 1800;
		// throw invalid_argument( buf );
		return errorCode;
	}

	// Get the associate key
	std::shared_ptr<void> dataVPtr = mapPtr->get_void_ptr(mapKey);

	// Link the addresses.
	if (typeOfLink == "link") {
		// std::cout << "I should transfert address! from void ptr or mapIO directly." << std::endl;
		map->change_address_svptr(key, dataVPtr);
	} else if (typeOfLink == "copy") {
		// std::cout << "I should copy address! from void ptr or mapIO directly." << std::endl;
		map->copy_data_svptr(key, dataVPtr);
	} else {
		pLog->error("link_string_to_mapio: Invalid typeOfLink computed: {}", typeOfLink);
		errorCode = 1900;
		// throw invalid_argument( buf );
		return errorCode;
	}

	errorCode = 0;
	return errorCode;
}


/**
 * @brief Template function returning a unique_ptr of the template argument given.
 *
 * @tparam T template class derived from BaseX.
 * @return std::unique_ptr<BaseX>
 */
template <typename T>
std::unique_ptr<BaseX> unique_creator() {
	std::unique_ptr<BaseX> uniqPtr(new T());
	return uniqPtr;
}

// BaseX Factory
/*! @TypeName alias for the function pointer targeting a template function
 *! 	creating a new unique_ptr of the given class.
 */
typedef std::unique_ptr<BaseX> (*CreatorU)();

class BaseRunFactory {
 public:
	/// returns true if the registration succeeded, false otherwise
	bool register_builder(std::string const &key, CreatorU const &builder) {
		// std::cout << "BaseRunFactory:register_builder: insert of " << key << std::endl;
		return m_mapFac.insert(std::make_pair(key, builder)).second;
	}

	/// returns a unique pointer to a new instance of BaseX (or a derived class)
	/// if the key was found, 0 otherwise
	std::unique_ptr<BaseX> build_new(std::string const &key) const {
		auto it = m_mapFac.find(key);
		if (it == m_mapFac.end()) { // no such key
			auto pLog = shareLog::get_plog();
			pLog->error("build_new: the requested object '{}' is not present in the factory. Segmentation fault are expected.", key);
			return 0; // 0 = null_ptr
		} else { // key found
			return (it->second)();
		}
	}

	int print_builders() {
		// Get the logger.
		auto pLog = shareLog::get_plog();

		// init variables
		int errorCode = -1;

		//
		pLog->debug("print_builders: all available builder are:");
		int i = 0;
		for (auto const &[key, _val_] : m_mapFac) {
			i++;
			pLog->debug("print_builders: {} {}", i, key);
		} // end for

		//
		errorCode = 0;
		return errorCode;
	}

	int clear_all_builders() {
		// Get the logger.
		auto pLog = shareLog::get_plog();

		// init variables
		int errorCode = -1;

		// remove all elements from the map.
		m_mapFac.clear();

		//
		pLog->info("clear_all_builders: all available builder have been cleaned !");

		//
		errorCode = 0;
		return errorCode;
	}

	int erase_builder(std::string const &key) {
		// init variables
		int errorCode = -1;

		// remove the element from the map.
		auto it = m_mapFac.find(key);
		if (it == m_mapFac.end()) { // no such key
			// Get the logger.
			auto pLog = shareLog::get_plog();
			pLog->error("erase_builder: the requested object '{}' is not present in the factory.", key);
			return errorCode; // 0 = null_ptr
		} else { // key found
			m_mapFac.erase(it); // m_mapFac.erase(key) can work also.
		}

		//
		errorCode = 0;
		return errorCode;
	}

 private:
	std::map<std::string, CreatorU> m_mapFac;
}; // end class BaseRunFactory

// Create a singleton of this factory, to register the derived classes
// during library load, which is handy for plugins-like architecture.
inline BaseRunFactory &get_factory() {
	static BaseRunFactory s_sF;
	return s_sF;
}

} // end namespace OBLiX
#endif // BASE_X_HPP

// #ifdef __cplusplus
// } // end extern "C"
// #endif

// EoF