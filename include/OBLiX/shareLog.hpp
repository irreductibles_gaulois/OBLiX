/**
 * @file sharelog.hpp
 *
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2021-09-24
 *
 * @copyright Copyright (c) 2021
 *
 */

// #ifdef __cplusplus
// extern "C" {
// #endif

#ifndef SHARELOG_HPP
#define SHARELOG_HPP

#undef SPDLOG_ACTIVE_LEVEL
#define SPDLOG_ACTIVE_LEVEL SPDLOG_LEVEL_TRACE
#include <spdlog/spdlog.h> // SpdLog
#include "spdlog/sinks/stdout_color_sinks.h"
#include "spdlog/sinks/basic_file_sink.h"

namespace shareLog {

/**
 * @brief
 *
 */
static inline std::string loggerName = "empty_logger";

/**
 * @brief Set the up logger object
 *
 * @param logger
 * @return std::shared_ptr<spdlog::logger>
 */
inline std::shared_ptr<spdlog::logger> setup_logger(spdlog::logger logger) {
	logger.trace("Logger initialization: {}", logger.name());
	loggerName = logger.name(); // store name of the logger.
	auto ptrLogger = std::make_shared<spdlog::logger>(logger); // create the share pointer
	spdlog::register_logger(ptrLogger); // register the logger via the shared pointer.
	logger.trace("Logger initialization. END");

	return ptrLogger;
} // end setup_logger

/**
 * @brief Get the pLog object
 *
 * @return std::shared_ptr<spdlog::logger>
 */
inline std::shared_ptr<spdlog::logger> get_plog() {
	auto ptrLogger = spdlog::get(loggerName);
	if (ptrLogger) { // the logger have been register and correctly setup.
		return ptrLogger;
	} else {
		auto consoleSink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
		consoleSink->set_pattern("[%H:%M:%S proc.%P][%^%-8l%$]%n:%@| %v");
		spdlog::logger logger(loggerName, consoleSink);
		ptrLogger = std::make_shared<spdlog::logger>(logger);
		return ptrLogger;
	}
} // end get_plog
} // end namespace shareLog

#endif // SHARELOG_HPP
// #ifdef __cplusplus
// } // end extern "C"
// #endif

// EoF