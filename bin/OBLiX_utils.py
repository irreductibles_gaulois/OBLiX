#!/usr/bin/env python3

# -*- coding: utf-8 -*-

# pylint: disable=invalid-name

"""
	LICENSE :
	Copyright (C) 2016-20** Romain NOEL

  	AUTHOR(S) :
		Romain NOËL
			Mail	: romain.noel@univ-eiffel.fr
"""

# pylint: enable=invalid-name
# Import of Libraries------------------------------------------------------------------------------
import os 					# library for Operating System
import sys					# library for system manipulation; sys is a submodule of os

import multiprocessing 	# lib for multiprocessing
import asyncio.subprocess	# lib to call command from the os terminal in a asynchronous subprocess
import logging				# lib to manipulate loggers
import inspect				# lib to have access to the caller of this module

# for old compatibility
import subprocess

# Initialization of primary variables--------------------------------------------------------------
#
PROJECT_NAME = "OBLiX"
SCRIPT_NAME = PROJECT_NAME +"_utils"
SCRIPT_DESCRIPTION = "Collection of utility python scripts."
AUTHORS_NAME = ["Romain NOËL"]
VERSION_DATE = "24-Jan-2022"
VERSION_NUM = 1.03

(frame, filename_frame, line_number, function_name, lines_frame, index_frame) = \
	inspect.getouterframes( inspect.currentframe() )[-1]
# print(frame, filename_frame, line_number, function_name, lines_frame, index_frame)
# print('caller is :', FILENAME_CALLER, 'and __name__ is :', __name__)
if filename_frame.startswith('<string>') or filename_frame.startswith('<stdin>') :
	FILENAME_CALLER='local'
else:
	# print('filename_frame='+filename_frame, flush=True)
	try:
		FILENAME_CALLER= filename_frame.rsplit('/',1)[-1].rsplit('.py',1)[0]
	except IndexError as e:
		FILENAME_CALLER= filename_frame.rsplit('\\',1)[-1].rsplit('.py',1)[0]
del frame, filename_frame, line_number, function_name, lines_frame, index_frame

logger = logging.getLogger(FILENAME_CALLER+'.'+SCRIPT_NAME)

thrd_list=[]


# Collection of useful functions -------------------------------------------------------------------
class MyExceptionNotfound( Exception ):
	"""this is my exception class for which not found: nothing to do it is possible"""
	# pylint: disable=unnecessary-pass
	pass
	# pylint: enable=unnecessary-pass


class MyExceptionExitNotZero( Exception ):
	"""this is my exception class for a Not Zero Exit, bad luck."""
	# pylint: disable=unnecessary-pass
	pass
	# pylint: enable=unnecessary-pass


def pywhich(file):
	""" function to simulation which bash function."""
	for path in sys.path:
		try:
			if any(path.startswith(file + '.py') for path in os.listdir(path)):
				return os.path.join(path, file)
		except OSError:
			pass
	for path in os.environ["PATH"].split(os.pathsep):
		try:
			if os.path.exists(os.path.join(path, file)):
				return os.path.join(path, file)
		except OSError:
			pass
	raise MyExceptionNotfound(f"{file}: Not found in PATH or pythonpath \n")


def init_logs(loglevel, logfilename):
	"""
		function description.
	"""

	#  Creating the custom log levels
	# pylint: disable=protected-access # for _log
	logging.addLevelName(75, "EMERGENCY")
	def emergency(self, message, *args, **kws):
		# Yes, logger takes its '*args' as 'args'.
		if self.isEnabledFor(75):
			self._log(75, message, args, **kws)
	logging.Logger.emergency = emergency
	logging.EMERGENCY = 75

	logging.addLevelName(55, "ALERT")
	def alert(self, message, *args, **kws):
		# Yes, logger takes its '*args' as 'args'.
		if self.isEnabledFor(55):
			self._log(55, message, args, **kws)
	logging.Logger.alert = alert
	logging.ALERT = 55

	logging.addLevelName(25, "Notice")
	def notice(self, message, *args, **kws):
		# Yes, logger takes its '*args' as 'args'.
		if self.isEnabledFor(25):
			self._log(25, message, args, **kws)
	logging.Logger.notice = notice
	logging.NOTICE = 25

	logging.addLevelName(5, "Trace")
	def trace(self, message, *args, **kws):
		# Yes, logger takes its '*args' as 'args'.
		if self.isEnabledFor(5):
			self._log(5, message, args, **kws)
	logging.Logger.trace = trace
	logging.TRACE = 5
	# pylint: enable=protected-access

	# create loggerBis with based on the logfilename
	loggerBis = logging.getLogger(logfilename)

	# create file handler
	fileHandle = logging.FileHandler(logfilename+'.log', mode='w') # by default is mode='a'
	# fileHandle = logging.handlers.RotatingFileHandler('mptest.log', 'a', 300, 10)

	# create console handler
	consoleHandle = logging.StreamHandler(stream=sys.stdout)

	# create formatter and add it to the handlers
	formatterFH = logging.Formatter('%(asctime)s; %(process)d-%(thread)d; '\
							'%(name)s-[%(levelname)s]: %(message)s (%(funcName)s)')
	formatterCH = logging.Formatter('[%(levelname)s]: %(message)s (%(name)s-%(funcName)s)')
	# datefmt='%m/%d/%Y %I:%M:%S'
	fileHandle.setFormatter(formatterFH)
	consoleHandle.setFormatter(formatterCH)

	# set log level by default
	loggerBis.setLevel(logging.ERROR)
	# fileHandle.setLevel(logging.DEBUG)
	# consoleHandle.setLevel(logging.TRACE)

	# Parametering logging and redirection according to the given arguments
	if loglevel >= 4 :
		loggerBis.setLevel(logging.WARNING)
	if loglevel >= 5 :
		loggerBis.setLevel(logging.NOTICE)
	if loglevel >= 6 :
		loggerBis.setLevel(logging.INFO)
	if loglevel >= 7 :
		loggerBis.setLevel(logging.DEBUG)
	if loglevel >= 8 :
		loggerBis.setLevel(logging.TRACE)

	# add the handlers to the loggerBis
	loggerBis.addHandler(fileHandle)
	loggerBis.addHandler(consoleHandle)

	# Add explanations at beginning of the log file

	return loggerBis


def subp_old(cmdList, encoding=None):
	"""
		subp_old ...
	"""
	print ('[Notice]     ',' '.join(cmdList), end='', flush=True)
	retCode=-1

	if sys.platform.startswith('linux'):
		if not encoding:
			encoding='utf-8' # = cp65001
		numNL=-1
	elif sys.platform.startswith('win'):
		if not encoding:
			encoding='utf-8' # Warning! NO: latin1, MAYBE: cp437,cp850, cp860, cp1252
		numNL=-2
	else :
		print('[ERROR]     OS not available for encoding... EXIT')
		sys.exit(2)

	# process = subprocess.check_call(cmdList, shell=True, close_fds=True,
	# 	stdout=subprocess.PIPE, stderr=subprocess.PIPE, bufsize=-1, universal_newlines=True)
	with subprocess.Popen(cmdList, stdout=subprocess.PIPE,  stderr=subprocess.PIPE) as process:
		for lin in iter(process.stdout.readline, ''):
			if lin:
				print(lin[:numNL].decode(encoding), flush=True)

			if (not lin) and (process.poll() is not None):
				# if errput: print('\n',errput[:numNL].decode(encoding), flush=True)
				# process.stderr.readline() is a trick to avoid 'process.stdout.readline()' to bug when
				# 	some errors appears on windows, don't ask me why.
				errput=process.stderr.readlines()
				for lines in errput:
					print(lines[:numNL].decode(encoding), flush=True)
				break

		retCode=process.poll()
	#end with
	print(f" [Exit {retCode}]", flush= True)
	if retCode!=0 :
		raise MyExceptionExitNotZero(f'subp: {cmdList} Exit with a non-zero code.')
	return retCode


# @asyncio.coroutine equivalent to async def ...
async def read_stream_and_display(stream, display, numNL, encoding, word=''):
	"""
		Read from stream line by line until EOF, display, and capture the lines.
	"""

	# output = []

	# while True:
	# 	line = await stream.readline()
	# 	if not line:
	# 		break
	# 	# output.append(line)
	# 	print(word,line[:numNL].decode(encoding), file=display, flush=True) # assume it doesn't block

	async for line in stream:
		# output.append(line)
		print(word,line[:numNL].decode(encoding), file=display, flush=True)

	# return b''.join(output)

# pylint: disable=too-many-arguments
# @asyncio.coroutine
async def get_subp(cmdList, numNL, encoding, streamout ,streamerr, wordout, worderr, log):
	"""
		get_subp: description to write !
	"""
	# print ('[Notice]     ',*cmdList, file=f5, end='', flush=True)
	log.notice('subp: '+' '.join(cmdList) )
	# Create the subprocess, redirect the standard output into a pipe
	create = asyncio.create_subprocess_exec(*cmdList, \
							stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE) #
	# proc = yield from create
	proc = await create 	# await is equivalent to yield from in a async routine
	# yield from iterator
	#  is equivalent to
	# for x in iterator:
	# 	yield x

	# read child's stdout/stderr concurrently (capture and display)
	try:
		# stdout, stderr = yield from asyncio.gather(
		stdout, stderr = await asyncio.gather(
			read_stream_and_display(proc.stdout, streamout, numNL, encoding, wordout),
			read_stream_and_display(proc.stderr, streamerr, numNL, encoding, worderr))
	except Exception:
		proc.kill()
		raise
	finally:
		# wait for the process to exit
		# retCode = yield from proc.wait()
		retCode = await proc.wait()
		# print(" [Exit {}]".format(retCode), file=f5, flush= True)
		log.notice(f" [Exit {retCode}] {' '.join(cmdList)}")
	return retCode, stdout, stderr


def subp(cmdList, encoding=None, streamout=sys.stdout, streamerr=sys.stderr, \
			wordout='out_subp:', worderr='ERR_subp:', log=logger):
	""" subp(cmdList, encoding=None, streamout=sys.stdout, streamerr=sys.stderr, \
			wordout='out_subp:', worderr='ERR_subp:', log=logger):

		Description: Create the asyncio event loop and call 'get_subp' to execute a subprocess command
			list in it. The loop does the call via a 'run_until_complete'.
		Args:
			cmdList: the command list to execute in the function 'get_subp'.
			encoding=None: the encoding used for encode/decode the processe.
			streamout=sys.stdout: the stream used for displaying the (standard) output channel.
			streamerr=sys.stderr: the stream used for displaying the (standard) error channel.
			wordout='out_subp:': the string add before each line of the streamout.
			worderr='ERR_subp:': the string add before each line of the streamerr.
			log=logger: the logging.logger to use.
		Returns: (retCode, stdout, stderr)
			retCode: return code (bool), -1 if it has not run yet,
											0 if everything was executed correctly,
											>0 if an error occurred (then the retCode value is the error code).
			stdout: the output stream.
			stderr: the error stream.
		Raises:
			No raise. It has been deactivate since version 1.00 .
		Examples:
			retCode, stringO, stringE = subp(['echo','toto'])
	"""

	if sys.platform.startswith('linux'):
		if not encoding:
			encoding='utf-8' # = cp65001
		numNL=-1
		# loop = asyncio.get_event_loop()
		newLoop = asyncio.new_event_loop()
		asyncio.set_event_loop(newLoop)
	elif sys.platform.startswith('win'):
		if not encoding:
			encoding='utf-8' # Warning! NO: latin1, MAYBE: cp437,cp850, cp860, cp1252
		numNL=-2
		newLoop = asyncio.ProactorEventLoop() # for 'subprocess' pipes on Windows
		asyncio.set_event_loop(newLoop)
	else :
		log.error('[ERROR]     OS not available for encoding... EXIT')
		sys.exit(2)

	retCode, stdout, stderr= newLoop.run_until_complete(
							get_subp(cmdList, numNL, encoding, streamout ,streamerr, wordout, worderr, log)
						)
	newLoop.close()
	if retCode!=0 :
		logger.warning(f"Exit not zero ({retCode}): " +f"{sys.exc_info()[0]}")
		# raise MyExceptionExitNotZero('Subp {} Exit a {} exit code'.format(retCode, cmdList[0]) )
	return retCode, stdout, stderr


def subp_from_loop(cmdList, loop, stop=True, encoding=None, \
							streamout=sys.stdout ,streamerr=sys.stderr, \
						 	wordout='out_subp:', worderr='ERR_subp:', log=logger):
	"""subp_from_loop(cmdList, loop, stop=True, encoding=None, streamout=sys.stdout ,\
							streamerr=sys.stderr, wordout='out_subp:', worderr='ERR_subp:', log=logger):

		Description: Run a 'get_subp' to execute a subprocess command
			list in a given asyncio loop. The loop does the call via a 'run_coroutine_threadsafe'.
			Then eventually the loop is close.
		Args:
			cmdList: the command list to execute in the function 'get_subp'.
			loop: asyncio loop that will be used to execute the command list.
			stop=True: bool the close or leave open the loop after the execution.
			encoding=None: the encoding used for encode/decode the processe.
			streamout=sys.stdout: the stream used for displaying the (standard) output channel.
			streamerr=sys.stderr: the stream used for displaying the (standard) error channel.
			wordout='out_subp:': the string add before each line of the streamout.
			worderr='ERR_subp:': the string add before each line of the streamerr.
			log=logger: the logging.logger to use.
		Returns: (retCode, stdout, stderr)
			retCode: return code (bool), -1 if it has not run yet,
											0 if everything was executed correctly,
											>0 if an error occurred (then the retCode value is the error code).
			stdout: the output stream.
			stderr: the error stream.
		Raises:
			No raise. It has been deactivate since version 1.00 .
		Examples:
			retCode, stringO, stringE = subp_from_loop(['echo','toto'])
	"""

	if sys.platform.startswith('linux'):
		if not encoding:
			encoding='utf-8' # = cp65001
		numNL=-1
		# loop = asyncio.get_event_loop()
		# newLoop = asyncio.new_event_loop()
		# asyncio.set_event_loop(newLoop)
	elif sys.platform.startswith('win'):
		if not encoding:
			encoding='utf-8' # Warning! NO: latin1, MAYBE: cp437,cp850, cp860, cp1252
		numNL=-2
		# newLoop = asyncio.ProactorEventLoop() # for subprocess' pipes on Windows
		# asyncio.set_event_loop(newLoop)
	else :
		log.error('[ERROR]     OS not available for encoding... EXIT')
		sys.exit(2)

	# submit the task to asyncio
	fut = asyncio.run_coroutine_threadsafe( \
						get_subp(cmdList, numNL, encoding, streamout ,streamerr, wordout, worderr, log),\
						loop )
	# wait for the task to finish
	retCode, stdout, stderr = fut.result()
	# fut.add_done_callback(partial( stop_loop, loop ) ) # loop, fut
	# loop.call_soon_threadsafe(stop_loop(loop))
	if stop:
		loop.call_soon_threadsafe( loop.stop )

	return retCode, stdout, stderr
# pylint: disable=too-many-arguments


def spin_loop(loop):
	""" spin_loop(loop):
		spin_loop: description to write !
	"""
	if loop.is_running() is False :
		print('spinloop start')
		asyncio.set_event_loop(loop)

		# # May want to catch other signals too
		# signals = (signal.SIGHUP, signal.SIGTERM, signal.SIGINT)
		# for s in signals:
		# 	loop.add_signal_handler(s, partial(stop_loop, loop) )
		try:
			loop.run_forever()
		except Exception as ex:
			print('Exception detected so I ask to close the loop')
			print(ex)
			loop.run_until_complete( loop.shutdown_asyncgens() )
			raise
		# finally:
		# 	loop.run_until_complete(loop.shutdown_asyncgens())
		# 	loop.close()
		print('spinloop end')


def stop_loop(loop):
	""" stop_loop(loop):
		stop_loop: description to write !
	"""
	# loop.stop()
	# loop.cancel()
	# loop.call_soon_threadsafe( loop.stop )
	loop.stop()
	# loop.close()
	print('stop_loop')


def get_loop():
	""" get_loop():
		Function that return a child loop for windows or linux.
	"""
	if sys.platform.startswith('win'):
		loop = asyncio.ProactorEventLoop() # for subprocess' pipes on Windows
		asyncio.set_event_loop(loop)
	elif sys.platform.startswith('linux'):
		try :
			asyncio.get_child_watcher()
			loop = asyncio.get_event_loop()
			# print('the loop exists')
		except RuntimeError :
			# print('the loop does not exist')
			loop = asyncio.new_event_loop()
			asyncio.set_event_loop(loop)
	# asyncio.set_child_watcher(loop)
	else :
		print('[ERROR]     OS not available for encoding in subp from my_utils.py module... EXIT')
		sys.exit(2)

	return loop


# The size of the rotated files is made small so you can see the results easily.
def listener_configurer():
	"""
		listener_configurer: description ...
	"""
	root = logging.getLogger()
	#(filename, mode='a', maxBytes=0, backupCount=0, encoding=None, delay=False)
	handlers = logging.handlers.RotatingFileHandler('mptest.log', 'a', 300, 10)
	fileHandle = logging.Formatter(
			'%(asctime)s %(processName)-10s %(name)s %(levelname)-8s %(message)s')
	consoleHandle = logging.StreamHandler()
	handlers.setFormatter(fileHandle)
	consoleHandle.setFormatter(fileHandle)
	root.addHandler(handlers)
	root.addHandler(consoleHandle)

# This is the listener process top-level loop: wait for logging events
# (LogRecords)on the queue and handle them, quit when you get a None for a
# LogRecord.
def listener_process(queue, configurer):
	"""[summary]

	Args:
		 queue ([type]): [description]
		 configurer ([type]): [description]
	"""
	configurer()
	while True:
		try:
			record = queue.get()
			if record is None:  # We send this as a sentinel to tell the listener to quit.
				break
			# pylint: disable=redefined-outer-name
			logger = logging.getLogger(record.name)
			# pylint: enable=redefined-outer-name
			logger.handle(record)  # No level or filter logic applied - just do it!
		except Exception:
			print('Whoops! Problem:', file=sys.stderr)
			raise

# The worker configuration is done at the start of the worker process run.
# Note that on Windows you can't rely on fork semantics, so each process
# will run the logging configuration code when it starts.
def worker_configurer(queue):
	"""[summary]

	Args:
		 queue ([type]): [description]
	"""
	handlers = logging.handlers.QueueHandler(queue)  # Just the one handler needed
	root = logging.getLogger()
	root.addHandler(handlers)
	# send all messages, for demo; no other level or filter logic applied.
	root.setLevel(logging.DEBUG)

# This is the worker process top-level loop, which just logs ten events with
# random intervening delays before terminating.
# The print messages are just so you know it's doing something!
def worker_process(queue, configurer):
	"""[summary]

	Args:
		 queue ([type]): [description]
		 configurer ([type]): [description]
	"""
	configurer(queue)
	name = multiprocessing.current_process().name
	name = name +''
	print(f'Worker started: {name}')
	# pylint: disable=redefined-outer-name
	logger = logging.getLogger('test')
	# pylint: enable=redefined-outer-name
	# pylint: disable=logging-not-lazy
	logger.info( f"info from {name}"+"." )
	# pylint: enable=logging-not-lazy
	# DO SOMETHING

#EOF
