#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# pylint: disable=invalid-name
"""
	LICENSE:
		Copyright (C) 2021-20** Romain NOEL

	AUTHOR(S):
		Romain NOËL
			Mail: romain.noel@univ-eiffel.fr
			Address: Allee des Ponts et Chaussees, 44340 Bouguenais, FRANCE
"""
# pylint: enable=invalid-name


# Import of Libraries-----------------------------------------------------------
import os 					# library for Operating System
import sys					# library for system manipulation; sys is a submodule of os
import logging				# lib to manipulate loggers
import inspect				# lib to have access to the caller of this module

import shutil				# lib for copy file and so on
import re 					# lib for regular expression

# Find chRootProject path.
# pylint: disable=no-else-break
_PREV_PATH = None
_CALLING_DIR = os.getcwd()
while True :
	if (os.path.exists('./OBLiX-README.md')) and (os.getcwd()!='/'):
		CHROOT_PATH=os.getcwd()
		break
	elif _PREV_PATH==os.getcwd() :
		print('ERROR: the chRootProject folder was not found...')
		os.chdir(_CALLING_DIR)
		sys.exit(3)
		break
	else:
		_PREV_PATH=os.getcwd()
		os.chdir('../')
os.chdir(_CALLING_DIR) # Go back to the call directory.
del _PREV_PATH
# del _callingDir
# pylint: enable=no-else-break

# Import Project Libraries
sys.path.append(CHROOT_PATH+'/bin')
MyUtils= __import__('OBLiX_utils')


# Initialization of primary variables-------------------------------------------
#
AUTHORS_NAME=["Romain NOËL"]
VERSION_NUM=1.01
VERSION_DATE="14-December-2021"
PROJECT_NAME="OBLiX"
SCRIPT_NAME="executor_"+PROJECT_NAME
SCRIPT_DESCRIPTION= f"script python to execute {PROJECT_NAME} in a dedicated workspace."

(frame, filename_frame, line_number, function_name, lines_frame, index_frame) = \
	inspect.getouterframes( inspect.currentframe() )[-1]
# print(frame, filename_frame, line_number, function_name, lines_frame, index_frame)
# print('caller is :', FILENAME_CALLER, 'and __name__ is :', __name__)
if filename_frame.startswith('<string>') or filename_frame.startswith('<stdin>') :
	FILENAME_CALLER='local'
else:
	# print('filename_frame='+filename_frame, flush=True)
	try:
		FILENAME_CALLER= filename_frame.rsplit('/',1)[-1].rsplit('.py',1)[0]
	except IndexError as e:
		FILENAME_CALLER= filename_frame.rsplit('\\',1)[-1].rsplit('.py',1)[0]
del frame, filename_frame, line_number, function_name, lines_frame, index_frame

logger = logging.getLogger(FILENAME_CALLER+'.'+SCRIPT_NAME)


################################ 	FUNCTIONS 	################################

def check_folder_file (folder, file):
	"""
		Description:
			x
		Args:
			x
		Returns:
			x
		Raises:
			x
		Examples:
			x
		Remarks: (refs, notes, ToDo, etc.)
			x
	"""
	retCode=-1
	if os.path.isfile(folder+file):
		tempPath= os.path.abspath( folder+file )
		folder= tempPath.rsplit('/',1)[0]+'/'
		file= tempPath.rsplit('/',1)[-1]
		retCode=0
	elif os.path.isfile(file):
		tempPath= os.path.abspath( file )
		folder= tempPath.rsplit('/',1)[0]+'/'
		file= tempPath.rsplit('/',1)[-1]
		retCode=0
	elif os.path.isdir(folder):
		tempExec= file.rsplit('/',1)[-1]
		execFound= False
		for fileLocal in os.listdir(folder) :
			if fileLocal==tempExec :
				tempPath= os.path.abspath( fileLocal )
				folder= tempPath.rsplit('/',1)[0]+'/'
				file= tempPath.rsplit('/',1)[-1]
				retCode=0
				execFound= True
				break
		if not execFound:
			# logger.error("Impossible to find: {} in {}".format(file, folder))
			retCode=1
	else:
		retCode=2
		# logger.error("Impossible to find: {} Nor {} Nor {}".format(folder+file, file, folder))

	return retCode, folder, file
# end function


class ExecutorOptions(): # pylint: disable=too-many-instance-attributes
	"""
		Description:
			x
		Args:
			x
		Returns:
			x
		Raises:
			x
		Examples:
			x
		Remarks: (refs, notes, ToDo, etc.)
			x
	"""

	def __init__(self, mainPID):
		"""
			Description:
				x
			Args:
				x
			Returns:
				x
			Raises:
				x
			Examples:
				x
			Remarks: (refs, notes, ToDo, etc.)
				x
		"""

		# super(ExecutorOptions, self).__init__()
		self.verbosity = 5
		self.ext = ''
		self.prefixExe = './'
		self.sep = os.path.sep
		if sys.platform.startswith('linux') :
			pass
		elif sys.platform.startswith('win') :
			self.prefixExe = ''
			self.ext = '.exe'
		else :
			print('WARNING: Untested OS')
		self.mainPID = mainPID
		self.binExec = 'OBLiXprog'
		self.binDir = CHROOT_PATH+'/build/dist/bin/OBLiX/'
		self.binExecOpt = ''
		self.binExecVersion = False
		self.otherBinTools = []
		self.otherToolsDir = ['']*len(self.otherBinTools)
		self.inputFile = '../input/params_OBLiX.xml'
		self.inputFileDir = ''
		self.secondaryInputFiles = []
		self.secondaryInputDir = ['']*len(self.secondaryInputFiles)
		self.outputDir = CHROOT_PATH+'/output/'
		self.tmpDir = CHROOT_PATH +self.sep+'tmp'+self.sep+'OBLiX_pid'+str(self.mainPID)
		self.tmpDirEnabled = False

		# other internal variables.
		self.cmd=''

		# return
	# end function


	def create_cmd(self): # pylint: disable=too-many-branches
		"""
			Description:
				x
			Args:
				x
			Returns:
				x
			Raises:
				x
			Examples:
				x
			Remarks: (refs, notes, ToDo, etc.)
				x
		"""

		# binExec
		retCode, self.binDir, self.binExec = check_folder_file (self.binDir, self.binExec)
		if retCode!=0 :
			logger.error( f"Impossible to find: {self.binDir+self.binExec} "\
				+ f"Nor {self.binExec} Nor {self.binDir}" )
		# add to the comand string
		self.cmd= self.prefixExe +self.binExec +self.ext + ' '

		# version
		if self.binExecVersion:
			self.cmd= self.cmd +'-V '
		# extra options
		if self.binExecOpt:
			self.cmd= self.cmd +self.binExecOpt+' '

		# Verbosity configuration.
		if self.verbosity <= 3 :
			self.cmd= self.cmd +'-q '
		elif self.verbosity == 5 :
			self.cmd= self.cmd +'-v '
		elif self.verbosity == 6 :
			self.cmd= self.cmd +'-vv '
		elif self.verbosity == 7 :
			self.cmd= self.cmd +'-vvv '
		elif self.verbosity == 8 :
			self.cmd= self.cmd +'-vvvv '

		# otherBinTools
		maxLengthList = max(len( self.otherToolsDir ), len( self.otherBinTools ))
		self.otherBinTools += [''] * (maxLengthList - len( self.otherBinTools ))
		self.otherToolsDir += [''] * (maxLengthList - len( self.otherToolsDir ))
		arrayRetCode   = [[1 for i in range(maxLengthList)] for j in range(maxLengthList)]
		arrDir = [['' for i in range(maxLengthList)] for j in range(maxLengthList)]
		arrBin = [['' for i in range(maxLengthList)] for j in range(maxLengthList)]
		for i, valueI in enumerate(self.otherBinTools ):
			for j, valueJ in enumerate( self.otherToolsDir ):
				arrayRetCode[i][j], arrDir[i][j], arrBin[i][j] = check_folder_file(valueJ, valueI)
				#
				if arrayRetCode[i][j] == 0: # pylint: disable=no-else-break
					self. otherToolsDir[i]= arrDir[i][j]
					self.otherBinTools[i]= arrBin[i][j]
					break
				elif j == maxLengthList :
					logger.error( f"Impossible to find: {valueI} "\
						+ f"in none of directories: {self.otherToolsDir}" )

		# input file
		retCode, self.inputFileDir, self.inputFile = check_folder_file (self.inputFileDir, self.inputFile)
		if retCode!=0 :
			logger.error( f"Impossible to find: {self.inputFileDir+self.inputFile} " \
				+ f"Nor {self.inputFile} Nor {self.inputFileDir}." )
			logger.error( f"The current path is: {os.getcwd()} ." +f" returned code={retCode}" )
		# Add input to the command string
		self.cmd= self.cmd +'--input='+self.inputFile

		# Secondary Inputs
		maxLengthList = max(len( self.secondaryInputDir ), len( self.secondaryInputFiles ))
		self.secondaryInputFiles += [''] * (maxLengthList - len( self.secondaryInputFiles ))
		self.secondaryInputDir += [''] * (maxLengthList - len( self.secondaryInputDir ))
		arrayRetCode   = [[1 for i in range(maxLengthList)] for j in range(maxLengthList)]
		arrDir = [['' for i in range(maxLengthList)] for j in range(maxLengthList)]
		arrBin = [['' for i in range(maxLengthList)] for j in range(maxLengthList)]
		for i, valueI in enumerate(self.secondaryInputFiles ):
			for j, valueJ in enumerate( self.secondaryInputDir ):
				arrayRetCode[i][j], arrDir[i][j], arrBin[i][j] = check_folder_file(valueJ, valueI)
				#
				if arrayRetCode[i][j] == 0: # pylint: disable=no-else-break
					self. secondaryInputDir[i]= arrDir[i][j]
					self.secondaryInputFiles[i]= arrBin[i][j]
					break
				elif j == maxLengthList :
					logger.error( f"Impossible to find: {valueI}" + \
						f" in none of directories: {self.secondaryInputDir}" )

		# return
	# end function

	def edit_from_conf(self, conf): # pylint: disable=too-many-branches
		"""
			Description:
				x
			Args:
				x
			Returns:
				x
			Raises:
				x
			Examples:
				x
			Remarks: (refs, notes, ToDo, etc.)
				x
		"""

		self.verbosity = conf.loglevel
		# pylint: disable=multiple-statements
		if conf.binExec : self.binExec = conf.binExec

		if conf.binDir : self.binDir = conf.binDir
		if conf.binExecOpt : self.binExecOpt= conf.binExecOpt
		if conf.binExecVersion : self.binExecVersion= conf.binExecVersion
		if conf.otherBinTools : self.otherBinTools= conf.otherBinTools
		if conf.otherToolsDir : self.otherToolsDir= conf.otherToolsDir
		if conf.inputFile :
			self.inputFile= conf.inputFile
		elif conf.flagExecute is True :
			logger.warning('inputFile is recommended in the batch_configuration file.')
		if conf.inputFileDir : self.inputFileDir= conf.inputFileDir
		if conf.secondaryInputFiles : self.secondaryInputFiles= conf.secondaryInputFiles
		if conf.secondaryInputDir : self.secondaryInputDir= conf.secondaryInputDir
		if conf.outputDir :
			self.outputDir= conf.outputDir
		elif conf.flagExecute is True :
			logger.warning('outputDir is recommended in the batch_configuration file.')
		if conf.tmpDir : self.tmpDir= conf.tmpDir
		if conf.tmpDirEnabled : self.tmpDirEnabled= conf.tmpDirEnabled
		# pylint: enable=multiple-statements

		# return
	# end function

	def moving_outfiles(self):
		""" moving_outfiles(self)

			Description:
				copy all the files with 'out' in their name
			Args:
				x
			Returns:
				x
			Raises:
				x
			Examples:
				x
			Remarks: (refs, notes, ToDo, etc.)
				x
		"""

		# os.chdir(CHROOT_PATH+'/bin')

		listPossibleExtensions= ['.dat', '.csv', '.vtk', '.bin', '.pvd', 'gmon.out']
		filesSources = [f for f in os.listdir(self.tmpDir) if re.search(".*log.*", f, re.IGNORECASE)\
			 or f.endswith(tuple(listPossibleExtensions))]
		if not os.path.exists(self.outputDir):
			os.makedirs(self.outputDir)
		for files in filesSources:
			shutil.move(self.tmpDir+'/'+files, self.outputDir +files )

		logger.notice( f"Move the output files in the folder {self.outputDir}: done" )
		# return
	# end function


	def copy_exe(self):
		""" copy_exe(self)

			Description:
				copying the executable and the inputs in a temporary file
			Args:
				x
			Returns:
				x
			Raises:
				x
			Examples:
				x
			Remarks: (refs, notes, ToDo, etc.)
				x
		"""

		if not os.path.exists(self.tmpDir):
			os.makedirs(self.tmpDir)

		# Copy of exe
		shutil.copy('./'+self.binExec+self.ext, self.tmpDir +'/'+ self.binExec+self.ext)
		# Copy of input parameter file
		shutil.copy(self.inputFileDir+self.inputFile, self.tmpDir +'/'+ self.inputFile)

		# os.chdir(self.tmpDir)
		logger.notice( "Copy the executables and necessary file in the working "\
			f"temporary file {self.tmpDir}: done" )
		# return
	# end function


	def clean_tmp(self):
		""" clean_tmp(self)

			Description:
				cleaning the temporary file
			Args:
				x
			Returns:
				x
			Raises:
				x
			Examples:
				x
			Remarks: (refs, notes, ToDo, etc.)
				x
		"""

		# os.chdir(CHROOT_PATH)
		shutil.rmtree(self.tmpDir+'/')

		logger.notice( f"Cleaning the working temporary folder {self.tmpDir}: done" )
		# return
	# end function


	def clean_destination(self):
		"""
			Description:
				x
			Args:
				x
			Returns:
				x
			Raises:
				x
			Examples:
				x
			Remarks: (refs, notes, ToDo, etc.)
				x
		"""
		listPossibleExtensions= ['.dat', '.csv', '.vtk', '.hdf5']
		try:
			filelist = list( os.listdir(self.outputDir) )
			for files in filelist:
				# if files==self.VTKDir[1:]:
				# 	shutil.rmtree(self.outputDir+files)
				# if files==self.CSVDir[1:]:
				# 	shutil.rmtree(self.outputDir+files)
				if files.endswith(tuple(listPossibleExtensions)) :
					os.remove(self.outputDir+files)
		except FileNotFoundError:
			# If the error file not found arise it means to destination folder must be created.
			if not os.path.exists(self.outputDir):
				# print(os.getcwd())
				os.makedirs(self.outputDir)
			else:
				raise
		# return
	# end function


	def create_symlinks(self):
		""" create_symlinks(self)
			Description:
				Creating symbolic links in the output directory.
			Args:
				x
			Returns:
				x
			Raises:
				x
			Examples:
				x
			Remarks: (refs, notes, ToDo, etc.)
				x
		"""

		self.outputDir=os.path.abspath( self.outputDir ) +'/'

		logger.notice( f"Creating symlinks in the output directory:{self.outputDir}" )

		if not os.path.exists(self.outputDir):
			os.makedirs(self.outputDir)

		# binExec
		if not os.path.isfile( self.outputDir+self.binExec ):
			os.symlink(self.binDir+self.binExec, self.outputDir+self.binExec)

		# otherBinTools
		for folders, files in zip( self.otherToolsDir, self.otherBinTools ):
			if not os.path.isfile( self.outputDir+files ):
				os.symlink(folders+files, self.outputDir+files)

		# InputFile
		if not (os.path.isfile(self.outputDir+self.inputFile) or \
				os.path.islink(self.outputDir+self.inputFile)) :
			os.symlink(self.inputFileDir+self.inputFile, self.outputDir+self.inputFile)

		# secondaryInputFiles
		for folders, files in zip( self.secondaryInputDir, self.secondaryInputFiles ):
			if not os.path.isfile( self.outputDir+files ):
				os.symlink(folders+files, self.outputDir+files)
		# return
	# end function


	def destruct_symlinks(self):
		""" destruct_symlinks(self)

			Description:
				Detaching the symbolic links from the output directory.
			Args:
				x
			Returns:
				x
			Raises:
				x
			Examples:
				x
			Remarks: (refs, notes, ToDo, etc.)
				x
		"""

		# Say hello.
		logger.notice('Symlinks detached from the output directory.')

		# binExec
		if os.path.islink( self.outputDir+self.binExec ):
			os.unlink(self.outputDir+self.binExec)

		# otherBinTools
		for files in self.otherBinTools:
			if os.path.islink( self.outputDir+files ):
				os.unlink(self.outputDir+files)

		# InputFile
		if os.path.islink( self.outputDir+self.inputFile ):
			os.unlink(self.outputDir+self.inputFile)

		# secondaryInputFiles
		for files in self.secondaryInputFiles:
			if os.path.islink( self.outputDir+files ):
				os.unlink(self.outputDir+files)
		# return
	# end function


	def run_cmd(self):
		"""
			Description:
				x
			Args:
				x
			Returns:
				x
			Raises:
				x
			Examples:
				x
			Remarks: (refs, notes, ToDo, etc.)
				x
		"""
		# Notify the execution itself.
		logger.notice('Beginning of the execution')

		# Get encoding associated to the OS used.
		if sys.platform.startswith('linux'):
			encode='utf-8' # = cp65001
		elif sys.platform.startswith('win'):
			encode='cp1252' #NO: latin1, OK: cp437,cp850, cp857, cp858, cp860, cp861, cp863, cp865, cp1252

		# Execution of the main program
		# Execute the binary code in a new external subprocess.
		listcmd=[]
		listcmd.extend(self.cmd.split())
		retCode=-1
		try:
			retCode= MyUtils.subp(listcmd, encoding=encode, log=logger)
			if retCode[0] == 0 :
				pass
			else:
				logger.error( f"None zero exit code: {retCode[0]}"+"." ) # pylint: disable=logging-not-lazy
				# raise
		except:
			logger.error( f"EXCEPT in the run_exec: {sys.exc_info()}" +f"return code:{retCode}" )
			raise
			# sys.exit(5)

		# return error code.
		return retCode[0]


	def run_exec(self):
		"""
			Description:
				x
			Args:
				x
			Returns:
				x
			Raises:
				x
			Examples:
				x
			Remarks: (refs, notes, ToDo, etc.)
				x
		"""
		# The command prompt (cmd.exe) is by default using cp850 encoding so it's
		# preferable to change it into utf8=cp65001

		# get the calling directory
		prevCallingDir= os.getcwd()

		# Create the command string.
		self.create_cmd()

		if self.tmpDirEnabled:
			# Clean up the destination folder.
			logger.notice('Cleaning of the destination folder')
			self.clean_destination()

			# copy
			self.copy_exe()
		else:
			#
			self.create_symlinks()
			#
			os.chdir(self.outputDir)

		# Notify the execution itself.
		retCode= self.run_cmd()

		if self.tmpDirEnabled:
			# copy
			self.moving_outfiles()
			# cleaning tmp
			self.clean_tmp()
		else:
			#
			self.destruct_symlinks()

		# Go back to the previous calling directory.
		os.chdir(prevCallingDir)

		# return error code.
		return retCode


################################ 	MAIN SCRIPT 	########################################
if __name__ == '__main__':
	# Import of Libraries-------------------------------------------------------
	BatchConfigClass = __import__( 'batch_config_class' )

	# Initialization of primary variables---------------------------------------

	# Read the options----------------------------------------------------------
	# pylint: disable=invalid-name
	confInputPath=''
	confGiven=False
	ArgvNum= len(sys.argv[:])
	if ArgvNum >1 :
		for iMain in range(1, ArgvNum):
			if sys.argv[iMain]=='-i' : # or sys.argv[2]=='-p'
				if iMain+1< ArgvNum:
					confGiven=True
					confInputPath=sys.argv[iMain+1]
				else:
					logger.error("the option -i must be followed by a path to a conf file.")
	# end if
	# pylint: enable=invalid-name

	# Initialization of files, logs and redirections ---------------------------
	confMain= BatchConfigClass.ConfModel()
	if confGiven :
		confMain.read_from_file(confInputPath)

	logfilename=os.path.basename(__file__).rsplit('.py',1)[0]
	logger= MyUtils.init_logs(confMain.loglevel, logfilename)

	############
	### MAIN ###
	############
	logger.notice( f"Main {SCRIPT_NAME} sequence Script: Start (PID={os.getpid()})" )

	executionObj=ExecutorOptions(os.getpid())
	executionObj.edit_from_conf(confMain)
	retCodeMain = executionObj.run_exec()

	# check the right execution of run_exec
	if retCodeMain == 0:
		logger.notice( f"End of {SCRIPT_NAME} sequence: 0 **\n" )
	else:
		logger.error( f"End of {SCRIPT_NAME} sequence "+f"with Exit Code {retCodeMain} **\n" )

	sys.exit(retCodeMain)


######################## 	TO DO     	########################
# '''
# toto
# '''

######################## 	BUGS KNOWN     	########################
# '''
# toto
# '''

######################## 	AIDE MEMOIRE 	########################
# '''
# # Progress Bar
# print ('to')
# for x in range(10):
#     print("Progress {:2.1%}\r".format(x / 10), end='')
# '''

# EOF
