#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
	Batch configuration Class model file
"""

import os
import sys

class ConfModel():
	""" ConfModel()
		Description:
			Class ConfModel to emulate a configuration file.
		Args:
			x
		Returns:
			x
		Raises:
			x
		Examples:
			x
		Remarks: (refs, notes, ToDo, etc.)
			x
	"""
	# pylint: disable=too-many-instance-attributes
	def __init__(self):
		super().__init__()

		## General
		self.loglevel = 5
		self.binExec = ''
		self.binDir = '' # './bin'
		## Compiler
		self.flagCompile = False
		self.compiler = '' #'gnu'
		self.fast = False
		self.warnall = False
		self.static = False
		self.profile = False
		#self.binExec = '' #'OBLiXprog'
		#self.binDir = '' # './bin'
		self.buildDir = '' # '/build/'
		self.progMain = '' #'main.f90'
		self.listObjectFile = []
		self.remove = False
		self.cMakeFlag = True
		## Executor
		self.flagExecute = False
		#self.binExec = 'OBLiXprog'
		#self.binDir = '' # './bin'
		self.binExecOpt = '' # '-s '
		self.binExecVersion = False
		self.otherBinTools = []
		self.otherToolsDir = ['']*len(self.otherBinTools)
		self.inputFile = '' #'InputCommand.xml'
		self.inputFileDir = '' #'./input'
		self.secondaryInputFiles = []
		self.secondaryInputDir = ['']*len(self.secondaryInputFiles)
		self.outputDir = '' #'./output/'
		self.tmpDir = '' #'./tmp'
		self.tmpDirEnabled = False
		## Plotter
		self.flagPlot = False
		self.plotter = 'ParaView'
		self.savePicture = None
		self.saveMovie = None
		self.movieFramerate = None
		## Performance Test-Cases
		self.flagPTC=False
		self.listCompiler=[] #['gnu', 'intel', 'pgi', 'clang', 'cray']
		self.listOptionCompile=[] #['warnall', 'fast', 'static', 'faststatic', 'parallel', 'None']
		self.listTestCases= '' #'All'
	# pylint: enable=too-many-instance-attributes

	# pylint: disable=too-many-statements
	# pylint: disable=multiple-statements
	# pylint: disable=too-many-branches
	def read_from_file(self, confPath):
		""" read_from_file(self, confPath)
			Description:
				Function to change the ConfModel object according to a confFile.
			Args:
				x
			Returns:
				x
		"""
		# Process the confPath to be imported
		absolutePath= os.path.abspath( confPath )
		directory=os.path.dirname(absolutePath)
		fileName=os.path.basename(absolutePath)
		confName=fileName.split('.py')[0]

		# Import the conf file.
		sys.path.append(directory)
		# print("Directory",directory)
		# print("conFName",confName)
		# print("sys.path",sys.path)
		confImported=__import__(confName)

		# pylint: disable=line-too-long
		### make the changes
		## General
		self.loglevel = confImported.loglevel
		if hasattr(confImported,'binExec'): self.binExec = confImported.binExec
		if hasattr(confImported,'binDir'): self.binDir = confImported.binDir
		## Compiler
		if hasattr(confImported,'flagCompile'): self.flagCompile = confImported.flagCompile
		if hasattr(confImported,'compiler'): self.compiler = confImported.compiler
		if hasattr(confImported,'fast'): self.fast = confImported.fast
		if hasattr(confImported,'warnall'): self.warnall = confImported.warnall
		if hasattr(confImported,'static'): self.static = confImported.static
		if hasattr(confImported,'profile'): self.profile = confImported.profile
		#if hasattr(confImported,'binExec'): self.binExec = confImported.binExec
		#if hasattr(confImported,'binDir'): self.binDir = confImported.binDir
		if hasattr(confImported,'buildDir'): self.buildDir = confImported.buildDir
		if hasattr(confImported,'progMain'): self.progMain = confImported.progMain
		if hasattr(confImported,'listObjectFile'): self.listObjectFile = confImported.listObjectFile
		if hasattr(confImported,'remove'): self.remove = confImported.remove
		if hasattr(confImported,'CmakeFlag'): self.cMakeFlag = confImported.cMakeFlag
		## Executor
		if hasattr(confImported,'flagExecute'): self.flagExecute = confImported.flagExecute
		#if hasattr(confImported,'binExec'): self.binExec = confImported.binExec
		#if hasattr(confImported,'binDir'): self.binDir = confImported.binDir
		if hasattr(confImported,'binExecOpt'): self.binExecOpt = confImported.binExecOpt
		if hasattr(confImported,'binExecVersion'): self.binExecVersion = confImported.binExecVersion
		if hasattr(confImported,'OtherBinTools'): self.otherBinTools = confImported.otherBinTools
		if hasattr(confImported,'OtherToolsDir'): self.otherToolsDir = confImported.otherToolsDir
		if hasattr(confImported,'inputFile'): self.inputFile = confImported.inputFile
		if hasattr(confImported,'inputFileDir'): self.inputFileDir = confImported.inputFileDir
		if hasattr(confImported,'SecondaryInputFiles'): self.secondaryInputFiles = confImported.secondaryInputFiles
		if hasattr(confImported,'SecondaryInputDir'): self.secondaryInputDir = confImported.secondaryInputDir
		if hasattr(confImported,'outputDir'): self.outputDir = confImported.outputDir
		if hasattr(confImported,'tmpDir'): self.tmpDir = confImported.tmpDir
		if hasattr(confImported,'tmpDirEnabled'): self.tmpDirEnabled = confImported.tmpDirEnabled
		## Plotter
		if hasattr(confImported,'flagPlot'): self.flagPlot = confImported.flagPlot
		if hasattr(confImported,'plotter'): self.plotter = confImported.plotter
		if hasattr(confImported,'savePicture'): self.savePicture = confImported.savePicture
		if hasattr(confImported,'saveMovie'): self.saveMovie = confImported.saveMovie
		if hasattr(confImported,'movieFramerate'): self.movieFramerate = confImported.movieFramerate
		## Performance Test-Case
		if hasattr(confImported,'flagPTC'): self.flagPTC = confImported.flagPTC
		if hasattr(confImported,'listCompiler'): self.listCompiler = confImported.listCompiler
		if hasattr(confImported,'listOptionCompile'): self.listOptionCompile = confImported.listOptionCompile
		if hasattr(confImported,'listTestCases'): self.listTestCases = confImported.listTestCases
		# pylint: enable=line-too-long
		# return
	# end function
	# pylint: enable=multiple-statements
	# pylint: enable=too-many-statements

	def get_id(self):
		""" get_id(self)
			Description:
				Function to change the ConfModel object according to a confFile.
			Args:
				x
			Returns:
				x
		"""

		print( 'toto'+self.binExec )
		# return
	# end function

# end class

# EOF
