#!/usr/bin/env python3

# -*- coding: utf-8 -*-

# pylint: disable=invalid-name


"""
	LICENSE :
	Copyright (C) 2021-20** Romain NOEL

	AUTHOR(S) :
		Romain NOËL
			Mail	: romain.noel@univ-eiffel.fr
"""

# pylint: enable=invalid-name

# Import of Libraries-----------------------------------------------------------
import os 					# library for Operating System
import sys					# library for system manipulation; sys is a submodule of os
import logging				# lib to manipulate loggers
import inspect				# lib to have access to the caller of this module

import shutil				# lib for copy file and so on
import re 					# lib for regular expression

# Find chRootProject path.
# pylint: disable=no-else-break
_PREV_PATH = None
_CALLING_DIR = os.getcwd()
while True :
	if (os.path.exists('./OBLiX-README.md')) and (os.getcwd()!='/'):
		CHROOT_PATH=os.getcwd()
		break
	elif _PREV_PATH==os.getcwd() :
		print('ERROR: the chRootProject folder was not found...')
		os.chdir(_CALLING_DIR)
		sys.exit(3)
		break
	else:
		_PREV_PATH=os.getcwd()
		os.chdir('../')
os.chdir(_CALLING_DIR) # Go back to the call directory.
del _PREV_PATH
# del _callingDir
# pylint: enable=no-else-break

# Import Project Libraries
sys.path.append(CHROOT_PATH+'/bin')
MyUtils= __import__('OBLiX_utils')

# Initialization of primary variables-------------------------------------------
#
AUTHORS_NAME=["Romain NOËL"]
VERSION_NUM=1.01
VERSION_DATE="14-December-2021"
PROJECT_NAME="OBLiX"
SCRIPT_NAME="compiler_"+PROJECT_NAME
SCRIPT_DESCRIPTION= f"script python to compile {PROJECT_NAME} program with Cmake."

(frame, filename_frame, line_number, function_name, lines_frame, index_frame) = \
	inspect.getouterframes( inspect.currentframe() )[-1]
# print(frame, filename_frame, line_number, function_name, lines_frame, index_frame)
# print('caller is :', FILENAME_CALLER, 'and __name__ is :', __name__)
if filename_frame.startswith('<string>') or filename_frame.startswith('<stdin>') :
	FILENAME_CALLER='local'
else:
	# print('filename_frame='+filename_frame, flush=True)
	try:
		FILENAME_CALLER= filename_frame.rsplit('/',1)[-1].rsplit('.py',1)[0]
	except IndexError as e:
		FILENAME_CALLER= filename_frame.rsplit('\\',1)[-1].rsplit('.py',1)[0]
del frame, filename_frame, line_number, function_name, lines_frame, index_frame

logger = logging.getLogger(FILENAME_CALLER+'.'+SCRIPT_NAME)


################################ 	FUNCTIONS 	########################################

def do_backspace(inString):
	"""
		Description:
			x
		Args:
			x
		Returns:
			x
		Raises:
			x
		Examples:
			x
		Remarks: (refs, notes, ToDo, etc.)
			x
	"""
	chars = []
	for character in inString:
		if character == '\b' and chars:
			chars.pop()
		else:
			chars.append(character)
	return ''.join(chars)


class CompilerOptions():
	"""
		Description:
			x
		Attributes:
			x
		Methods:
			x
		Examples:
			x
		Remarks: (refs, notes, ToDo, etc.)
			x
	"""

	# pylint: disable=too-many-instance-attributes
	def __init__(self):
		"""
			Description:
				x
			Args:
				x
			Returns:
				x
			Raises:
				x
			Examples:
				x
			Remarks: (refs, notes, ToDo, etc.)
				x
		"""
		# super(CompilerOptions, self).__init__()
		self.verbosity= 5
		self.compiler = 'gfortran'
		self.fast = False
		self.debugWarn = False
		self.static = False
		self.profile = False
		self.parallel = False
		self.buildDir = CHROOT_PATH+'/build/'
		self.progMain= 'OBLiX_main.f90'
		self.binExec = 'OBLiXprog'
		self.binDir = CHROOT_PATH+'/bin/'
		self.listObjectFile= ['lib/lib_porPre.f90','LBM/DnQmConst_mod.f90', 'lib/lib_VTK_IO.f90',
				 'IO_mod.f90','Executor.f90'
				]
		self.remove= False
		self.objExt='.o'
		self.versionCompiler = '--version'
		self.relativePathBuild = CHROOT_PATH+'/src/'
		self.cMakeFlag=True
		self.cMakeOpt=''
		self.cMakeCmd='cmake'
		self.makeCmd='make'
		# other internal variables.
		self.cmdObjFiles=''
		self.cmdBinExec=''

		# return
	# end function
	# pylint: enable=too-many-instance-attributes

	def option_from_keyword(self, option, log=logger):
		""" option_from_keyword(self, option, log=logger):
			Description:
				Subroutine to change the options of compilation from a given keyword.
			Args:
				x
			Returns:
				x
			Raises:
				x
			Examples:
				x
			Remarks: (refs, notes, ToDo, etc.)
				x
		"""
		if option.lower() == 'warnall':
			self.fast= False
			self.debugWarn= True
			self.static= False
			self.profile= False
		elif option.lower() == 'fast':
			self.fast= True
			self.debugWarn= False
			self.static= False
			self.profile= False
		elif option.lower() == 'static':
			self.fast= False
			self.debugWarn= False
			self.static= True
			self.profile= False
		elif option.lower() == 'faststatic':
			self.fast= True
			self.debugWarn= False
			self.static= True
			self.profile= False
		elif option.lower() == 'none':
			self.fast= False
			self.debugWarn= False
			self.static= False
			self.profile= False
		else:
			log.error( f"The compiling option {option} is not recognized" )

		# return
	# end function

	# pylint: disable=too-many-statements, too-many-branches
	def create_cmd(self):
		"""
			Description:
				x
			Args:
				x
			Returns:
				x
			Raises:
				x
			Examples:
				x
			Remarks: (refs, notes, ToDo, etc.)
				x
		"""
		if self.compiler.startswith('pgi'):
			self.compiler='pgfortran'

		# Re-Init the command strings.
		self.cmdObjFiles=self.compiler+' '
		self.cmdBinExec =self.compiler+' '

		# Warnall Debug configuration.
		if self.debugWarn:
			if self.compiler.startswith('gfortran'):
				self.cmdObjFiles=self.cmdObjFiles \
					+'-fbacktrace -ffpe-trap=invalid,zero,overflow,underflow -Wall -Warray-bounds \
						 -fcheck=all -ftrapv ' # invalid,zero,overflow,underflow,inexact,denormal
				self.cmdBinExec =self.cmdBinExec \
					+'-fbacktrace -ffpe-trap=invalid,zero,overflow,underflow -Wall -Warray-bounds \
						-fcheck=all -ftrapv '
				# # invalid,zero,overflow,underflow,inexact,denormal
				# self.cmdObjFiles=self.cmdObjFiles+'-fbacktrace -ffpe-trap=underflow -Wall
				# 		-fcheck=all -ftrapv '
				# self.cmdBinExec =self.cmdBinExec +'-fbacktrace -ffpe-trap=underflow -Wall
				# 		-fcheck=all -ftrapv '
			elif self.compiler.startswith('ifort'):
				# self.cmdObjFiles=self.cmdObjFiles+'-gen-interface -warn all -check all -traceback
				# 							-fpe-all -fp-stack-check -ftrapuv -heap-arrays '
				self.cmdBinExec =self.cmdBinExec +'-gen-interface -warn all -check all -traceback \
					-fpe-all -fp-stack-check -ftrapuv -fstack-protector -implicitnone -check noshape -heap-arrays '
				self.cmdObjFiles=self.cmdObjFiles+'-gen-interface -warn all -check all -traceback \
					-fpe-all -fp-stack-check -ftrapuv -fstack-protector -implicitnone -check noshape -heap-arrays '
			elif self.compiler.startswith('pg'):#pgfortran
				# -Ktrap=inexact, denorm
				self.cmdObjFiles=self.cmdObjFiles+'-traceback -Mbounds -Mchkptr -Ktrap=divz,inv,ovf,unf '
				self.cmdBinExec =self.cmdBinExec +'-traceback -Mbounds -Mchkptr -Ktrap=divz,inv,ovf,unf '

		# Fast configuration.
		if self.fast:
			if self.compiler.startswith('gfortran'):
				self.cmdObjFiles=self.cmdObjFiles+'-O3 -Ofast -fno-stack-arrays '
				self.cmdBinExec =self.cmdBinExec +'-O3 -Ofast -fno-stack-arrays '
			elif self.compiler.startswith('ifort'):
				self.cmdObjFiles=self.cmdObjFiles+'-fast -O3 -xHost -ipo '
				self.cmdBinExec =self.cmdBinExec +'-fast -O3 -xHost -ipo '
			elif self.compiler.startswith('pg'):  #pgfortran
				self.cmdObjFiles=self.cmdObjFiles+'-fast -O4 '
				self.cmdBinExec =self.cmdBinExec +'-fast -O4 '

		# Verbosity configuration.
		if self.verbosity <= 3 :
			if self.compiler.startswith('pg'):  #pgfortran
				self.cmdObjFiles=self.cmdObjFiles+'-silent '
				self.cmdBinExec =self.cmdBinExec +'-silent '
			else:
				self.cmdObjFiles=self.cmdObjFiles+'-q '
				self.cmdBinExec =self.cmdBinExec +'-q '
		elif self.verbosity == 6 :
			if self.compiler.startswith('pg'):  #pgfortran
				self.cmdObjFiles=self.cmdObjFiles+''
				self.cmdBinExec =self.cmdBinExec +''
			else:
				self.cmdObjFiles=self.cmdObjFiles+'-g1 '
				self.cmdBinExec =self.cmdBinExec +'-g1 '
		elif self.verbosity == 7 :
			if self.compiler.startswith('gfortran'):
				self.cmdObjFiles=self.cmdObjFiles+'-g2 '
				self.cmdBinExec =self.cmdBinExec +'-g2 '
			elif self.compiler.startswith('ifort'):
				self.cmdObjFiles=self.cmdObjFiles+'-debug all -g2 '
				self.cmdBinExec =self.cmdBinExec +'-debug all -g2 '
			elif self.compiler.startswith('pg'):  #pgfortran
				self.cmdObjFiles=self.cmdObjFiles+'-Mdwarf1 -C -g '
				self.cmdBinExec =self.cmdBinExec +'-Mdwarf1 -C -g '
		elif self.verbosity == 8 :
			if self.compiler.startswith('gfortran'):
				self.cmdObjFiles=self.cmdObjFiles+'-g3 '   # '-fdump-fortran-original -g3 ' ?
				self.cmdBinExec =self.cmdBinExec +'-g3 '   # '-fdump-fortran-original -g3 ' ?
			elif self.compiler.startswith('ifort'):
				self.cmdObjFiles=self.cmdObjFiles+'-debug all -g3 '
				self.cmdBinExec =self.cmdBinExec +'-debug all -g3 '
			elif self.compiler.startswith('pg'):  #pgfortran
				self.cmdObjFiles=self.cmdObjFiles+'-Mdwarf2 -C -g -v '
				self.cmdBinExec =self.cmdBinExec +'-Mdwarf2 -C -g -v '

		# List object files configuration.
		if self.compiler.startswith('gfortran'):
			self.cmdObjFiles= self.cmdObjFiles+'-c -cpp '
			# -c to create .o object file ou -cpp for preprocess
		elif self.compiler.startswith('ifort'):
			self.cmdObjFiles= self.cmdObjFiles+'-c -heap-arrays -no-wrap-margin -fpp '
			# -no-wrap-margin if I want to removed the 77 strint length by line	-fpp  for preprocess
		elif self.compiler.startswith('pg'):  #pgfortran
			self.cmdObjFiles= self.cmdObjFiles+'-c -cpp '
			# -c to create .o object file ou -cpp for preprocess
		elif self.compiler.startswith('g++'):
			self.cmdObjFiles= self.cmdObjFiles+'-c '
		else:
			# Unknown compiler.
			logger.error( f"Unknown compiler: {self.compiler}"+f" (cMakeFlag={self.cMakeFlag})" )

		# Profiling configuration, this will create a gmon.out in the out directory
		# This gmon.out file can be read using "gprof [option] gmon.out"
		if self.profile:
			if self.compiler.startswith('gfortran'):
				self.cmdObjFiles=self.cmdObjFiles+'-fopenacc -pg -O3 -foffload=nvptx-none \
					-B /usr/lib/gcc/x86_64-linux-gnu/8/accel/nvptx-none/mkoffload \
						-B /usr/bin/x86_64-linux-gnu-accel-nvptx-none-gcc-8 '
				self.cmdBinExec =self.cmdBinExec +'-fopenacc -pg -O3 -foffload=nvptx-none \
					-B /usr/lib/gcc/x86_64-linux-gnu/8/accel/nvptx-none/mkoffload \
						-B /usr/bin/x86_64-linux-gnu-accel-nvptx-none-gcc-8 '
			elif self.compiler.startswith('ifort'):
				self.cmdObjFiles=self.cmdObjFiles+'-p '
				self.cmdBinExec =self.cmdBinExec +'-p '
			# LINUX ONLY !
			elif self.compiler.startswith('pg'):  #pgfortran
				self.cmdObjFiles=self.cmdObjFiles+'-pg '
				self.cmdBinExec =self.cmdBinExec +'-pg '

		# Static configuration
		if self.static:
			if self.compiler.startswith('gfortran'):
				self.cmdBinExec =self.cmdBinExec +'-static '
			elif self.compiler.startswith('ifort'):
				self.cmdBinExec =self.cmdBinExec +'-static '
			elif self.compiler.startswith('pg'):
				self.cmdBinExec =self.cmdBinExec +'-Bstatic '

		# Parallel configuration
		if self.parallel:
			if self.compiler.startswith('gfortran'):
				self.cmdBinExec =self.cmdBinExec +'-D _MPI_ '
			elif self.compiler.startswith('ifort'):
				self.cmdBinExec =self.cmdBinExec +'-D _MPI_  '
			elif self.compiler.startswith('pg'):
				self.cmdBinExec =self.cmdBinExec +'-D _MPI_  '

		# List object files configuration.
		if self.compiler.startswith('gfortran') :
			self.cmdBinExec=self.cmdBinExec +'-o '	 	# -o for output for preprocess
		elif self.compiler.startswith('ifort') :
			self.cmdBinExec=self.cmdBinExec +'-o '		# -o for output for preprocess
		elif self.compiler.startswith('pg'):  #pgfortran
			self.cmdBinExec=self.cmdBinExec +'-o '
			# self.objExt = '.obj' # no necessary any more: need version use .o also
		elif self.compiler.startswith('g++'):  #
			self.cmdBinExec=self.cmdBinExec

		# Create the executable command
		if self.compiler.startswith('g++'):  #pgfortran
			self.cmdBinExec= do_backspace(self.cmdBinExec +self.relativePathBuild+self.progMain \
				+' -o '+self.binExec)
		else:
			self.cmdBinExec= do_backspace(self.cmdBinExec +self.binExec+' ' +self.progMain[:-4] \
				+ self.objExt + ' ' +' '.join(map( lambda x: x[:-4].rsplit('/',1)[-1]+self.objExt, \
					self.listObjectFile )) )
		# print(self.cmdBinExec)

		# return
	# end function
	# pylint: enable=too-many-statements, too-many-branches


	def create_cmake_cmd(self):
		"""
			Description:
				x
			Args:
				x
			Returns:
				x
			Raises:
				x
			Examples:
				x
			Remarks: (refs, notes, ToDo, etc.)
				x
		"""

		self.cMakeOpt+= ' ../'
		self.cMakeOpt+= ' -DOBLiX_BUILD_EXECUTABLE=ON'

		if self.fast is True :
			self.cMakeOpt= self.cMakeOpt+' -Dfast=ON'
		if self.debugWarn is True :
			self.cMakeOpt= self.cMakeOpt+' -DdebugWarn=ON'
		if self.static is True :
			self.cMakeOpt= self.cMakeOpt+' -Dstatic=ON'
		if self.profile is True :
			self.cMakeOpt= self.cMakeOpt+' -Dprofile=ON'

		# return
	# end function


	def cleaning(self):
		"""
			Description:
				x
			Args:
				x
			Returns:
				x
			Raises:
				x
			Examples:
				x
			Remarks: (refs, notes, ToDo, etc.)
				x
		"""

		logger.notice('Cleaning of the old object files')
		filelist = [ f for f in os.listdir( self.buildDir ) if re.search(
			".*\\.o|.*\\.mod|.*\\__genmod.f90|.*\\.smod|.*\\.pdb|.*\\.dwf",
			f, re.IGNORECASE) ]
		for file in filelist:
			os.remove(file)

		# return
	# end function

	def edit_from_conf(self, conf):
		"""
			Description:
				x
			Args:
				x
			Returns:
				x
			Raises:
				x
			Examples:
				x
			Remarks: (refs, notes, ToDo, etc.)
				x
		"""
		self.verbosity = conf.loglevel
		# pylint: disable=multiple-statements
		if conf.binExec : self.binExec = conf.binExec
		if conf.binDir : self.binDir= conf.binDir

		if conf.compiler: self.compiler= conf.compiler
		if conf.fast: self.fast= True
		if conf.warnall: self.debugWarn= True
		if conf.static: self.static= True
		if conf.profile: self.profile= True
		if conf.progMain : self.progMain= conf.progMain
		if conf.buildDir: self.buildDir= conf.buildDir
		# if conf.binExec : self.binExec= conf.binExec
		# if conf.binDir : self.binDir= conf.binDir
		if conf.listObjectFile : self.listObjectFile= conf.listObjectFile
		if conf.remove : self.remove= conf.remove
		if conf.cMakeFlag: self.cMakeFlag= conf.cMakeFlag
		# pylint: enable=multiple-statements

		# return
	# end function


	def version_compiler(self):
		"""
			Description:
				x
			Args:
				x
			Returns:
				x
			Raises:
				x
			Examples:
				x
			Remarks: (refs, notes, ToDo, etc.)
				x
		"""
		listCmd=[self.compiler, self.versionCompiler]
		retCode = MyUtils.subp(listCmd)
		return retCode
	# end function


	def run_compiler(self):
		"""
			Description:
				x
			Args:
				x
			Returns:
				x
			Raises:
				x
			Examples:
				x
			Remarks: (refs, notes, ToDo, etc.)
				x
		"""

		try:
			# Cleaning the old files if present in the working directory
			self.cleaning()

			logger.notice('Beginning of the Compilation(compiler)' )

			# Store the original calling directory
			previousCallingDir=os.getcwd()

			# Create the commands to use for compiling the modules and object plus the main executable.
			self.create_cmd()

			# cd ./build
			os.chdir(self.buildDir)

			# Compilation of the modules and object files.
			for item in iter(self.listObjectFile+[self.progMain] ):
				listcmd=[]
				listcmd.extend(self.cmdObjFiles.split())
				listcmd.append(self.relativePathBuild+item)
				logger.notice( f"subp: {listcmd}" )
				retCode = MyUtils.subp(listcmd)
				if retCode[0] != 0:
					sys.exit(10)

			# Compilation of the executable program
			listcmd=[]
			listcmd.extend(self.cmdBinExec.split())
			logger.notice( f"subp: {listcmd}" )
			retCode = MyUtils.subp(listcmd)
			if retCode[0] != 0:
				sys.exit(10)
			logger.notice('Compilation finished')
			binExecLocal=self.binExec
			if sys.platform.startswith('win'):
				binExecLocal = binExecLocal + '.exe'

			# Move all the output files in the associate directory
			src = './' + binExecLocal
			dst = CHROOT_PATH+'/bin/' + binExecLocal
			shutil.copy(src, dst)
			logger.notice( f"Copy of the binary executable file '{binExecLocal}' in the "\
				f"folder {os.getcwd()+'/'+dst}: done" ) #

			if self.remove:
				self.cleaning()

			logger.notice('END of the Compilation(compiler)' )

		except Exception as exce:
			# Go back to the original calling directory.
			os.chdir(previousCallingDir)

			logger.error('Somethin went wrong during the compilation: STOP')
			raise exce

		finally :
			# Go back to the original calling directory.
			os.chdir(previousCallingDir)

		return retCode[0]
	# end run_compiler


	def run_cmake(self):
		"""
			Description:
				x
			Args:
				x
			Returns:
				x
			Raises:
				x
			Examples:
				x
			Remarks: (refs, notes, ToDo, etc.)
				x
		"""

		try:
			logger.notice('Beginning of the Compilation(CMake)' )

			# Store the original calling directory
			previousCallingDir=os.getcwd()

			#
			self.create_cmake_cmd()

			# cd ./build
			os.chdir(self.buildDir)

			# cmake ../
			listcmd=[self.cMakeCmd]+ self.cMakeOpt.split()
			retCode = MyUtils.subp(listcmd)
			if retCode[0] != 0:
				sys.exit(10)

			# make target
			listcmd=[self.makeCmd, self.binExec]
			retCode = MyUtils.subp(listcmd)
			if retCode[0] != 0:
				sys.exit(10)

			# make install
			listcmd=[self.makeCmd, 'install']
			retCode = MyUtils.subp(listcmd)
			if retCode[0] != 0:
				sys.exit(10)

			# make clean
			if self.remove:
				listcmd=[self.makeCmd, 'clean']
				retCode = MyUtils.subp(listcmd)
				if retCode[0] != 0:
					sys.exit(10)

			logger.notice('END of the Compilation' )

		except Exception as exce:
			# Go back to the original calling directory.
			os.chdir(previousCallingDir)

			logger.error('Somethin went wrong during the compilation(CMake): STOP')
			raise exce

		finally :
			# Go back to the original calling directory.
			os.chdir(previousCallingDir)

		return retCode[0]
	# end run_cmake


	def run_compilation(self):
		"""
			Description:
				x
			Args:
				x
			Returns:
				x
			Raises:
				x
			Examples:
				x
			Remarks: (refs, notes, ToDo, etc.)
				x
		"""

		if self.cMakeFlag is True:
			retCode= self.run_cmake()
		else:
			retCode= self.run_compiler()

		return retCode
	# end function


################################ 	MAIN SCRIPT 	########################################
if __name__ == '__main__':
	# Import of Libraries-------------------------------------------------------
	BatchConfigClass = __import__( 'batch_config_class' )

	# Initialization of primary variables---------------------------------------

	# Read the options----------------------------------------------------------
	# pylint: disable=invalid-name
	confInputPath=''
	confGiven=False
	ArgvNum= len(sys.argv[:])
	if ArgvNum >1 :
		for i in range(1, ArgvNum):
			if sys.argv[i]=='-i' : # or sys.argv[2]=='-p'
				if i+1< ArgvNum:
					confGiven=True
					confInputPath=sys.argv[i+1]
				else:
					logger.error("the option -i must be followed by a path to a conf file.")
	# end if
	# pylint: enable=invalid-name

	# Initialization of files, logs and redirections ---------------------------
	confMain= BatchConfigClass.ConfModel()
	if confGiven :
		confMain.read_from_file(confInputPath)

	logfilename=os.path.basename(__file__).rsplit('.py',1)[0]
	logger= MyUtils.init_logs(confMain.loglevel, logfilename)

	############
	### MAIN ###
	############
	logger.notice( f"Main {SCRIPT_NAME} sequence Script: Start (PID={os.getpid()})" )

	compilerMain= CompilerOptions()
	compilerMain.edit_from_conf(confMain)

	retCodeMain= compilerMain.run_compilation()

	# check the right execution of run_compilation
	if retCodeMain == 0:
		logger.notice( f"End of {SCRIPT_NAME} sequence: 0 **\n" )
	else:
		logger.error( f"End of {SCRIPT_NAME} sequence "+f"with Exit Code {retCodeMain} **\n" )

	sys.exit(retCodeMain)


######################## 	TO DO     	########################
# '''
# toto
# '''

######################## 	BUGS KNOWN     	########################
# '''
# toto
# '''

######################## 	AIDE MEMOIRE 	########################
# '''
# toto
# '''

# EOF
