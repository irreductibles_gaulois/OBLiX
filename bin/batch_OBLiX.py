#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# pylint: disable=invalid-name
"""
   LICENSE:
  		Copyright (C) 2021-20** Romain NOEL

  	AUTHOR(S):
		Romain NOËL
			Mail: romain.noel@univ-eiffel.fr
 			Address: Allee des Ponts et Chaussees, 44340 Bouguenais, FRANCE
"""
# pylint: enable=invalid-name


# Import of Libraries------------------------------------------------------------------------------
import os 					# library for Operating System
import sys					# library for system manipulation; sys is a submodule of os
import logging				# lib to manipulate loggers
import inspect				# lib to have access to the caller of this module

# Find chRootProject path.
# pylint: disable=no-else-break
_PREV_PATH = None
_CALLING_DIR = os.getcwd()
while True :
	if (os.path.exists('./OBLiX-README.md')) and (os.getcwd()!='/'):
		CHROOT_PATH=os.getcwd()
		break
	elif _PREV_PATH==os.getcwd() :
		print('ERROR: the chRootProject folder was not found...')
		os.chdir(_CALLING_DIR)
		sys.exit(3)
		break
	else:
		_PREV_PATH=os.getcwd()
		os.chdir('../')
os.chdir(_CALLING_DIR) # Go back to the call directory.
del _PREV_PATH
# del _CALLING_DIR
# pylint: enable=no-else-break

# Import Project Libraries
sys.path.append(CHROOT_PATH+'/bin')
MyUtils= __import__('OBLiX_utils')


# Initialization of primary variables--------------------------------------------------------------
#
AUTHORS_NAME=["Romain NOËL"]
VERSION_NUM=1.07
VERSION_DATE="1-January-2022"
PROJECT_NAME="OBLiX"
SCRIPT_NAME="batch_"+PROJECT_NAME
SCRIPT_DESCRIPTION= f"script python to schedule and batch process {PROJECT_NAME} operations."

(frame, filename_frame, line_number, function_name, lines_frame, index_frame) = \
	inspect.getouterframes( inspect.currentframe() )[-1]
# print(frame, filename_frame, line_number, function_name, lines_frame, index_frame)
# print('caller is :', FILENAME_CALLER, 'and __name__ is :', __name__)
if filename_frame.startswith('<string>') or filename_frame.startswith('<stdin>') :
	FILENAME_CALLER='local'
else:
	# print('filename_frame='+filename_frame, flush=True)
	try:
		FILENAME_CALLER= filename_frame.rsplit('/',1)[-1].rsplit('.py',1)[0]
	except IndexError as e:
		FILENAME_CALLER= filename_frame.rsplit('\\',1)[-1].rsplit('.py',1)[0]
del frame, filename_frame, line_number, function_name, lines_frame, index_frame

logger = logging.getLogger(FILENAME_CALLER+'.'+SCRIPT_NAME)


# Import compiler, executor and testCases modules
try:
	# sys.path.append(CHROOT_PATH+'/src')
	CompilerMod = __import__('compiler_OBLiX')
except ImportError as exc:
	logger.warning('import compiler_OBLiX fails')
	# pylint: disable=invalid-name
	CompilerMod = 'nothing' # To define the variable and do not create boggus.
	# pylint: enable=invalid-name
	logger.warning(exc)

sys.path.append(CHROOT_PATH+'/bin')
ExecutorMod = __import__('executor_OBLiX')
BatchConfigClass = __import__( 'batch_config_class' )

sys.path.append(CHROOT_PATH+'/testing/integration_tests')
PTCMod= __import__('perform_testcases')


################################ 	FUNCTIONS 	########################################

class BatchClass(BatchConfigClass.ConfModel):
	""" BatchClass(BatchConfigClass.ConfModel)

		Description:
			x
		Attributes:
			x
		Methods:
			x
	"""

	def __init__(self, pid, conf=None, pathConf=None):
		""" __init__(self, pathConf=None, pid)

		Description:
			constructor function.
		Args:
			pid (os.pid):
				process id required by the executor
			conf (BatchConfigClass.ConfModel, optional):
				configuration object to initialize the BatchClass object.
				Defaults to None.
			pathConf (string, optional):
				A string containing a path to a configuration file.
				The configuration file have to be from class BatchConfigClass.ConfModel
				Defaults to None.
		"""

		# Call parent constructor.
		# BatchConfigClass.ConfModel.__init__(self)
		super().__init__()

		# Get configuration from configuration object.
		if conf:
			# Get a list of attributes.
			# vars(obj) returns a dict of the attributes
			listAttrib=list(vars(conf).keys())
			# Transfert each attribute.
			for attrib in listAttrib:
				setattr( self, attrib, getattr(conf, attrib) )
			# end for
		# end if

		# Get configuration from a path to a configuration file.
		if pathConf :
			self.read_from_file(pathConf)

		# Get a compiler.
		if 'compiler_OBLiX' in sys.modules:
			self.compiler = CompilerMod.CompilerOptions()
			self.compiler.edit_from_conf( self )
		else:
			logger.warning('compiler_OBLiX module file has not been found.')
			self.compiler = None

		# Get an executor.
		self.executor= ExecutorMod.ExecutorOptions( pid )
		self.executor.edit_from_conf( self )

		# Get a plotter.
		self.plotter='ParaView'

		# Get a testor.
		self.testor= PTCMod.TestCases( [], logger, sys.stdout )
		self.testor.edit_from_conf( self )

		# return
	# end function

	def get_variables(self):
		""" get_variables()

			Description:
				Method to have a copy the compiler, executor, plotter and testor.
			Returns:
				x
		"""
		return self.compiler, self.executor, self.plotter, self.testor
	# end function

	def run_batch(self):
		"""
			Description:
				x
			Args:
				x
			Returns:
				x
			Raises:
				x
			Examples:
				x
			Remarks: (refs, notes, ToDo, etc.)
				x
		"""

		retCode=-1

		#
		if (not self.flagCompile) and (not self.flagExecute) \
				and (not self.flagPlot) and (not self.flagPTC):
			logger.warning('(run_batch) BatchClass is configured with no action to do.')

		## Compiler
		if self.flagCompile:
			if 'compiler_OBLiX' in sys.modules:
				# print(compiler)
				retCodeTemp= self.run_compilation()
			else:
				# do nothing
				logger.warning('Do_compile does nothing since compiler_OBLiX is not imported')
				retCodeTemp=267
			if retCodeTemp != 0 :
				logger.warning('run_batch (Compilation): None zero exit code')
				# raise
			retCode= max(retCode, retCodeTemp)
		## Executor
		if self.flagExecute:
			retCodeTemp= self.executor.run_exec()
			if retCodeTemp != 0 :
				logger.warning('run_batch (Execution): None zero exit code')
				# raise
			retCode= max(retCode, retCodeTemp)
		## Plotter
		if self.flagPlot:
			self.plotter= self.plotter+' to do!'
			# retCode= max(retCode, retCodeTemp)
		## Testor
		if self.flagPTC:
			retCodeTemp= self.testor.run_testcases()
			if retCodeTemp != 0 :
				logger.warning('run_batch (Testing): None zero exit code')
				# raise
			retCode= max(retCode, retCodeTemp)

		return retCode
	# end function


################################ 	MAIN SCRIPT 	########################################
if __name__ == '__main__':
	# Import of Libraries-------------------------------------------------------

	# Initialization of primary variables---------------------------------------

	# Read the options----------------------------------------------------------
	# pylint: disable=invalid-name
	confInputPath=''
	confGiven=False
	ArgvNum= len(sys.argv[:])
	if ArgvNum >1 :
		for i in range(1, ArgvNum):
			if sys.argv[i]=='-i' : # or sys.argv[2]=='-p'
				if i+1< ArgvNum:
					confGiven=True
					confInputPath=sys.argv[i+1]
				else:
					logger.error("the option -i must be followed by a path to a conf file.")
	# end if
	# pylint: enable=invalid-name

	# Initialization of files, logs and redirections ---------------------------
	confMain= BatchConfigClass.ConfModel()
	if confGiven :
		confMain.read_from_file(confInputPath)

	logfilename=os.path.basename(__file__).rsplit('.py',1)[0]
	logger= MyUtils.init_logs(confMain.loglevel, logfilename)

	############
	### MAIN ###
	############
	main_PID= os.getpid()
	logger.notice( f"Main {SCRIPT_NAME} sequence Script: Start (PID={main_PID})" )

	# retCodeMain=-1
	try:
		# print(compiler)
		batchObjMain=BatchClass(main_PID, conf=confMain)
		retCodeMain= batchObjMain.run_batch()

		# check the right execution of run_batch
		if retCodeMain == 0:
			logger.notice( f"End of {SCRIPT_NAME} sequence: 0 **\n" )
		else:
			logger.error( f"End of {SCRIPT_NAME} sequence "+f"with Exit Code {retCodeMain} **\n" )

		# sys.exit(retCodeMain)
	except Exception as exce :
		logger.error("Something went wrong in the batch sequence")
		logger.error( f"Exception: {exce} \n" +f"{SCRIPT_NAME}: Exit Code {retCodeMain} **\n" )
		raise exce
	# else:
	# 	pass
	finally:
		sys.exit(retCodeMain)


######################## 	TO DO     	########################
# '''
# toto
# '''

######################## 	BUGS KNOWN     	########################
# '''
# toto
# '''

######################## 	AIDE MEMOIRE 	########################
# '''
# toto
# '''

# EOF
