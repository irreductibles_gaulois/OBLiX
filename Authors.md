Project Authors
===============

This is where your explain when the project started, for which client, and list all devteams who contributes to it.


## Project technical leads:

* Romain NOËL - Scientific Researcher - Nantes (France)
<!-- * Bar Doe - position - City (Country) -->


## Developers:

* Thibaud TOULLIER - Postdoc Researcher - Nantes (France)
* Nathanaël GEY - Engineer Student - Nantes (France)


## All other contributors and their affiliations:

* Team Name - City (Country)

    * John Foo - position - City (Country)
    * Bar Doe - position - City (Country)

* Team Name - City (Country)

    * John Foo - position - City (Country)
    * Bar Doe - position - City (Country)


## Special thanks to
* Ali HOSSEINI (Reviewing)
* Sylvain MARTIN (Reviewing)