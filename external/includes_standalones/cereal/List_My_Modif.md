<!-- List of my modifications in Cereal Library -->
in /cereal/archives/json.hpp, change the path to the rapidJson library.
l.64 to l.68:
#include "rapidjson/prettywriter.h"
#include "rapidjson/ostreamwrapper.h"
#include "rapidjson/istreamwrapper.h"
#include "rapidjson/document.h"
#include "base64.hpp"

in /cereal/archives/xml.hpp, change the path to the rapidXML library.
l.34 to l.36:
#include "rapidxml/rapidxml.hpp"
#include "rapidxml/rapidxml_print.hpp"
#include "base64.hpp"

add at the end of cereal.hpp the exra function for optional arguments
l. 1085 to l. 1135:
  //!! ROMAIN : https://github.com/USCiLab/cereal/issues/30
  //! <summary>
  //! Save/Load an NVP if its name is located inside the text archive
  //! Returns TRUE if the value is loaded or saved
  //! </summary>
  template <typename Archive, typename T>
  bool make_optional_nvp(Archive& ar, const char* name, T&& value)
  {
    constexpr bool isTextArchive= traits::is_text_archive<Archive>::value;
    
    //Do check for node here, if not available then bail!
    if constexpr  (isTextArchive)
    {
      constexpr bool isInputArchive = std::is_base_of_v<InputArchive<Archive>, Archive>;
      constexpr bool isOutputArchive = std::is_base_of_v<OutputArchive<Archive>, Archive>;

      if constexpr (isInputArchive && !isOutputArchive)
      {
        try
        {
          ar(make_nvp(name, std::forward<T>(value)));
          // std::cout<< "make_optional_nvp: "<<name<<" HAS BEEN found:"<<value<< std::endl;
          return true;
        }
        catch(const std::exception& e)
        {
          // std::cout << e.what() << std::endl;
          // std::cout<< "make_optional_nvp: "<<name<<" not found. IGNORED"<< std::endl;
          return false;
        }
      }	
      else if constexpr (isOutputArchive)
      {
        ar(make_nvp(name, std::forward<T>(value)));
        // std::cout<< "make_optional_nvp: "<<name<<" has been written: "<<value<< std::endl;
        return true;
      }
      else
      {
        // std::cout<< "make_optional_nvp: the archive is not an input text archive nor an output text archive."<< std::endl;
        return false;
      }
    }
    else
    {
      // std::cout<< "make_optional_nvp: the archive is not a text archive."<< std::endl;
      return false;
    }
    return false;
  }// end function

  #define CEREAL_OPTIONAL_NVP(ar, T) ::cereal::make_optional_nvp(ar, #T, T)
