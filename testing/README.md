# OBLiX Testing

## Using Tests ##

### Run CMake ###

To compile the tests for this project, you need to use cmake. Here are the steps:

1. Navigate to the project directory in your terminal.
2. Run the following command to generate the Makefile with cmake, including the flag for building tests:

```sh
mkdir build
cd build
cmake -DOBLiX_TESTING=ON ..
```

If you want to add the binding tests aswell, you should add the flag `-DOBLiX_SWIG_BINDING=ON`.

### Run tests ###

1. Make sure you compiled the project with `make`. And `make install` if you added the binding tests.
2. Run with the command `make test`






