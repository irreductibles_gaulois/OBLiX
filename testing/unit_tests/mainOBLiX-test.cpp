#include "gtest/gtest.h"

#include "OBLiX/OBLiX.hpp"
#include "OBLiX/shareLog.hpp"
#include "spdlog/spdlog.h"
#include "cereal/archives/xml.hpp"

TEST(mainOBLiX, exec_from_file1) {
	// arrange
	// act
	// assert
	int errorCode;
	std::string fileName = "fakefilename";
	errorCode = OBLiX::exec_from_file(fileName);
	std::cout << "My test:" << errorCode;
	EXPECT_EQ(errorCode, 11);
}