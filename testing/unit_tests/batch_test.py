#!/usr/bin/env python3

# -*- coding: utf-8 -*-

""" This module GUI_controller is """

# Import of Libraries-----------------------------------------------------------
import os
import sys 				# library for Operating System and for system manipulation
# import logging					# lib to manipulate loggers
# import inspect					# lib to have access to the caller of this module

import warnings
import pytest #unittest     # lib for framework of testcases

# Find chRootProject path.
# pylint: disable=no-else-break
_PREV_PATH = None
_CALLING_DIR = os.getcwd()
while True :
	if (os.path.exists('./OBLiX-README.md')) and (os.getcwd()!='/'):
		CHROOT_PATH=os.getcwd()
		break
	elif _PREV_PATH==os.getcwd() :
		print('ERROR: the chRootProject folder was not found...')
		os.chdir(_CALLING_DIR)
		sys.exit(3)
		break
	else:
		_PREV_PATH=os.getcwd()
		os.chdir('../')
os.chdir(_CALLING_DIR) # Go back to the call directory.
del _PREV_PATH
# del _callingDir
# pylint: enable=no-else-break

# Import Project Libraries
sys.path.append(CHROOT_PATH+'/bin')
BatchMod = __import__('batch_OBLiX')
#my_init_scripts = __import__('my_init_scripts')

################################ 	FUNCTIONS 	########################################

# @pytest.fixture(autouse=True)
# def no_logs_gte_error(caplog):
# 	""" no_logs_gte_error(caplog)

# 		Description:
# 			Function with a pytest fixture to be runned automatically.
# 			The function check that no errors are present in logs captured by caplog.
# 		Args:
# 			caplog : a pytest fixture that capture logs.
# 	"""
# 	yield
# 	errors = [record for record in caplog.get_records('call') if record.levelno >= logging.ERROR]
# 	assert not errors


def no_logs_gte_num(caplog, logLevel):
	""" no_logs_gte_num(caplog, logLevel)

		Description:
			The function check that logs captured by caplog contain no message
			higher than a given logLevel number.
		Args:
			caplog : a pytest fixture that capture logs.
			logLevel (int): the maximum logLevel acceptable.
	"""
	errors = [record for record in caplog.get_records('call') if record.levelno >= logLevel]
	assert not errors

# pylint: disable=no-self-use
@pytest.mark.incremental
class TestBatch() : 		# TestSum(unittest.TestCase):
	''' The class to test my module with utilities functions. '''


	def test_init_variables(self): #, logs, Parser
		""" test_init_variables(self)

			Description:
				X
		"""
		# logger= logs

		# pylint: disable=invalid-name
		BatchConfigClassMod=__import__('batch_config_class')
		# pylint: enable=invalid-name

		conf= BatchConfigClassMod.ConfModel()
		conf.read_from_file( CHROOT_PATH+'/input/conf_default.py')

		pid=101
		batchObj=BatchMod.BatchClass(pid, conf=conf)
		compilerObj, executor, plotter, tester = batchObj.get_variables()
		assert executor.binExec == 'OBLiXprog'
		assert executor.inputFile == 'inputCommand_OBLiX.xml'

		assert compilerObj.binExec == 'OBLiXprog'
		# assert compilerObj.compiler == 'gnu' # this one bugs I think it comes from a python bug.
		assert compilerObj.cMakeFlag is True

		assert plotter == 'ParaView'

		assert tester.listCompiler == ['gnu', 'clang']
		assert tester.listTestCases == 'All'
	# end function

	def test_run_batch(self):
		""" test_run_batch(self)

			Description:
				X
		"""

		# pylint: disable=invalid-name
		BatchConfigClassMod=__import__('batch_config_class')
		# pylint: enable=invalid-name

		conf= BatchConfigClassMod.ConfModel()
		conf.read_from_file( CHROOT_PATH+'/input/conf_default.py')

		batchObj=BatchMod.BatchClass(os.getpid(), conf=conf)
		batchObj.loglevel=5

		warnings.warn(UserWarning( "Skip because is not ready yet" ))
		pytest.skip("Skip because Test is not ready yet", allow_module_level=True)

# pylint: enable=no-self-use


################################ 	MAIN SCRIPT 	########################################
if __name__ == '__main__':
	print('main batch_test')

	# unittest.main()


######################## 	TO DO     	########################
# '''
# toto
# '''

######################## 	BUGS KNOWN     	########################
# '''
# toto
# '''

######################## 	AIDE MEMOIRE 	########################
# '''
# toto
# '''

# EOF
