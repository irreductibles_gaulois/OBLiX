#!/usr/bin/env python3

# -*- coding: utf-8 -*-

""" This module PerfTestCases_test is """

# Import of Libraries-----------------------------------------------------------
import os
import sys 				# library for Operating System and for system manipulation
import logging					# lib to manipulate loggers
# import inspect				# lib to have access to the caller of this module

import datetime

import warnings
import pytest 					# lib for framework of testcases

# Find chRootProject path.
# pylint: disable=no-else-break
_PREV_PATH = None
_CALLING_DIR = os.getcwd()
while True :
	if (os.path.exists('./OBLiX-README.md')) and (os.getcwd()!='/'):
		CHROOT_PATH=os.getcwd()
		break
	elif _PREV_PATH==os.getcwd() :
		print('ERROR: the chRootProject folder was not found...')
		os.chdir(_CALLING_DIR)
		sys.exit(3)
		break
	else:
		_PREV_PATH=os.getcwd()
		os.chdir('../')
os.chdir(_CALLING_DIR) # Go back to the call directory.
del _PREV_PATH
# del _callingDir
# pylint: enable=no-else-break

# Import Project Libraries
sys.path.append(CHROOT_PATH+'/testing/integration_tests')
PerfTC = __import__('perform_testcases')


# Initialization of primary variables-------------------------------------------
authorsName=["Romain NOËL"]
logfilename=os.path.basename(__file__).rsplit('.py',1)[0]


################################ 	FUNCTIONS 	########################################

@pytest.fixture(autouse=True)
def no_logs_gte_error(caplog):
	""" no_logs_gte_error(caplog)

		Description:
			Function with a pytest fixture to be runned automatically.
			The function check that no errors are present in logs captured by caplog.
		Args:
			caplog : a pytest fixture that capture logs.
	"""
	yield
	errors = [record for record in caplog.get_records('call') if record.levelno >= logging.ERROR]
	assert not errors


def no_logs_gte_num(caplog, logLevel):
	""" no_logs_gte_num(caplog, logLevel)

		Description:
			The function check that logs captured by caplog contain no message
			higher than a given logLevel number.
		Args:
			caplog : a pytest fixture that capture logs.
			logLevel (int): the maximum logLevel acceptable.
	"""
	errors = [record for record in caplog.get_records('call') if record.levelno >= logLevel]
	assert not errors

# pylint: disable=unused-argument
@pytest.fixture(scope="module")
def get_ptc_obj(request, logs):
	"""get_ptc_obj(request, logs)

		Description:
			Check the initialization of the PTC object.
		Args:
			request ([type]): [description]
			logs (logging logger): [description]

		Yields:
			[type]: [description]
	"""
	print("  SETUP perform_testcases object")
	logger= logs # from conftest.py

	obj= PerfTC.TestCases([], logger, sys.stdout)
	assert obj.listTestCases == []
	yield obj

	print("  TEARDOWN perform_testcases object")
# end function
# pylint: enable=unused-argument

# pylint: disable=no-self-use
#@pytest.mark.incremental
class Test_perform_testcases() : 		# TestSum(unittest.TestCase):
	''' The class to test my module with utilities functions. '''

	# pylint: disable=invalid-name
	# pylint: disable=redefined-outer-name
	def test_launch_test(self, get_ptc_obj) : #, caplog
		""" test_launch_test(self, get_ptc_obj)

			Args:
				get_ptc_obj ([type]): [description]
		"""

		ptcObj=get_ptc_obj

		nameTest="test_launchTest_WITHOUT_fill"
		warnings.warn(UserWarning( f"Skip because {nameTest} is not ready yet" ))
		pytest.skip("Skip because Test is not ready yet", allow_module_level=True)

		nLoop=1
		ptcObj.launchTest(nLoop)
	# pylint: enable=invalid-name
	# pylint: enable=redefined-outer-name


	def test_get_id(self):
		""" test_get_id(self)
		"""
		try:
			PerfTC.get_id()
		except Exception as exce:
			pytest.fail("Unexpected error with getID ...")
			pytest.fail( f"{exce}" )
			raise


	# pylint: disable=invalid-name
	# pylint: disable=redefined-outer-name
	def test_read_time_from_logfile (self, get_ptc_obj): #, caplog
		""" test_read_time_from_logfile (self, get_ptc_obj, caplog)

			Args:
				get_ptc_obj ([type]): [description]
				caplog ([type]): [description]
		"""
		ptcObj=get_ptc_obj

		with open(CHROOT_PATH+'/testing/toto.log', 'w', encoding='utf-8') as file:
			lines=[
				'[18:18:55.025 pid.306125:OBLiX.][  info  ] OBLiX Read & Run: (Start) \n'
				'[18:18:55.025 pid.306125:OBLiX.][  info  ] Reading input. \n'
				'[18:18:55.025 pid.306125:OBLiX.][warning ] I am a Circle with radius 3.12, width 0 \n'
				'[18:19:55.014 pid.306125:OBLiX.][  info  ] OBLiX Read & Run: (End) 0. \n'
			]
			file.writelines(lines)

		numWarn, timeValue = ptcObj.read_time_from_logfile(CHROOT_PATH+'/testing/toto.log')

		os.remove(CHROOT_PATH+'/testing/toto.log')

		assert numWarn == 1
		assert timeValue == datetime.timedelta(seconds=59, microseconds=989001)
	# pylint: enable=invalid-name
	# pylint: enable=redefined-outer-name

	# pylint: disable=invalid-name
	# pylint: disable=redefined-outer-name
	def test_read_version (self, get_ptc_obj): #, caplog
		"""test_read_version (self, get_ptc_obj)

			Args:
				get_ptc_obj ([type]): [description]
		"""
		ptcObj=get_ptc_obj

		version = ptcObj.read_version()
		assert version != 'Unknown'
	# pylint: enable=invalid-name
	# pylint: enable=redefined-outer-name

	# pylint: disable=invalid-name
	# pylint: disable=redefined-outer-name
	@pytest.mark.parametrize("listComp, listOpt, testCas", [
    	# (['gfortran'], ['None'], []),
		# ([], [], ['C1x']),
		#([], [], 'All'), still have to be commented
	 	(['gfortran'], ['None'], ['C1x']),
	] )
	def test_run_testcases(self, get_ptc_obj, listComp, listOpt, testCas): #logs,
		"""test_run_testcases(self, get_ptc_obj, listComp, listOpt, testCas)

			Args:
				get_ptc_obj ([type]): [description]
				listComp ([type]): [description]
				listOpt ([type]): [description]
				testCas ([type]): [description]
		"""

		ptcObj=get_ptc_obj

		ptcObj.listCompiler=listComp
		ptcObj.listOptionCompile=listOpt
		ptcObj.listTestCases=testCas
		retCode= ptcObj.run_testcases()

		assert 0==retCode
	# pylint: enable=invalid-name
	# pylint: enable=redefined-outer-name

	# @pytest.mark.parametrize("listComp, ListOpt, TestCas, errorname", [
	# 	(['gfortranZ'], ['None'], [], FileNotFoundError),
	# 	(['gfortran'], [], [], RecursionError),
	# ] )
	# def test_runTC_errors(self, get_ptc_obj, logs, listComp, ListOpt, TestCas, errorname, caplog):
	# 	logger= logs
	# 	ptcObj=get_ptc_obj

	# 	ptcObj.listCompiler=listComp
	# 	ptcObj.listOptionCompiler=ListOpt
	# 	ptcObj.listTest=TestCas
	# 	with pytest.raises(errorname):
	# 		rc= ptcObj.run_TestCases()
	# 		logMessages=[rec.message for rec in caplog.records]
	# 		logLevel=[rec.levelname for rec in caplog.records]
	# 		if errorname == FileNotFoundError:
	# 			if logMessages[-1].endswith("No such file or directory: 'gfortranZ': 'gfortranZ'") :
	# 				raise FileNotFoundError
	# 		elif errorname == RecursionError:
	# 			if logMessages[-2].startswith("run_TestCases: If you want to run \
	# 					the compilation with simplest") : #and logLevel[-2].endswith('ERROR') :
	# 				raise RecursionError
	# 		caplog.clear() # Cleaning the log caputred

# end class
# pylint: enable=no-self-use

################################ 	MAIN SCRIPT 	########################################
if __name__ == '__main__':
	print('main perform_testcases')

	# unittest.main()


######################## 	TO DO     	########################
# '''
# toto
# '''

######################## 	BUGS KNOWN     	########################
# '''
# toto
# '''

######################## 	AIDE MEMOIRE 	########################
# '''
# toto
# '''

# EOF
