#!/usr/bin/env python3

# -*- coding: utf-8 -*-

# pylint: disable=invalid-name


""" This module is """

# pylint: enable=invalid-name


# Import of Libraries-----------------------------------------------------------
import os
import sys 				# library for Operating System and for system manipulation
import logging					# lib to manipulate loggers
# import inspect					# lib to have access to the caller of this module

import pytest #unittest     # lib for framework of testcases

# Find chRootProject path.
# pylint: disable=no-else-break
_PREV_PATH = None
_CALLING_DIR = os.getcwd()
while True :
	if (os.path.exists('./OBLiX-README.md')) and (os.getcwd()!='/'):
		CHROOT_PATH=os.getcwd()
		break
	elif _PREV_PATH==os.getcwd() :
		print('ERROR: the chRootProject folder was not found...')
		os.chdir(_CALLING_DIR)
		sys.exit(3)
		break
	else:
		_PREV_PATH=os.getcwd()
		os.chdir('../')
os.chdir(_CALLING_DIR) # Go back to the call directory.
del _PREV_PATH
# del _callingDir
# pylint: enable=no-else-break

# Import Project Libraries
sys.path.append(CHROOT_PATH+'/install')


# Initialization of primary variables-------------------------------------------
AUTHORS_NAME=["Romain NOËL"]

logfilename=os.path.basename(__file__).rsplit('.py',1)[0]

################################ 	FUNCTIONS 	########################################
@pytest.fixture(autouse=True)
def no_logs_gte_error(caplog):
	""" no_logs_gte_error(caplog)

		Description:
			Function with a pytest fixture to be runned automatically.
			The function check that no errors are present in logs captured by caplog.
		Args:
			caplog : a pytest fixture that capture logs.
	"""
	yield
	errors = [record for record in caplog.get_records('call') if record.levelno >= logging.ERROR]
	assert not errors


def no_logs_gte_num(caplog, logLevel):
	""" no_logs_gte_num(caplog, logLevel)

		Description:
			The function check that logs captured by caplog contain no message
			higher than a given logLevel number.
		Args:
			caplog : a pytest fixture that capture logs.
			logLevel (int): the maximum logLevel acceptable.
	"""
	errors = [record for record in caplog.get_records('call') if record.levelno >= logLevel]
	assert not errors


@pytest.fixture(scope="module")
def logs(request): # # pylint: disable=unused-argument
	""" logs(request)

		Description:
			X
		Args:
			request ([type]): [description]
		Yields:
			[type]: [description]
	"""
	print("  SETUP logs")
	logger = logging.getLogger(logfilename)
	yield logger
	print("  TEARDOWN logs")


# pylint: disable=no-self-use
class TestlibsPy2install() : 		# TestSum(unittest.TestCase):
	""" TestlibsPy2install

		Description:
			X
		Methods:
			X
	"""

	def test_import(self):
		"""[summary]
		"""
		sys.argv=['','-c']
		libsPyInstaller = __import__('libsPy2install')
		assert libsPyInstaller.sourceCompressed == 'DirFile2Install'
	# end function


	def test_try2install(self):
		"""[summary]
		"""
		assert True
	# end function
# end class
# pylint: enable=no-self-use



################################ 	MAIN SCRIPT 	########################################
if __name__ == '__main__':
	print('main libsPy2install_test')

	# unittest.main()


######################## 	TO DO     	########################
# '''
# toto
# '''

######################## 	BUGS KNOWN     	########################
# '''
# toto
# '''

######################## 	AIDE MEMOIRE 	########################
# '''
# toto
# '''

# EOF
