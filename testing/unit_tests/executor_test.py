#!/usr/bin/env python3

# -*- coding: utf-8 -*-

""" This module is """

# Import of Libraries-----------------------------------------------------------
import os
import sys 				# library for Operating System and for system manipulation
# import logging					# lib to manipulate loggers
# import inspect				# lib to have access to the caller of this module
import shutil

import pytest					# lib for framework of testcases

# Find chRootProject path.
# pylint: disable=no-else-break
_PREV_PATH = None
_CALLING_DIR = os.getcwd()
while True :
	if (os.path.exists('./OBLiX-README.md')) and (os.getcwd()!='/'):
		CHROOT_PATH=os.getcwd()
		break
	elif _PREV_PATH==os.getcwd() :
		print('ERROR: the chRootProject folder was not found...')
		os.chdir(_CALLING_DIR)
		sys.exit(3)
		break
	else:
		_PREV_PATH=os.getcwd()
		os.chdir('../')
os.chdir(_CALLING_DIR) # Go back to the call directory.
del _PREV_PATH
# del _callingDir
# pylint: enable=no-else-break

# Import Project Libraries
sys.path.append(CHROOT_PATH+'/bin')
ExecuteMod = __import__('executor_OBLiX')


################################ 	FUNCTIONS 	########################################

# @pytest.fixture(autouse=True)
# def no_logs_gte_error(caplog):
# 	""" no_logs_gte_error(caplog)

# 		Description:
# 			Function with a pytest fixture to be runned automatically.
# 			The function check that no errors are present in logs captured by caplog.
# 		Args:
# 			caplog : a pytest fixture that capture logs.
# 	"""
# 	yield
# 	errors = [record for record in caplog.get_records('call') if record.levelno >= logging.ERROR]
# 	assert not errors


def no_logs_gte_num(caplog, logLevel):
	""" no_logs_gte_num(caplog, logLevel)

		Description:
			The function check that logs captured by caplog contain no message
			higher than a given logLevel number.
		Args:
			caplog : a pytest fixture that capture logs.
			logLevel (int): the maximum logLevel acceptable.
	"""
	errors = [record for record in caplog.get_records('call') if record.levelno >= logLevel]
	assert not errors
# end function


def test_check_folder_file():
	""" test_check_folder_file()

		Description:
			X
	"""
	dirExisted= False
	if os.path.isdir('./tata'):
		dirExisted= True
	else:
		print(os.path.isdir('./tata'))
		os.mkdir('./tata')
	with open('./tata/toto.txt', 'w+', encoding='utf-8') as file :
		file.write('This a dummy file')
		file.close()
	retCode, folders, files = ExecuteMod.check_folder_file('./titi','toto.txt')
	assert retCode == 2
	assert folders == './titi'
	assert files == 'toto.txt'
	retCode, folders, files = ExecuteMod.check_folder_file('./tata','titi.txt')
	assert retCode == 1
	assert folders == './tata'
	assert files == 'titi.txt'
	retCode, folders, files = ExecuteMod.check_folder_file('./tata','toto.txt')
	assert retCode == 0
	# assert folders == './tata'
	# assert files == 'titi.txt'
	if not dirExisted:
		shutil.rmtree('./tata')
	else:
		os.remove('./tata/toto.txt')
	# return
# end function

@pytest.fixture(scope="module")
def get_exec_obj(request): # # pylint: disable=unused-argument
	""" get_exec_obj(request)

		Description:
			X
		Args:
			request ([type]): [description]
		Yields:
			[type]: [description]
	"""
	print("  SETUP Execution object")
	pid=1234
	os.chdir(CHROOT_PATH+'/bin')
	obj= ExecuteMod.ExecutorOptions(pid)
	assert obj.binExec == 'OBLiXprog'
	yield obj
	os.chdir(CHROOT_PATH)
	print("  TEARDOWN Execution object")
# end function


# pylint: disable=no-self-use
@pytest.mark.incremental
class TestExecute() : 		# TestSum(unittest.TestCase):
	""" TestExecute

		Description:
			X
		Methods:
			X
	"""

	def test_create_cmd_windows(self): #, logs
		""" test_create_cmd_windows(self)

			Description:
				X
		"""
		# logger= logs
		cmdStr=None
		originalOS=sys.platform
		sys.platform='win32'
		executeObj= ExecuteMod.ExecutorOptions( os.getpid() )
		cmdStr='OBLiXprog.exe -v --input=../input/params_OBLiX.xml'
		executeObj.create_cmd()
		assert executeObj.cmd == cmdStr
		sys.platform=originalOS
	# end function


	def test_create_cmd_linux(self): #, logs
		""" test_create_cmd_linux(self)

			Description:
				X
		"""
		# logger= logs
		cmdStr=None
		originalOS=sys.platform
		sys.platform='linux'
		executeObj= ExecuteMod.ExecutorOptions( os.getpid() )
		cmdStr='./OBLiXprog -v --input=../input/params_OBLiX.xml'
		executeObj.create_cmd()
		assert executeObj.cmd == cmdStr
		sys.platform=originalOS
	# end function

	# def test_edit_from_conf(self, logs, get_exec_obj, caplog):
	# 	pass
	# end function

	# def test_moving_outfiles(self, logs, get_exec_obj, caplog):
	# 	pass
	# end function

	# def test_copy_exe(self, logs, get_exec_obj, caplog):
	# 	pass
	# end function

	# def test_clean_tmp(self, logs, get_exec_obj, caplog):
	# 	pass
	# end function

	# def test_clean_destination(self, logs, get_exec_obj, caplog):
	# 	pass
	# end function

	def test_create_symlink(self, get_exec_obj, logs): # # pylint: disable=invalid-name,redefined-outer-name
		""" test_create_symlink(self, get_exec_obj)

			Description:
				X
			Args:
				get_exec_obj ([type]): [description]
		"""
		# logger is initialized in order to init logger (aka notice to exist).
		logger= logs # pylint: disable=unused-variable
		executeObj=get_exec_obj

		executeObj.inputFile="input_C1x.xml"
		executeObj.inputFileDir="./testing/integration_tests/C1x/"
		executeObj.OtherBinTools=['batch_OBLiX.py', 'OBLiX_utils.py']
		executeObj.OtherToolsDir=['./bin']
		executeObj.SecondaryInputFiles=['compiler_OBLiX.py']
		executeObj.SecondaryInputDir=['./bin']

		executeObj.create_cmd()
		executeObj.create_symlinks()

		assert os.path.islink(executeObj.outputDir+executeObj.binExec)
		assert os.path.islink(executeObj.outputDir+executeObj.inputFile)
		for files in executeObj.otherBinTools:
			assert os.path.islink(executeObj.outputDir+files)
		for files in executeObj.secondaryInputFiles:
			assert os.path.islink(executeObj.outputDir+files)

		executeObj.destruct_symlinks()

		assert not os.path.islink(executeObj.outputDir+executeObj.binExec)
		assert not os.path.islink(executeObj.outputDir+executeObj.inputFile)
		for files in executeObj.otherBinTools:
			assert not os.path.islink(executeObj.outputDir+files)
		for files in executeObj.secondaryInputFiles:
			assert not os.path.islink(executeObj.outputDir+files)

		executeObj.inputFile='input.xml'
		executeObj.inputFileDir=''
		executeObj.otherBinTools=[]
		executeObj.otherToolsDir=[]
		executeObj.secondaryInputFiles=[]
		executeObj.secondaryInputDir=[]
		#return
	# end function

	def test_run_cmd(self, get_exec_obj): #, logs, caplog # # pylint: disable=invalid-name,redefined-outer-name
		""" test_run_cmd(self, get_exec_obj)

			Description:
				X
			Args:
				get_exec_obj ([type]): [description]
		"""
		# Create this file and retest.
		# logger= logs
		executeObj=get_exec_obj

		executeObj.cmd='echo dummy.XML'
		retCode=executeObj.run_cmd()
		assert retCode == 0
	# end function


	def test_run_cmd_fails(self, get_exec_obj): #, logs, caplog # # pylint: disable=invalid-name,redefined-outer-name
		""" test_run_cmd_fails(self, get_exec_obj)

			Description:
				X
			Args:
				get_exec_obj ([type]): [description]
			Raises:
				FileNotFoundError: [description]
		"""

		# Create this file and retest.
		# logger= logs
		executeObj=get_exec_obj

		with pytest.raises(FileNotFoundError):
			executeObj.cmd='ecto dummy.XML'
			executeObj.run_cmd()
			raise FileNotFoundError
		#return
	# end function


	def test_run_exec(self, get_exec_obj, caplog): #logs, # # pylint: disable=invalid-name,redefined-outer-name
		""" test_run_exec(self, get_exec_obj, caplog)

			Description:
				X
			Args:
				get_exec_obj ([type]): [description]
				caplog ([type]): [description]
		"""
		# logger= logs
		executeObj=get_exec_obj

		# Create an error with a file that does not exist.
		executeObj.inputFile = 'dummy.xml'
		executeObj.verbosity = 7
		# This should capture an error
		# with pytest.raises(FileNotFoundError):
			# executeObj.run_exec()
			# os.chdir(CHROOT_PATH+'/bin')
			# if os.path.isfile(executeObj.inputFile):
			# 	os.remove(executeObj.inputFile)
			# 	print("Error: %s file has been deleted but it should not be there,
			# # so the next assertion with logs will fail." % executeObj.inputFile)
			# 	raise FileNotFoundError
			# else:    ## Show an error ##
			# 	print("Error: %s file not found but it should have raised an error
			# before" % executeObj.inputFile)
			# #end with pytest.raises
		executeObj.run_exec()
		# And to the logger should also capture an error message
		logMessages=[rec.message for rec in caplog.records]
		assert logMessages[0] == "Impossible to find: dummy.xml Nor dummy.xml Nor ."
		caplog.clear() # Cleaning the log caputred
	# end function

# end class
# pylint: enable=no-self-use


################################ 	MAIN SCRIPT 	########################################
if __name__ == '__main__':
	print('main execute_test')

	# unittest.main()


######################## 	TO DO         ########################
# '''
# toto
# '''

######################## 	BUGS KNOWN     ########################
# '''
# toto
# '''

######################## 	AIDE MEMOIRE   ########################
# '''
# toto
# '''

# EOF
