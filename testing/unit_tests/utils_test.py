#!/usr/bin/env python3

# -*- coding: utf-8 -*-

""" This module is """

# Import of Libraries-----------------------------------------------------------
import os
import sys 				# library for Operating System and for system manipulation
import logging					# lib to manipulate loggers
# import inspect					# lib to have access to the caller of this module

import pytest #unittest     # lib for framework of testcases

# Find chRootProject path.
# pylint: disable=no-else-break
_PREV_PATH = None
_CALLING_DIR = os.getcwd()
while True :
	if (os.path.exists('./OBLiX-README.md')) and (os.getcwd()!='/'):
		CHROOT_PATH=os.getcwd()
		break
	elif _PREV_PATH==os.getcwd() :
		print('ERROR: the chRootProject folder was not found...')
		os.chdir(_CALLING_DIR)
		sys.exit(3)
		break
	else:
		_PREV_PATH=os.getcwd()
		os.chdir('../')
os.chdir(_CALLING_DIR) # Go back to the call directory.
del _PREV_PATH
# del _callingDir
# pylint: enable=no-else-break

# Import Project Libraries
sys.path.append(CHROOT_PATH+'/bin')
MyUtils = __import__('OBLiX_utils')

# Initialization of primary variables-------------------------------------------
AUTHORS_NAME=["Romain NOËL"]

logfilename=os.path.basename(__file__).rsplit('.py',1)[0]

################################ 	FUNCTIONS 	########################################

@pytest.fixture(autouse=True)
def no_logs_gte_error(caplog):
	""" no_logs_gte_error(caplog)

		Description:
			Function with a pytest fixture to be runned automatically.
			The function check that no errors are present in logs captured by caplog.
		Args:
			caplog : a pytest fixture that capture logs.
	"""
	yield
	errors = [record for record in caplog.get_records('call') if record.levelno >= logging.ERROR]
	assert not errors
# end function


def no_logs_gte_num(caplog, logLevel):
	""" no_logs_gte_num(caplog, logLevel)

		Description:
			The function check that logs captured by caplog contain no message
			higher than a given logLevel number.
		Args:
			caplog : a pytest fixture that capture logs.
			logLevel (int): the maximum logLevel acceptable.
	"""
	errors = [record for record in caplog.get_records('call') if record.levelno >= logLevel]
	assert not errors
# end function


@pytest.fixture(scope="module")
def logs(request): # # pylint: disable=unused-argument
	""" logs(request)

		Description:
			X
		Args:
			request ([type]): [description]
		Yields:
			[type]: [description]
	"""
	print("  SETUP logs")
	logger= MyUtils.init_logs(8, logfilename)
	yield logger
	print("  TEARDOWN logs")
# end function


# pylint: disable=no-self-use
class TestUtils() : 		# TestSum(unittest.TestCase):
	""" TestUtils

		Description:
			The class to test my module with utilities functions.
		Methods:
			X
	"""

	# def test_subp_old(self, logs):
	# 	logger, f0, f1, f2, f3, f4, f5, f6, f7, f8= logs
	# 	rc, a, b= MyUtils.subp_old(['python','--version'], log=logger)
	# 	assert rc == 0
	# end function

	def test_subp(self, logs): # # pylint: disable=redefined-outer-name
		""" test_subp(self, logs)

			Description:
				X
			Args:
				logs ([type]): [description]
		"""
		# logger is initialized in order to init logger (aka notice to exist).
		logger= logs # pylint: disable=unused-variable
		retCode= MyUtils.subp(['python','--version'], log=logger)[0]
		assert retCode == 0
	# end function

	def test_stop_job(self):
		""" test_stop_job(self)

			Description:
				X
		"""
		var= True
		assert var is True
	# end function
# end class
# pylint: enable=no-self-use



################################ 	MAIN SCRIPT 	########################################
if __name__ == '__main__':
	print('main MyUtils_test')

	# unittest.main()


######################## 	TO DO     	########################
# '''
# toto
# '''

######################## 	BUGS KNOWN     	########################
# '''
# toto
# '''

######################## 	AIDE MEMOIRE 	########################
# '''
# toto
# '''

# EOF
