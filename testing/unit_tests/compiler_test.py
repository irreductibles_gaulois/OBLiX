#!/usr/bin/env python3

# -*- coding: utf-8 -*-

""" This module GUI_controller is """

# Import of Libraries-----------------------------------------------------------
import os
import sys 				# library for Operating System and for system manipulation
# import logging					# lib to manipulate loggers
# import inspect				# lib to have access to the caller of this module

# import warnings
import pytest      			# lib for framework of testcases

# Find chRootProject path.
# pylint: disable=no-else-break
_PREV_PATH = None
_CALLING_DIR = os.getcwd()
while True :
	if (os.path.exists('./OBLiX-README.md')) and (os.getcwd()!='/'):
		CHROOT_PATH=os.getcwd()
		break
	elif _PREV_PATH==os.getcwd() :
		print('ERROR: the chRootProject folder was not found...')
		os.chdir(_CALLING_DIR)
		sys.exit(3)
		break
	else:
		_PREV_PATH=os.getcwd()
		os.chdir('../')
os.chdir(_CALLING_DIR) # Go back to the call directory.
del _PREV_PATH
# del _callingDir
# pylint: enable=no-else-break

# Import Project Libraries
sys.path.append(CHROOT_PATH+'/bin')
compiler = __import__('compiler_OBLiX')

# Initialization of primary variables-------------------------------------------
#
AUTHORS_NAME=["Romain NOËL"]
VERSION_NUM=1.01
VERSION_DATE="14-December-2021"
PROJECT_NAME="OBLiX"
SCRIPT_NAME="compiler_test"
SCRIPT_DESCRIPTION="script pytest to compile OBLiX program with Cmake."


################################ 	FUNCTIONS 	########################################

# @pytest.fixture(autouse=True)
# def no_logs_gte_error(caplog):
# 	""" no_logs_gte_error(caplog)

# 		Description:
# 			Function with a pytest fixture to be runned automatically.
# 			The function check that no errors are present in logs captured by caplog.
# 		Args:
# 			caplog : a pytest fixture that capture logs.
# 	"""
# 	yield
# 	errors = [record for record in caplog.get_records('call') if record.levelno >= logging.ERROR]
# 	assert not errors


def no_logs_gte_num(caplog, logLevel):
	""" no_logs_gte_num(caplog, logLevel)

		Description:
			The function check that logs captured by caplog contain no message
			higher than a given logLevel number.
		Args:
			caplog : a pytest fixture that capture logs.
			logLevel (int): the maximum logLevel acceptable.
	"""
	errors = [record for record in caplog.get_records('call') if record.levelno >= logLevel]
	assert not errors

# pylint: disable=unused-argument
@pytest.fixture(scope="module")
def get_compile_obj(request): #, logs
	""" get_compile_obj(request)

		Description:
			Check the initialization of the compiler object.
		Args:
			request ([type]): [description]
		Yields:
			[type]: [description]
	"""
	print("  SETUP Compiler object")

	# logger= logs # from conftest.py

	obj= compiler.CompilerOptions()
	obj.CmakeFlag= False
	assert obj.cmdBinExec == ''

	yield obj

	print("  TEARDOWN Compiler object")
# end function
# pylint: enable=unused-argument

# pylint: disable=no-self-use
@pytest.mark.incremental
class TestCompiler() : 		# TestSum(unittest.TestCase):
	''' The class to test my module with utilities functions. '''

	def test_create_cmd(self, logs, get_compile_obj, caplog): # # pylint: disable=invalid-name, redefined-outer-name, unused-argument
		"""[summary]

		Args:
			logs (logging logger): is necessary, even if not used because otherwise the logger
				is not set correctly and have not NOTICE attributes for example.
			get_compile_obj ([type]): [description]
			caplog ([type]): [description]
		"""
		# logger= logs
		compileObj=get_compile_obj

		compileObj.compiler='gfortran'
		compileObj.progMain= 'main.f90'
		compileObj.binExec = 'main'
		compileObj.objExt='.o'
		compileObj.listObjectFile=[]
		compileObj.create_cmd()
		assert compileObj.cmdBinExec.startswith('gfortran -o main main.o')

		compileObj.compiler='ifort'
		compileObj.create_cmd()
		assert compileObj.cmdBinExec.startswith('ifort -o main main.o')

		compileObj.compiler='pgi'
		compileObj.create_cmd()
		assert compileObj.cmdBinExec.startswith('pgfortran -o main main.o')

		compileObj.compiler='XXXXXXX'
		compileObj.create_cmd()
		logMessages=[rec.message for rec in caplog.records]
		assert logMessages[0] == 'Unknown compiler: XXXXXXX (cMakeFlag=True)'
		caplog.clear() # Cleaning the log caputred


	def test_version_compiler(self, get_compile_obj): #, caplog # # pylint: disable=invalid-name, redefined-outer-name
		""" test_version_compiler(self, get_compile_obj)

			Description:
				x
			Args:
				get_compile_obj ([type]): [description]
		"""

		# Just test if the compiler is recognized.
		compileObj=get_compile_obj
		compileObj.compiler='g++'
		retCode=compileObj.version_compiler()
		assert retCode[0] == 0


	def test_run_compiler(self, get_compile_obj, caplog): # # pylint: disable=invalid-name, redefined-outer-name
		""" test_run_compiler(self, get_compile_obj, caplog)

			Description:
				x
			Args:
				get_compile_obj ([type]): [description]
				caplog ([type]): [description]
		"""
		compileObj=get_compile_obj

		#
		if sys.platform.startswith('win'):
			assert True

		# Just test if the compiler is compiling someting.
		compileObj.compiler='g++'
		compileObj.objExt='.o'
		compileObj.progMain= 'hello.c'
		compileObj.binExec = 'hello'
		compileObj.listObjectFile= []
		compileObj.cMakeFlag= False

		compileObj.buildDir= CHROOT_PATH+'/testing/unit_tests/'
		with open(CHROOT_PATH+'/src/'+'hello.c', 'w', encoding='utf-8') as file:
			file.writelines(
				"""
				#include <stdio.h>

				int
				main (void)
				{
					printf ("Hello, world!");
					return 0;
				}
				"""
			)
		compileObj.run_compilation()
		os.remove(CHROOT_PATH+'/src/'+'hello.c')
		os.remove(CHROOT_PATH+'/bin/'+'hello')
		os.remove(CHROOT_PATH+'/testing/unit_tests/'+'hello')
		os.remove(CHROOT_PATH+'/testing/unit_tests/'+'hello.o')

		# Run compilation with wrong compiler should return Error !
		with pytest.raises(FileNotFoundError):
			compileObj.compiler='XXXXXXX'
			compileObj.run_compilation()
		logMessages=[rec.message for rec in caplog.records]
		assert logMessages[1] == 'Somethin went wrong during the compilation: STOP'
		caplog.clear() # Cleaning the log caputred

		# warnings.warn(UserWarning("Not ready Yet"))
		# pytest.skip("Not ready Yet", allow_module_level=True)


	def test_run_cmake(self, get_compile_obj): #, caplog # # pylint: disable=invalid-name, redefined-outer-name
		""" test_run_cmake(self, get_compile_obj)

			Description:
				X
			Args:
				get_compile_obj ([type]): [description]
		"""
		compileObj=get_compile_obj

		compileObj.cMakeFlag= True
		compileObj.buildDir= CHROOT_PATH+'/build/'
		compileObj.binExec= 'OBLiXprog'
		# print( "ICI !!! {}".format(os.getcwd()))
		# print( "compiler !!! {}".format( compileObj ))
		retCode= compileObj.run_compilation()
		assert retCode == 0

# end class
# pylint: enable=no-self-use

################################ 	MAIN SCRIPT 	########################################
if __name__ == '__main__':
	print('main compiler_test')

######################## 	TO DO     	########################
# '''
# toto
# '''

######################## 	BUGS KNOWN     	########################
# '''
# toto
# '''

######################## 	AIDE MEMOIRE 	########################
# '''
# toto
# '''

# EOF
