#include "iostream"
#include "gtest/gtest.h"

// #include "shareLog.hpp"
// #include "baseX.hpp"
#include "OBLiX/mapInOut.hpp"

// class MyFixture : public testing::Test {
//  public:
//   // All of these optional, just like in regular macro usage.
//   static void SetUpTestSuite() { ... }
//   static void TearDownTestSuite() { ... }
//   void SetUp() override { ... }
//   void TearDown() override { ... }
// };

// class BaseXCLASS : public testing::Test {
//     //
// };

TEST(mapInOutClass, set_store_address) {
	// arrange
	// act
	// assert
	OBLiX::MapInOutClass mapIn;

	// testing::internal::CaptureStdout();
	// std::cout << "My test";
	// std::string output = testing::internal::GetCapturedStdout();

	// std::shared_ptr<double> save = make_shared<double>();
	std::shared_ptr<double> length = std::make_shared<double>(0.1);
	// save.reset( length.get() );

	mapIn.set_store_address("length", length);

	EXPECT_EQ(*mapIn.get<double>("length"), 0.1);

	EXPECT_EQ(mapIn.get_type_str("length"), typeid(double).name());
}