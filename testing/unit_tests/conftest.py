#!/usr/bin/env python3

# -*- coding: utf-8 -*-

""" This module is """

import os
import sys 				# library for Operating System and for system manipulation
# import logging			# lib to manipulate loggers

import pytest

logfilename=os.path.basename(__file__).rsplit('.py',1)[0]

# Find chRootProject path.
# pylint: disable=no-else-break
_PREV_PATH = None
_CALLING_DIR = os.getcwd()
while True :
	if (os.path.exists('./OBLiX-README.md')) and (os.getcwd()!='/'):
		CHROOT_PATH=os.getcwd()
		break
	elif _PREV_PATH==os.getcwd() :
		print('ERROR: the chRootProject folder was not found...')
		os.chdir(_CALLING_DIR)
		sys.exit(3)
		break
	else:
		_PREV_PATH=os.getcwd()
		os.chdir('../')
os.chdir(_CALLING_DIR) # Go back to the call directory.
del _PREV_PATH
# del _callingDir
# pylint: enable=no-else-break

# Import Project Libraries
sys.path.append(CHROOT_PATH+'/bin')
MyUtils = __import__('OBLiX_utils')

#
conftestDetected=True # pylint: disable=invalid-name

######################################################################

@pytest.fixture(scope="session")
def logs(request): # # pylint: disable=unused-argument
	""" logs(request)

		Description:
			X
		Args:
			request ([type]): [description]
		Yields:
			[type]: [description]
	"""
	print("  SETUP conftest logs seesion")
	logger= MyUtils.init_logs(8, logfilename)
	yield logger
	print("  TEARDOWN conftest logs seesion")
# end function


@pytest.fixture(scope="session")
def parser(request): # # pylint: disable=unused-argument
	""" parser(request)

		Description:
			X
		Args:
			request ([type]): [description]
		Yields:
			[type]: [description]
	"""
	print("  SETUP conftest Parser seesion")
	import argparse # pylint: disable=import-outside-toplevel

	parserObj=argparse.ArgumentParser(prefix_chars='-+', formatter_class=argparse.RawTextHelpFormatter)
	parserObj.add_argument('-i', '--input', help='Input file name', dest='input', action="store")
	# groupe exclusive
	groupExclusiveV = parserObj.add_mutually_exclusive_group()
	groupExclusiveV.add_argument('-v', '--verbose', action="count", default=4, dest='loglevel')
	groupExclusiveV.add_argument('-q', '--quiet', action="store_const", const=3, dest='loglevel')
	groupExclusiveV.add_argument('-d', '--debug', action="store_const", dest="loglevel", const=8)

	args = parser.parse_args([]) # read args
	yield args
	print("  TEARDOWN conftest Parser seesion")
# end function


def pytest_runtest_makereport(item, call):
	""" pytest_runtest_makereport(item, call)

		Description:
			Create a new marker at beginning of the run.
		Args:
			item ([type]): [description]
			call ([type]): [description]
	"""
	if "incremental" in item.keywords:
		if call.excinfo is not None:
			parent = item.parent
			parent._previousfailed = item # pylint: disable=protected-access
# end function


def pytest_runtest_setup(item):
	""" pytest_runtest_setup(item)

		Description:
			Setup what to do with markers at beginning of the run.
		Args:
			item ([type]): [description]
	"""
	if "incremental" in item.keywords:
		previousfailed = getattr(item.parent, "_previousfailed", None)
		if previousfailed is not None:
			pytest.xfail( f"previous test failed ({previousfailed.name})" )
# end function


def pytest_configure(config):
	""" pytest_configure(config)

		Description:
			To configure some options such as declare markers.
		Args:
			config ([type]): [description]
	"""
	config.addinivalue_line(
		"markers", "incremental: mark test to run test from class incrementally"
	)
# end function

#EoF
