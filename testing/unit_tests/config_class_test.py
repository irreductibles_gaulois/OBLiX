#!/usr/bin/env python3

# -*- coding: utf-8 -*-

""" This module PerfTestCases_test is """

# Import of Libraries-----------------------------------------------------------------------------
import os
import sys 				# library for Operating System and for system manipulation
# import logging					# lib to manipulate loggers
# import inspect					# lib to have access to the caller of this module

# import warnings
import pytest #unittest     # lib for framework of testcases

# Find chRootProject path.
# pylint: disable=no-else-break
_PREV_PATH = None
_CALLING_DIR = os.getcwd()
while True :
	if (os.path.exists('./OBLiX-README.md')) and (os.getcwd()!='/'):
		CHROOT_PATH=os.getcwd()
		break
	elif _PREV_PATH==os.getcwd() :
		print('ERROR: the chRootProject folder was not found...')
		os.chdir(_CALLING_DIR)
		sys.exit(3)
		break
	else:
		_PREV_PATH=os.getcwd()
		os.chdir('../')
os.chdir(_CALLING_DIR) # Go back to the call directory.
del _PREV_PATH
# del _callingDir
# pylint: enable=no-else-break

# Import Project Libraries
sys.path.append(CHROOT_PATH+'/testing/integration_tests')
BCCMod = __import__('batch_config_class')


# Initialization of primary variables-------------------------------------------------------------
AUTHORS_NAME=["Romain NOËL"]

logfilename=os.path.basename(__file__).rsplit('.py',1)[0]


################################ 	FUNCTIONS 	########################################

class TestConfigModel() : 		# TestSum(unittest.TestCase):
	"""[summary]

		Description:
			X
		Methods:
			X
	"""

	def test_read_from_file(self): # # pylint: disable=no-self-use
		""" test_read_from_file(self)

			Description:
				X
		"""
		#logger, f0, f1, f2, f3, f4, f5, f6, f7, f8= logs
		confObj=BCCMod.ConfModel()
		assert confObj.loglevel == 5

		confFile=CHROOT_PATH+'/input/conf_default.py'
		confObj.read_from_file(confFile)
		assert confObj.loglevel == 6

	def test_error_read_from_file(self): #, caplog # # pylint: disable=no-self-use
		""" test_ERROR_read_from_file(self)

			Description:
				X
		"""
		confObj=BCCMod.ConfModel()
		assert confObj.loglevel == 5

		with pytest.raises(ModuleNotFoundError) : # as e_info
			confFile=CHROOT_PATH+'/input/conf_defaultXXXX.py'
			confObj.read_from_file(confFile)


################################ 	MAIN SCRIPT 	########################################
if __name__ == '__main__':
	print('main TestConfigModel')

	# unittest.main()


######################## 	TO DO     	########################
# '''
# toto
# '''

######################## 	BUGS KNOWN     	########################
# '''
# toto
# '''

######################## 	AIDE MEMOIRE 	########################
# '''
# toto
# '''

# EOF
