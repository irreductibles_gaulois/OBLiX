#include "gtest/gtest.h"

#include "OBLiX/shareLog.hpp"
// #include "OBLiX/baseX.hpp"
// #include "OBLiX/version.hpp"

// class MyFixture : public testing::Test {
//  public:
//   // All of these optional, just like in regular macro usage.
//   static void SetUpTestSuite() { ... }
//   static void TearDownTestSuite() { ... }
//   void SetUp() override { ... }
//   void TearDown() override { ... }
// };

// class baseXCLASS : public testing::Test {
//     //
// };

TEST(BaseX, logger) {
	// arrange
	// act
	// assert
	std::string loggerNameTest = "empty_logger";
	EXPECT_EQ(shareLog::loggerName, loggerNameTest);
}