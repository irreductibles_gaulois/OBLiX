#include "gtest/gtest.h"

// #include "shareLog.hpp"
#include "spdlog/sinks/ostream_sink.h"
// #include "baseX.hpp"
#include "OBLiX/version.hpp"

TEST(version, testMajorVersion) {
	// arrange
	// act
	// assert
	EXPECT_EQ(OBLIX_VER_MAJOR, 1);
}

TEST(version, testVersionLogger) {

	std::ostringstream oss;

	auto consoleSink = std::make_shared<spdlog::sinks::ostream_sink_st>(oss);
	consoleSink->set_pattern("[%H:%M:%S proc.%P][%^%-8l%$]%n:%@| %v");
	spdlog::logger logger("OBLiX", {consoleSink});
	logger.set_level(spdlog::level::trace); // to not bypass the previous setting

	// register it if you need to access it globally
	auto pLog = shareLog::setup_logger(logger);

	OBLiX::print_version();
	// std:: cout << oss.str() << std::endl;
	std::string testString = oss.str();

	EXPECT_TRUE(testString.find("OBLiX Version:") != std::string::npos);
}