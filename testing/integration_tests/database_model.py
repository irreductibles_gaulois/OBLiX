#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" database_model

	Description:
		This code will talk with the database to get the data we need !
	Raises:
		exc: [description]
		exce: [description]
	Returns:
		[type]: [description]
"""

# Module imports
import xml.etree.ElementTree as ET
# import sys

#
class ResultToStock(): # pylint: disable=too-few-public-methods, too-many-instance-attributes
	""" ResultToStock

		Description:
			x
		Attributes:
			x
		Methods:
			x
	"""
	def __init__(self):
		self.codeVersion = ""
		self.macAddress = ""
		self.machineOs = ""
		self.nameTest = ""
		self.compilerName=""
		self.optionCompilation = ""
		self.resultTime = ""
		self.resultPrecision = ""
		self.resultWarnErr = ""
	# end function
# end class

#
def indent(elem, level=0):
	""" indent(elem, level=0)

		Description:
			x
		Args:
			elem ([type]): [description]
			level (int, optional): [description]. Defaults to 0.
	"""
	i = "\n" + level*"  "
	if len(elem):
		if not elem.text or not elem.text.strip():
			elem.text = i + "  "
		if not elem.tail or not elem.tail.strip():
			elem.tail = i
		for elem in elem: # pylint: disable=redefined-argument-from-local
			indent(elem, level+1)
		if not elem.tail or not elem.tail.strip():
			elem.tail = i
	else:
		if level and (not elem.tail or not elem.tail.strip()):
			elem.tail = i
	# end if
# end function

#
def add_test_result(testResults, bddPath): # pylint: disable=too-many-locals
	""" add_test_result(testResults, bddPath)

		Descritpion:
			This function adds the result of a test to the XML file we give it.
		Args:
			testResults ([type]): [description]
			bddPath ([type]): [description]
	"""

	# Gets the full tree in the xml file if there is already something. Otherwise, creates the tree
	try:
		tree = ET.parse(bddPath)
		root = tree.getroot()
	except Exception as exce:
		print('There are no data on the XML File')
		root = ET.Element('XMLTests')
		raise exce

	numSubTrees = len(root)
	# encoding = '<?xml version="1.0" encoding="UTF-8"?>\n'

	# Creates the testResult to add to the tree
	try:
		testResult = ET.SubElement(root,'TestResults')
		testResult.set("data-id", str(numSubTrees+1))
		codeVersion = ET.SubElement(testResult, "CodeVersion")
		codeVersion.text = testResults.codeVersion
		macAddress = ET.SubElement(testResult, "macAddress")
		macAddress.text = testResults.macAddress
		machineOs = ET.SubElement(testResult, "MachineOS")
		machineOs.text = testResults.machineOs
		nameTest = ET.SubElement(testResult, "NameTest")
		nameTest.text = testResults.nameTest
		compilerName = ET.SubElement(testResult, "compilerName")
		compilerName.text = testResults.compilerName
		optionCompilation = ET.SubElement(testResult, "OptionCompilation")
		optionCompilation.text = testResults.optionCompilation
		resultTime = ET.SubElement(testResult, "ResultTime")
		resultTime.text = testResults.resultTime
		resultPrecision = ET.SubElement(testResult, "ResultPrecision")
		resultPrecision.text = testResults.resultPrecision
		resultWarnErr = ET.SubElement(testResult, "ResultWarnErr")
		resultWarnErr.text = testResults.resultWarnErr
	except Exception as exce:
		print('[ERROR]','Error in add_test_result in database_model')
		print(exce)
		# return(1)
		raise exce

	# Converts the XML to a string so we can write it in the file
	# myData = ET.tostring(root).decode("utf-8")
	#
	# # Opens the xml file and writes in it
	# database = open(bddPath,"w")
	# database.write(encoding + myData)

	indent(root)
	# writing xml
	tree.write(bddPath, encoding="utf-8", xml_declaration=True)
# end function


#
def search_and_change(data, bddPath):
	""" search_and_change(data, bddPath)

	Description:
		This function searches if there is already a test with the same options stored and
		if that's the case it changes the values of time, precision and warn/Err obtained
	Args:
		data ([type]): [description]
		ddPath ([type]): [description]
	Raises:
		exc: [description]
		exc: [description]
	"""
	changes=False
	try:
		tree = ET.parse(bddPath)
		root = tree.getroot()
		for result in root.iter('TestResults'):
			try:
				if (      result[0].text==data.codeVersion 	# pylint: disable=too-many-boolean-expressions
						and result[1].text==data.macAddress
						and result[2].text==data.machineOs
						and result[3].text==data.nameTest
						and result[4].text==data.compilerName
						and result[5].text==data.optionCompilation ) :
					result[6].text=data.resultTime
					result[7].text=data.resultPrecision
					result[8].text=data.resultWarnErr
					changes=True
			except Exception as exc:
				print('[WARNing]','Problem with if in search_and_change in DB_Model')
				print(exc)
				# return('1')
				raise exc
		if changes:
			encoding = '<?xml version="1.0" encoding="UTF-8"?>\n'
			myData = ET.tostring(root).decode("utf-8")
			with open(bddPath, "w", encoding='utf-8') as database:
				database.write(encoding + myData)
		else:
			add_test_result(data,bddPath)
	except Exception as exc:
		print( f"There are no data on the XML File: {exc}" )
		add_test_result(data,bddPath)
		raise exc
	# return
# end function

#
def read_database_from_version(bddPath, codeVersion):
	""" read_database_from_version(bddPath, codeVersion)

		Description:
			x
		Args:
			bddPath ([type]): [description]
			codeVersion ([type]): [description]
		Raises:
			exc: [description]
		Returns:
			[type]: [description]
	"""
	try:
		tree = ET.parse(bddPath)
		root = tree.getroot()
		databaseResults = []
		for result in root.iter('TestResults'):
			if result[0].text==codeVersion:
				resultNow = []
				resultNow.append(result[0].text)
				resultNow.append(result[1].text)
				resultNow.append(result[2].text)
				resultNow.append(result[3].text)
				resultNow.append(result[4].text)
				resultNow.append(result[5].text)
				resultNow.append(result[6].text)
				resultNow.append(result[7].text)
				resultNow.append(result[8].text)
				databaseResults.append(resultNow)
	except Exception as exc:
		print( f"There are no data on the XML File: {exc}" )
		root = ET.Element('XMLTests')
		raise exc
	return databaseResults
# end function

#
def read_database_from_filter(bddPath, filter): # pylint: disable=redefined-builtin
	""" read_database_from_filter(bddPath, filter)

		Description:
			x
		Args:
			bddPath ([type]): [description]
			filter ([type]): [description]
		Raises:
			exce: [description]
		Returns:
			[type]: [description]
	"""
	try:
		tree = ET.parse(bddPath)
		root = tree.getroot()
		databaseResults = []
		if filter==[]:
			for result in root.iter('TestResults'):
				resultNow = []
				resultNow.append(result[0].text)
				resultNow.append(result[1].text)
				resultNow.append(result[2].text)
				resultNow.append(result[3].text)
				resultNow.append(result[4].text)
				resultNow.append(result[5].text)
				resultNow.append(result[6].text)
				resultNow.append(result[7].text)
				resultNow.append(result[8].text)
				databaseResults.append(resultNow)
			# return(databaseResults)
		else :
			for result in root.iter('TestResults'):
				# pylint: disable=too-many-boolean-expressions
				if    (result[0].text==filter[0] or filter[0]=='-') and \
						(result[1].text==filter[1] or filter[1]=='-') and \
						(result[2].text==filter[2] or filter[2]=='-') and \
						(result[3].text==filter[3] or filter[3]=='-') and \
						(result[4].text==filter[4] or filter[4]=='-') and \
						(result[5].text==filter[5] or filter[5]=='-') and \
						(result[6].text==filter[6] or filter[6]=='-') and \
						(result[7].text==filter[7] or filter[7]=='-') :
					# pylint: enable=too-many-boolean-expressions
					resultNow = []
					resultNow.append(result[0].text)
					resultNow.append(result[1].text)
					resultNow.append(result[2].text)
					resultNow.append(result[3].text)
					resultNow.append(result[4].text)
					resultNow.append(result[5].text)
					resultNow.append(result[6].text)
					resultNow.append(result[7].text)
					resultNow.append(result[8].text)
					databaseResults.append(resultNow)
			# return(databaseResults)
	except Exception as exce:
		print( f"read_database_from_filter:There are no data on the XML File: {exce}" )
		root = ET.Element('XMLTests')
		raise exce
	return databaseResults
# end function


# End of the library part of the file, that can be call from other files.
# Beginning of the script part of the file.
if __name__=='__main__':

	# test = ResultToStock()
	# tree = ET.parse("../Test/bddxmltest2.xml")
	# root = tree.getroot()
	#
	# search_and_change(test,"../Test/bddxmltest2.xml")
	# add_test_result(test,"../Test/bddxmltest2.xml")

	rootMain = ET.fromstring("<fruits><fruit>banana</fruit><fruit>apple</fruit></fruits>""")
	treeMain = ET.ElementTree(rootMain)

	indent(rootMain)
	# writing xml
	treeMain.write("./example.xml", encoding="utf-8", xml_declaration=True)
	print (treeMain, rootMain)

# EoF
