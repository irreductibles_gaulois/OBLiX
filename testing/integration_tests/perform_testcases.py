#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
	Module Description:
		x
	Classes:
		x
	Functions:
		x
	Main:
		x
	Examples:
		x
	Remarks: (refs, notes, ToDo, etc.)
		x
"""


# Import of Libraries-----------------------------------------------------------
# Loading standard modules
import os
import sys
import logging
import inspect
import subprocess
import datetime

# Find chRootProject path.
# pylint: disable=no-else-break
_PREV_PATH = None
_CALLING_DIR = os.getcwd()
while True :
	if (os.path.exists('./OBLiX-README.md')) and (os.getcwd()!='/'):
		CHROOT_PATH=os.getcwd()
		break
	elif _PREV_PATH==os.getcwd() :
		print('ERROR: the chRootProject folder was not found...')
		os.chdir(_CALLING_DIR)
		sys.exit(3)
		break
	else:
		_PREV_PATH=os.getcwd()
		os.chdir('../')
os.chdir(_CALLING_DIR) # Go back to the call directory.
del _PREV_PATH
# del _callingDir
# pylint: enable=no-else-break

# Import Project Libraries
sys.path.append(CHROOT_PATH+'/bin')
MyUtils= __import__('OBLiX_utils')
BatchConfigClass=__import__('batch_config_class')
ExecutorMod=__import__('executor_OBLiX')
CompilerMod=__import__('compiler_OBLiX')
DBModel=__import__('database_model')

# Initialization of primary variables-------------------------------------------------------------
#
AUTHORS_NAME=["Romain NOËL"]
VERSION_NUM=1.01
VERSION_DATE="29-December-2021"
PROJECT_NAME="OBLiX"
SCRIPT_NAME="Perform_TestCases"
SCRIPT_DESCRIPTION="This script is made to launch a performance test cases for OBLiX."

(frame, filename_frame, line_number, function_name, lines_frame, index_frame) = \
	inspect.getouterframes( inspect.currentframe() )[-1]
# print(frame, filename_frame, line_number, function_name, lines_frame, index_frame)
# print('caller is :', FILENAME_CALLER, 'and __name__ is :', __name__)
if filename_frame.startswith('<string>') or filename_frame.startswith('<stdin>') :
	FILENAME_CALLER='local'
else:
	# print('filename_frame='+filename_frame, flush=True)
	try:
		FILENAME_CALLER= filename_frame.rsplit('/',1)[-1].rsplit('.py',1)[0]
	except IndexError as e:
		FILENAME_CALLER= filename_frame.rsplit('\\',1)[-1].rsplit('.py',1)[0]
del frame, filename_frame, line_number, function_name, lines_frame, index_frame

logger = logging.getLogger(FILENAME_CALLER+'.'+SCRIPT_NAME)

################################ 	FUNCTIONS 	########################################

def is_docker():
	""" is_docker()
		Description:
			Function that return True if it runned in a Docker.
		Returns:
			Bool: Bool = True if in Docker, Bool = False otherwise.
	"""
	# pylint: disable=unspecified-encoding
	path = '/proc/self/cgroup'
	return (
		os.path.exists('/.dockerenv') or
		os.path.isfile(path) and any('docker' in line for line in open(path))
	)
	# pylint: enable=unspecified-encoding
# end function

def get_id():
	""" get_id
		Description:
			x
		Args:
			x
		Returns:
			x
		Raises:
			x
		Examples:
			x
		Remarks: (refs, notes, ToDo, etc.)
			x
	"""
	returnedId=''
	if not is_docker():
		if 'nt' in os.name:
			# windows
			res=subprocess.check_output('wmic csproduct get uuid'.split())
			# return res.strip().decode()
		else:
			# linux commands:
			# sudo dmidecode -s system-uuid
			# hal-get-property --udi /org/freedesktop/Hal/devices/computer --key system.hardware.uuid
			# lsblk -n -o UUID,MOUNTPOINT | grep -Po '.*(?= /$)'
			res=subprocess.check_output(["lsblk -n -o UUID,MOUNTPOINT | grep -Po '.*(?= /$)'"], shell=True)
		# end if
		returnedId= res.strip().decode()
	else:
		returnedId= None
	# end if
	return returnedId
# end function


class TestCases():
	"""
		Description:
			x
		Args:
			x
		Returns:
			x
		Raises:
			x
		Examples:
			x
		Remarks: (refs, notes, ToDo, etc.)
			x
	"""
	# pylint: disable=redefined-outer-name
	# pylint: disable=too-many-instance-attributes
	def __init__(self, listTest, logger=logger, outstream=sys.stdout):
		"""
			Description:
				x
			Args:
				x
			Returns:
				x
			Raises:
				x
			Examples:
				x
			Remarks: (refs, notes, ToDo, etc.)
				x
		"""

		# Other internal variables
		self.verbosity = 5
		self.outstream = outstream
		self.logger = logger
		self.sep='/'
		if sys.platform == 'linux':
			self.sep='/'
		elif sys.platform.startswith('win'):
			self.sep='\\'
		else:
			self.logger.warning('OS unknown: not Unix nor Windows')
		self.perfCaseDir = CHROOT_PATH +self.sep+ "testing/integration_tests/"
		self.pathDB=self.perfCaseDir+"db_ptc.xml"
		self.substring4conf='_conf.py'
		self.logNames='/Results/OBLiX.log'
		self.fileNormalizedError='/CalcNormErrMetrics.py'
		# internal variables that can be tuned.
		self.listCompiler = []
		self.listOptionCompile = []
		self.listTestCases= listTest

		#return
	# pylint: enable=too-many-instance-attributes
	# pylint: enable=redefined-outer-name
	# end function

	def edit_from_conf(self, conf):
		"""
			Description:
				x
			Args:
				x
			Returns:
				x
			Raises:
				x
			Examples:
				x
			Remarks: (refs, notes, ToDo, etc.)
				x
		"""
		# pylint: disable=multiple-statements
		self.verbosity = conf.loglevel
		# if conf.binExec : self.binExec = conf.binExec

		if conf.listCompiler : self.listCompiler= conf.listCompiler
		if conf.listOptionCompile : self.listOptionCompile= conf.listOptionCompile
		if conf.listTestCases: self.listTestCases= conf.listTestCases
		# pylint: enable=multiple-statements
		# return
	# end function


	# def launchTest_fromLoop(self,loop):
	# 	""" launchTest is a subroutine that does for each element of the list self.list """
	# 	print('[Perform_TestCases]', 'Start of the launchTest')
	# 	for folder in self.listFoldersToTest:
	# 		if os.path.isdir(self.perfCaseDir + self.sep + folder) == True and folder in self.listTest:
	# 			listFiles=os.listdir(self.perfCaseDir+self.sep+folder)
	# 			for file in listFiles:
	# 				if file.endswith("conf.py")==True:
	# 					self.argCmd=self.verboseCmd+self.inputCmd+self.perfCaseDir+self.sep+folder+self.sep+file
	# 					try :
	# 						print('[Perform_TestCases]',sys.executable,self.chrootDir+"/bin/batch_OBLiX.py",
	# 								"-i",self.chrootDir+'/testing/integration_tests/'+folder+'/'+file)
	# 						retCode= MyUtils.subp_from_loop(
	# 							[sys.executable,self.chrootDir+"/bin/batch_OBLiX.py","-i",
	# 								self.chrootDir+'/testing/integration_tests/'+folder+'/'+file],
	# 							loop, stop=False, streamout=self.output, streamerr=self.output,
	# 							log=self.logger, wordout='og', worderr='EG')
	# 						if retCode[0] == 0 :
	# 							pass
	# 						else:
	# 							print (' [GUI]   ','None zero exit code', file=self.output)
	# 							raise
	# 						print(' [GUI]   ','Run launchTest sequence: end 0', file=self.output)
	# 					except :
	# 						print (' [GUI]   ',
	# 							"EXCEPT in the execution of the launchTest sequence :".format( sys.exc_info()[0] ),
	# 							 file=self.output )
	# 						raise
	# 	print('[Perform_TestCases]', 'End for the launchTest.')
	# 	return


	def launch_test_fill(self, executorObj, compiler, options):
		"""launch_test_fill(self, executorObj, compiler, options)
			Description:
				launchTest is a subroutine that does for each element of the list self.list
			Args:
				x
			Returns:
				x
			Raises:
				x
			Examples:
				x
			Remarks: (refs, notes, ToDo, etc.)
				x
		"""

		self.logger.notice('Start of the launch_test_fill')
		retCode=-1
		if len(self.listTestCases)==0:
			self.logger.warning('No test cases have been selected')

		conf=BatchConfigClass.ConfModel()
		listFoldersToTest=[ folder for folder in os.listdir(self.perfCaseDir) \
				if os.path.isdir( os.path.join(self.perfCaseDir, folder) ) ]
		if '__pycache__' in listFoldersToTest:
			listFoldersToTest.remove('__pycache__')

		executedTests={}
		if self.listTestCases== 'All':
			executedTests=dict.fromkeys(listFoldersToTest, False)
		else :
			executedTests=dict.fromkeys(self.listTestCases, False)
			listFoldersToTest= set(listFoldersToTest).intersection(self.listTestCases)
			#[False]*len(self.listTest)

		for folder in listFoldersToTest:
			# Get the config file in the folder to test, in order to know what to do.
			confFiles=[ file for file in os.listdir(self.perfCaseDir+self.sep+folder) \
					if (os.path.isdir( os.path.join(self.perfCaseDir, folder) ) and self.substring4conf in file) ]
			for files in confFiles:
				# self.argCmd=self.verboseCmd+self.inputCmd+self.perfCaseDir+self.sep+folder+self.sep+files
				try :
					self.logger.notice('launch_test_fill: '+folder+'/'+files+
						' ( '+str(compiler)+', '+str(options)+' )')

					# Edit the option of the executor according to the confFile
					conf.read_from_file( self.perfCaseDir+self.sep+folder+self.sep+files )
					executorObj.edit_from_conf( conf )
					#Run the executor
					retCode1 = executorObj.run_exec()
					executedTests.update({folder:True})
					retCode= max(retCode, retCode1)

					# Check the results
					if retCode1 == 0 :
						self.logger.notice( 'Runned : end 0' )
					else:
						self.logger.warning( f'Test {folder}: None zero exit code !' )
						raise ValueError( f'Test {folder}: None zero exit code !' ) #

					# Fill the database
					retCode1 =self.fill_database(folder, compiler, options)
					retCode= max(retCode, retCode1)
				except Exception as exce :
					retCode = 234
					self.logger.error( f"EXCEPT in the execution of the launch_test_fill sequence :{exce}" )
					raise # not sure that we should re-raise here. let's see.

		# Test that all the wanted tests have been executed.
		for key, value in executedTests.items():
			if not value:
				retCode= 987
				self.logger.error( f"The TestCase {key} have not been found in {self.perfCaseDir} !" )
		# End
		self.logger.notice( f'End for the launch_test_fill ({retCode}).' )
		return retCode
	#end function


	def fill_database(self, testName, compiler, options): # pylint: disable=too-many-locals, too-many-statements
		"""fill_database(testName, compiler, options)
			Description:
				This function will be used to fill the XML Database, given the path do the XML file
				and the values needed to fill it.
			Args:
				testName:
				compiler:
				options:
			Returns:
				retCode:
			Raises:
				if something went wrong in the computation of the error metric norm.
		"""

		# Say hello.
		self.logger.notice('fill_database: Start')
		retCode=-1

		# We get the number of warns and the value of time from the log files
		numWarn, timeValue = self.read_time_from_logfile(self.perfCaseDir+testName+self.logNames)

		# Here we launch the script doing the calculation of the error metric and we get its value
		previousCallingDir= os.getcwd()
		os.chdir(self.perfCaseDir+testName)
		normError = float('nan')
		minNEM = float('nan')
		maxWarn = float('nan')
		maxTime = datetime.timedelta(-1)
		try:
			# get the module name
			fileName= os.path.basename(self.perfCaseDir+testName+self.fileNormalizedError)
			moduleName= os.path.splitext(fileName)[0]
			# import the module
			# sys.path.append(os.getcwd()) # it seems it not necessary.
			normErrorModule =__import__(moduleName)
			# get the precision for this test and the acceptable values.
			normError = normErrorModule.main_calc()
			minNEM, maxWarn, maxTime = normErrorModule.acceptable_value()
			# forget the module
			del sys.modules[moduleName]
			del normErrorModule
			sys.path.remove(os.getcwd())
			os.chdir(previousCallingDir)
		except Exception as exce :
			self.logger.error(exce)
			numWarn = numWarn +1
			os.chdir(previousCallingDir)
			raise # not sure that we should re-raise here. let's see.

		# Check if the results are acceptable.
		if normError < minNEM:
			retCode = 1
			logger.error(f'fill_database: {testName} returned with unacceptable '+
							 f'precision ({normError}<{minNEM}).')
		if numWarn > maxWarn:
			retCode = 1
			logger.error(f'fill_database: {testName} returned with unacceptable '+
							f'Warning/Error number({numWarn}>{maxWarn}).')
		if timeValue > maxTime:
			retCode = 1
			logger.error(f"fill_database: {testName} returned with unacceptable "+
							 f"execution time ({timeValue}>{maxTime}).")

		#
		# If no compiler of options given, then do not fill the database
		if (compiler is None) or (options is None):
			self.logger.warning('fill_database: No compiler, so the filling is skipped!')
			self.logger.notice('fill_database: End')
			return retCode

		# We create the object defined in DBModel used to fill the database properly
		resultObject = DBModel.ResultToStock()
		resultObject.nameTest = str(testName)
		resultObject.resultWarnErr = str(numWarn)
		resultObject.resultTime = str(timeValue)
		resultObject.resultPrecision = str(normError)

		# Compiler option field.
		resultObject.codeVersion = self.read_version()
		resultObject.compilerName = str(compiler)
		resultObject.optionCompilation = str(options)

		# Mac address
		resultObject.macAddress = get_id()
		# Platform type
		if sys.platform.startswith('win'):
			resultObject.machineOs = 'Windows'
		elif sys.platform.startswith('linux'):
			resultObject.machineOs = 'linux'
		else:
			resultObject.machineOs = 'Unknown'

		# Fill the Database !
		DBModel.search_and_change(resultObject, self.pathDB)

		self.logger.notice('fill_database: End')
		return retCode
	#end function

	# Function to get computational time from log file.
	def read_time_from_logfile(self, logFile):
		"""read_time_from_logfile(logFile) function
			Description:
				This function, given a log file path, returns the number of warn in
				the log files and the value of total time of execution if it is
				found. The initial value of timeValue and numWarn is 0.
				In case of error, prints a [WARN].
			Args:
				x
			Returns:
				x
			Raises:
				x
			Examples:
				x
			Remarks: (refs, notes, ToDo, etc.)
				x
		"""

		numWarn = 0
		timeValue = 'NaN'
		try:
			with open(logFile, encoding='utf-8') as file:
				content = file.readlines()
			content = [x.strip().lower() for x in content]
			for i, line in enumerate(content):
				if line.find('WARN'.lower())!=-1 or line.find('ERROR'.lower())!=-1:
					numWarn += 1
					self.logger.notice( line )
					self.logger.notice( f" read_time_from_logfile -- WARN/ERR found "\
						f"in the log {logFile} at line:{i+1}" )
				elif line.find('OBLiX Read & Run: (Start)'.lower())!=-1:
					timeStamp= line[ line.find('[')+1:line.find(']') ]
					npidIndex= timeStamp.find(' pid')
					timeStart= datetime.datetime.strptime(timeStamp[:npidIndex], '%H:%M:%S.%f')
				elif line.find('OBLiX Read & Run: (End)'.lower())!=-1:
					timeStamp= line[ line.find('[')+1:line.find(']') ]
					npidIndex= timeStamp.find(' pid')
					timeEnd= datetime.datetime.strptime(timeStamp[:npidIndex], '%H:%M:%S.%f')

			timeValue= timeEnd -timeStart +datetime.timedelta(microseconds=1)
		except Exception as exc:
			self.logger.error( f'Error in read_time_from_logfile of {logFile}' )
			self.logger.error( f'Error in read_time_from_logfile: {exc}' )
			numWarn= numWarn +1
			raise # not sure that we should re-raise here. let's see.
		return numWarn, timeValue
	# end def


	def read_version(self):
		"""
			Description:
				x
			Args:
				x
			Returns:
				x
			Raises:
				x
			Examples:
				x
			Remarks: (refs, notes, ToDo, etc.)
				x
		"""

		version = 'Unknown'
		previousCallingDir= os.getcwd()
		os.chdir(CHROOT_PATH)
		pathReadMe = os.getcwd()+"/OBLiX-README.md"
		if os.path.exists(pathReadMe):
			try:
				with open(pathReadMe, encoding='utf-8') as file:
					content = file.readlines()
				content = [x.strip().lower() for x in content]
				for _, line in enumerate(content):
					if line.find('Current version'.lower())!=-1:
						indexVersion = line.find(':') + 1
						version = line[indexVersion+1:]
				os.chdir(previousCallingDir)
			except Exception as exce:
				os.chdir(previousCallingDir)
				self.logger.error( f'Error in read_version of {pathReadMe}: {exce}' )
				raise
		else:
			os.chdir(previousCallingDir)
			self.logger.error( f'Error in read_version, the file {pathReadMe} does not exist' )
		return version
	# end def


	# Function Do Test Cases
	def run_testcases(self):
		"""run_testcases(self)
			Description:
				x
			Args:
				x
			Returns:
				x
			Raises:
				x
			Examples:
				x
			Remarks: (refs, notes, ToDo, etc.)
				x
		"""

		# Say hello.
		self.logger.notice('run_testcases sequence: start')

		# init variables
		retCode=-1

		# Create a compiler and executor object.
		compilerObj=CompilerMod.CompilerOptions()
		executorObj=ExecutorMod.ExecutorOptions(os.getpid())

		# try for all the compiler with all their options, all the tests.
		try :
			if len(self.listCompiler)>0:
				if len(self.listOptionCompile)==0:
					self.logger.error('run_testcases: Some compilers given but no '\
						'compiler option, so nothing will be compiled nevers tested.')
					self.logger.error('run_testcases: If you want to run the '\
						'compilation with simplest option put "None", or if you want '\
						'to run tests without compiling put no compiler name.')
				for compilerName in self.listCompiler:
					for optionCompiler in self.listOptionCompile:
						self.logger.notice( f"Launching test with compiler:'{compilerName}'"\
							" with option: '{optionCompiler}'")

						# Setup options then Compile
						compilerObj.compiler=compilerName
						compilerObj.option_from_keyword(optionCompiler, log=self.logger)

						# Run compilation
						retCodeTemp1= compilerObj.run_compilation()
						# Check the results
						# No need yet, because if the compilation crash it raises !

						# Run list of TestCases
						retCodeTemp2= self.launch_test_fill(executorObj, compilerName, optionCompiler)
						retCode=max(retCodeTemp1, retCodeTemp2)
						# self.launch_test_fill(None, compilerName, optionCompiler)

			# If there is compiler selected just run the executor	for the testCases.
			elif len(self.listCompiler)==0:
				# Run list of TestCases
				self.logger.warning ('Since no compiler was given, the database will' \
					' not be filled with new values!' )
				retCode=self.launch_test_fill(executorObj, None, None)
				# self.launch_test_fill(None, None, None)
			#endif

			self.logger.notice('run_testcases sequence: end 0')

		except Exception as exce:
			self.logger.error ( f"EXCEPT in the execution of the run_testcases sequence :{exce}" )
			retCode=654
			raise

		return retCode
	# end def
#end class TestCases

########### MAIN ###########
if __name__ == "__main__":
	# Import of Libraries-------------------------------------------------------

	# Initialization of primary variables---------------------------------------

	# Read the options----------------------------------------------------------
	# pylint: disable=invalid-name
	confInputPath=''
	confGiven=False
	compilerGiven=False
	testcaseGiven=False
	optionCompilerGiven=False
	listCompileFromInput=[]
	listTestsFromInput=[]
	listOptionsFromInput=[]
	argvNum= len(sys.argv[:])
	if argvNum >1 :
		for j in range(1, argvNum):
			if sys.argv[j]=='-i' : # or sys.argv[2]=='-p'
				if j+1< argvNum:
					confGiven=True
					confInputPath=sys.argv[j+1]
				else:
					logger.error("the option -i must be followed by a path to a conf file.")
			elif sys.argv[j]=='-c' :
				if j+1< argvNum:
					compilerGiven=True
					listCompileFromInput=sys.argv[j+1].split(',')
				else:
					logger.error('the option -c must be followed by list (comma '\
						'separated, no space) of compiler name.')
			elif sys.argv[j]=='-l' :
				if j+1< argvNum:
					testcaseGiven=True
					listTestsFromInput=sys.argv[j+1].split(',')
				else:
					logger.error('the option -l must be followed by a list (comma '\
						'separated, no space) of testCases.')
			elif sys.argv[j]=='-o' :
				if j+1< argvNum:
					optionCompilerGiven=True
					listOptionsFromInput=sys.argv[j+1].split(',')
				else:
					logger.error('the option -o must be followed by a list (comma '\
						'separated, no space) of compiler options.')
	# pylint: enable=invalid-name

	# Initialization of files, logs and redirections ---------------------------
	configObj= BatchConfigClass.ConfModel()
	if confGiven :
		configObj.read_from_file(confInputPath)

	logfilename=os.path.basename(__file__).rsplit('.py',1)[0]
	logger= MyUtils.init_logs(configObj.loglevel, logfilename)

	############
	### MAIN ###
	############
	logger.notice( f"Main {SCRIPT_NAME} sequence: Start. \n" )

	# Create a testcase object from an empty list
	testcasesObj=TestCases('All', logger, sys.stdout)
	testcasesObj.edit_from_conf(configObj)

	# edit options from the input arguments.
	if compilerGiven:
		testcasesObj.listCompiler= listCompileFromInput
	if testcaseGiven:
		testcasesObj.listTestCases= listTestsFromInput
	if optionCompilerGiven:
		testcasesObj.listOptionCompile= listCompileFromInput

	# Run the testcase
	retCodeMain= testcasesObj.run_testcases()

	# check the right execution of run_testcases
	if retCodeMain == 0:
		logger.notice( f"End of {SCRIPT_NAME} sequence: 0 **\n" )
	else:
		logger.error( f"End of {SCRIPT_NAME} sequence "+f"with Exit Code {retCodeMain} **\n" )

	sys.exit(retCodeMain)


######################## 	TO DO     	########################
# '''
# 	Stuff to do:
# '''

######################## 	BUGS KNOWN     	########################
# '''
# 	Known bugs:
# '''

######################## 	AIDE MEMOIRE 	########################
# """
# 	Aide memoire:
# """

# EOF
