#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# pylint: disable=invalid-name
""" CalcNormErrMetrics

	Returns:
		[type]: [description]
"""
# pylint: enable=invalid-name

# Import of Libraries-----------------------------------------------------------
import os
# import sys
import logging
import inspect

# import math
import datetime


# Initialization of primary variables-------------------------------------------
#
AUTHORS_NAME=["Romain NOËL"]
VERSION_NUM=1.01
VERSION_DATE="29-December-2021"
PROJECT_NAME="OBLiX"
SCRIPT_NAME="CalcNormErrMetrics_C1x"
SCRIPT_DESCRIPTION="This script to compute the normalize error metric."


(frame, filename_frame, line_number, function_name, lines_frame, index_frame) = \
	inspect.getouterframes( inspect.currentframe() )[-1]
# print(frame, filename_frame, line_number, function_name, lines_frame, index_frame)
# print('caller is :', FILENAME_CALLER, 'and __name__ is :', __name__)
if filename_frame.startswith('<string>') or filename_frame.startswith('<stdin>') :
	FILENAME_CALLER='local'
else:
	# print('filename_frame='+filename_frame, flush=True)
	try:
		FILENAME_CALLER= filename_frame.rsplit('/',1)[-1].rsplit('.py',1)[0]
	except IndexError as e:
		FILENAME_CALLER= filename_frame.rsplit('\\',1)[-1].rsplit('.py',1)[0]
del frame, filename_frame, line_number, function_name, lines_frame, index_frame

logger = logging.getLogger(FILENAME_CALLER+'.'+SCRIPT_NAME)


################################ 	FUNCTIONS 	################################
def compare_logs(file1, file2):
	""" compare_logs(File1, File2):
		Description:
			x
		Args:
			file1 (string path): is path to the
			file2 (string path):
		Returns:
			x
		Raises:
			x
		Examples:
			x
		Remarks: (refs, notes, ToDo, etc.)
			x
	"""

	retCode= 0
	maxline= 0
	minline= 0
	numLineDiff= 0

	with open(file1, encoding='utf-8') as fileGt, open(file2, encoding='utf-8') as fileR:
		linesGt = fileGt.readlines()
		linesR  = fileR.readlines()
		maxline= max(len(linesGt), len(linesR))
		minline= min(len(linesGt), len(linesR))
		for linGt, linR in zip(linesGt,linesR):
			messageGT=linGt.partition("]")[2]
			messageR =linR.partition("]")[2]
			if messageGT!=messageR:
				numLineDiff= numLineDiff +1

	if maxline != minline:
		retCode= 0.5
	else:
		retCode= 1. - numLineDiff/maxline

	return retCode
# end function


def main_calc():
	"""
		Description:
			x
		Args:
			x
		Returns:
			x
		Raises:
			x
		Examples:
			x
		Remarks: (refs, notes, ToDo, etc.)
			x
	"""

	path2ThisFile = os.path.dirname(os.path.abspath(__file__))
	normEM = 0.

	pathFileGT = path2ThisFile +'/Results/OBLiX.log'
	pathFileR = path2ThisFile +'/Ground_Truth/OBLiX.txt'

	if os.path.exists(pathFileGT) and os.path.exists(pathFileR):
		normEM = compare_logs(pathFileGT, pathFileR)
	else:
		logger.error('One or the two files you are looking for do not exist')
		logger.error( f" pathFileGT: {pathFileGT} \n"\
			+f" pathFileR: {pathFileR}" )

	return normEM
# end function


def acceptable_value():
	""" acceptable_value()

		Description:
			This function gives access to acceptable parameters for the executed code.
			Parameters such as the precision, number of warning or time of execution.
		Returns:
			MinNEM: minimal acceptable precision.
			MaxWarn: maximal acceptable number of warning or error.
			MaxTime: maximal acceptable execution time.
	"""

	minNEM = 1.0
	maxWarn = 2
	maxTime = datetime.timedelta(seconds=2.0)

	return minNEM, maxWarn, maxTime
# end function


################################ 	MAIN  	###################################
if __name__ == "__main__":

	main_calc()

# EoF
