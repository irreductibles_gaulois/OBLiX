#!/usr/bin/python3
# -*- coding: utf-8 -*-

# pylint: disable=invalid-name
"""
   Batch configuration file for RCR1j test case.
"""
# pylint: enable=invalid-name

import os
# Find chRootProject path.
# pylint: disable=no-else-break
_PREV_PATH = None
_CALLING_DIR = os.getcwd()
while True :
	if (os.path.exists('./OBLiX-README.md')) and (os.getcwd()!='/'):
		CHROOT_PATH=os.getcwd()
		break
	elif _PREV_PATH==os.getcwd() :
		print('ERROR: the chRootProject folder was not found...')
		os.chdir(_CALLING_DIR)
		# sys.exit(3)
		break
	else:
		_PREV_PATH=os.getcwd()
		os.chdir('../')
os.chdir(_CALLING_DIR) # Go back to the call directory.
del _PREV_PATH
del _CALLING_DIR
# pylint: enable=no-else-break


# pylint: disable=invalid-name
# General
loglevel = 5
# Compiler
# Executor
flagExecute = True
inputFile = 'input_RCRCj.json'
inputFileDir = CHROOT_PATH+'/testing/integration_tests/RCRCj/'
outputDir = CHROOT_PATH+'/testing/integration_tests/RCRCj/Results/'
# Plotter
# Performance Test-Cases
# pylint: enable=invalid-name

# EOF
