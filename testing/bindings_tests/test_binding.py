#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# pylint: disable=invalid-name
"""
	LICENSE :
	Copyright (C) 2021-20** Romain NOEL

	AUTHOR(S) :
		Romain NOËL
			Mail	: romain.noel@univ-eiffel.fr
"""
# pylint: enable=invalid-name

# Import of Libraries-----------------------------------------------------------
# pylint: disable=unused-import
import os 					# library for Operating System
import sys					# library for system manipulation; sys is a submodule of os
import logging				# lib to manipulate loggers
import inspect				# lib to have access to the caller of this module

import pytest					# lib for framework of testcases
# pylint: enable=unused-import

# Find chRootProject path.
# pylint: disable=no-else-break
_PREV_PATH = None
_CALLING_DIR = os.getcwd()
while True :
	if (os.path.exists('./OBLiX-README.md')) and (os.getcwd()!='/'):
		CHROOT_PATH=os.getcwd()
		break
	elif _PREV_PATH==os.getcwd() :
		print('ERROR: the chRootProject folder was not found...')
		os.chdir(_CALLING_DIR)
		sys.exit(3)
		break
	else:
		_PREV_PATH=os.getcwd()
		os.chdir('../')
os.chdir(_CALLING_DIR) # Go back to the call directory.
del _PREV_PATH
# del _callingDir
# pylint: enable=no-else-break

# Import Project Libraries
# sys.path.append(CHROOT_PATH+'/bindings')
# sys.path.append(os.getcwd())
sys.path.append(CHROOT_PATH+'/build/dist/lib/OBLiX/')
OBLiXlib= __import__('py_OBLiX')


################################ 	FUNCTIONS 	########################################

# @pytest.fixture(autouse=True)
# def no_logs_gte_error(caplog):
# 	""" no_logs_gte_error(caplog)

# 		Description:
# 			Function with a pytest fixture to be runned automatically.
# 			The function check that no errors are present in logs captured by caplog.
# 		Args:
# 			caplog : a pytest fixture that capture logs.
# 	"""
# 	yield
# 	errors = [record for record in caplog.get_records('call') if record.levelno >= logging.ERROR]
# 	assert not errors


def no_logs_gte_num(caplog, logLevel):
	""" no_logs_gte_num(caplog, logLevel)

		Description:
			The function check that logs captured by caplog contain no message
			higher than a given logLevel number.
		Args:
			caplog : a pytest fixture that capture logs.
			logLevel (int): the maximum logLevel acceptable.
	"""
	errors = [record for record in caplog.get_records('call') if record.levelno >= logLevel]
	assert not errors
# end function

# pylint: disable=no-self-use
class TestBinding() : 		# TestSum(unittest.TestCase):
	""" TestBinding

		Description:
			X
		Methods:
			X
	"""

	def test_exec_from_file(self): #, logs , capsys
		""" def test_exec_from_file(self):

			Description:
				X
		"""

		assert OBLiXlib.get_factory().print_builders() == 0
		OBLiXlib.cvar.oblixInterns.consoleLevel=5
		OBLiXlib.setup_logger()
		OBLiXlib.redef_map_factory()
		res = OBLiXlib.exec_from_file( CHROOT_PATH+'/testing/integration_tests/RCRCj/input_RCRCj.json' )
		# out, err = capsys.readouterr()
		# assert out == "bla bla not working"
		assert res == 0
	# end function


	def test_create_new_derived_class(self):
		""" def test_create_new_derived_class(self):

			Description:
				X
		"""

		class TriangleClass( OBLiXlib.BaseX ):
			"""_summary_

			Args:
				 OBLiXlib (_type_): _description_
			"""

			def __init__(self):
				""" __init__(self, pathConf=None, pid)

				Description:
					constructor function.
				Args:
					pid (os.pid):
						process id required by the executor
					conf (BatchConfigClass.ConfModel, optional):
						configuration object to initialize the BatchClass object.
						Defaults to None.
					pathConf (string, optional):
						A string containing a path to a configuration file.
						The configuration file have to be from class BatchConfigClass.ConfModel
						Defaults to None.
				"""

				# Call parent constructor.
				# BatchConfigClass.ConfModel.__init__(self)
				super().__init__()

				self.hight = 2.
				self.base = 3.

				# return
			# end function

			def get_classname(self):
				"""_summary_

				Returns:
					 _type_: _description_
				"""
				return "Triangle"

			# def init(self):
			# 	"""_summary_
			# 	"""
			# 	self.mapOut.new_with_type<double>("area")
			# # end function

			def run(self):
				"""_summary_
				"""
				# log=OBLiXlib.get_pLog()
				print("in run triangle.")
				# self.mapOut.get("area")= self.hight * self.base / 2.
			# end function
		# end class

		# OBLiXlib.BaseRunFactory().register_builder("Triangle", OBLiXlib.unique_creator(TriangleClass))

		titi=TriangleClass()
		titi.run()

		assert titi.hight == 2.
		pytest.skip("Skip because Test is not ready yet", allow_module_level=True)
	# end function

# end class
# pylint: enable=no-self-use


################################ 	MAIN SCRIPT 	########################################
if __name__ == '__main__':
	print('main execute_test')

	# unittest.main()


######################## 	TO DO         ########################
# '''
# toto
# '''

######################## 	BUGS KNOWN     ########################
# '''
# toto
# '''

######################## 	AIDE MEMOIRE   ########################
# '''
# toto
# '''

# EOF
