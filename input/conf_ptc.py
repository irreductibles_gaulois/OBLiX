#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
   Default batch configuration file.
"""

# pylint: disable=invalid-name
## General
loglevel = 6
# binExec = 'OBLiXprog'
# binDir = CHROOT_PATH+'/bin/'
## Compiler
flagCompile = False
cMakeFlag = True
## Executor
flagExecute = False
## Plotter
flagPlot = False
# PerfTC
flagPTC = False
listCompiler = ['gnu', 'clang']  #['gnu', 'intel', 'clang', 'pgi']
listOptionCompile = ['warnall', 'None']
#['warnall', 'fast', 'static', 'faststatic', 'openACC', 'MPI', 'None']
listTestCases = 'All'
# pylint: enable=invalid-name

# EOF
