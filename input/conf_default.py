#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
   Default batch configuration file.
"""

# Get chRootProject path.
import os
import sys
# Find chRootProject path.
# pylint: disable=no-else-break
_PREV_PATH = None
_CALLING_DIR = os.getcwd()
while True :
	if (os.path.exists('./OBLiX-README.md')) and (os.getcwd()!='/'):
		CHROOT_PATH=os.getcwd()
		break
	elif _PREV_PATH==os.getcwd() :
		print('ERROR: the chRootProject folder was not found...')
		os.chdir(_CALLING_DIR)
		sys.exit(3)
		break
	else:
		_PREV_PATH=os.getcwd()
		os.chdir('../')
os.chdir(_CALLING_DIR) # Go back to the call directory.
del _PREV_PATH
# del _callingDir
# pylint: enable=no-else-break

# pylint: disable=invalid-name
## General
loglevel = 6
binExec = 'OBLiXprog'
binDir = CHROOT_PATH+'/bin/'
## Compiler
flagCompile = False
compiler = 'gnu'
fast = False
warnall = False
static = False
profile = False
#binExec = ''
#binDir = ''
buildDir = CHROOT_PATH+'/build/'
progMain = 'main.cpp'
listObjectFile = []
remove = False
cMakeFlag = True
## Executor
flagExecute = False
#binExec = ''
#binDir = ''
binExecOpt = '' # '-s '
binExecVersion = False
otherBinTools = []
otherToolsDir = ['']*len(otherBinTools)
inputFile = 'inputCommand_OBLiX.xml'
inputFileDir = CHROOT_PATH+'/input/'
secondaryInputFiles = []
secondaryInputDir = ['']*len(secondaryInputFiles)
outputDir = CHROOT_PATH+'/output/'
tmpDir = CHROOT_PATH+'/tmp/' #'./tmp'
tmpDirEnabled = False
## Plotter
flagPlot = False
plotter = 'ParaView'
savePicture = None
saveMovie = None
movieFramerate = None
# PerfTC
flagPTC = False
listCompiler = ['gnu', 'clang']  #['gnu', 'intel', 'clang', 'pgi']
listOptionCompile = ['warnall', 'fast', 'static', 'None']
#['warnall', 'fast', 'static', 'faststatic', 'openACC', 'MPI', 'None']
listTestCases = 'All'
# pylint: enable=invalid-name

# EOF
