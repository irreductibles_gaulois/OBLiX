#!/bin/bash

# ==============================================================================
# NAME: install_unix.sh
#
# DESCRIPTION:
# 		The universal setup of your project for unix (debian, redhat, alpine, ...)
# 
# USAGE:
# 		- for interactive mode, you should give no argument:
# 			$ ./install_unix.sh
# 		- c for conda installation:
# 			$ ./install_unix.sh -c
# 		- p for pip installation:
# 			$ ./install_unix.sh -p
# 		- l to define the list of packages to install:
# 			$ ./install_unix.sh -l "gcc cmake"
# 		- y to confirm the installation of packages :
# 			$ ./install_unix.sh -y -l "gcc cmake"
# ==============================================================================


# Global variables
ProjectName="OBLiX"
ScriptName="install_"$ProjectName"_unix"
ScriptName="addPackages"
ScriptAuthors="Romain NOEL"
ScriptVersion="0.0.1"
ScriptDate="03 Nov 2021"

AddPythonEnvScript="./addPythonEnv.sh"
AddPythonLibScript="./libsPy2install.py"
AddPackagesScript="./addPackages.sh"
FileConfInstall="./config_install.py"
LIST_OF_PACKAGES="bash wget git cmake make"

opt_libPy2install=""
opt_addPack=""

# Create a log file
LogFile=./install_$ProjectName
LogExt=.log
# If the log file already exist then rename it
if [ -e $LogFile$LogExt ] ; then
	# `ls file* | wc -l` is the number of log file present
	mv $LogFile$LogExt $LogFile-`ls $LogFile*.log | wc -l`$LogExt
fi
# Be nice and say hello.
echo "Installation of $ProjectName unix: Start." | tee $LogFile$LogExt
# Tee: redirect stdout and stderr in both terminal and log file
exec 1> >(tee -a $LogFile$LogExt) 
exec 2> >(tee -a $LogFile$LogExt >&2)

# The usage function.
usage()
{
	# Creation of a sub-shell to isolate.
	(
		# Print special messages.
		test -n "$*" && echo "`basename $0`: $* : on attent dans cette option"
		
		# Print the help message.
		echo "Usage: $> ./`basename $0` [-y] [-l <package_list>] \n"
		echo "\t -y \t yes : accept to install the packages without asking confirmation."
		echo "\t -l <package_list>\t list : specify a list of package to install."
	)
}

# Special case of distros.
distro=`awk '/^ID=/' /etc/*-release | awk -F'=' '{ print tolower($2) }'`
if [ "$distro" == "alpine" ]; then
	LIST_OF_PACKAGES="$LIST_OF_PACKAGES libstdc++ build-base glib glibc"
fi

# Get options.
while getopts cfl:py opt
do
   case $opt in
		c) # conda
			opt_conda="true" 
			opt_lib2install="-c"
			opt_addPack="-y"
         ;;
		f) # file config
			opt_file="true"
			;;
		l) # list
			opt_list="true"
			val_list="$val_list $OPTARG" 
         ;;
		p) # pip
			opt_pip="true" 
			opt_lib2install="-p"
			opt_addPack="-y"
         ;;
		y) # yes
			opt_confirm="true"
			opt_addPack="-y"
         ;;
		:) # when argument missing.
			echo "The option $opt requires an argument !" >&2
			exit 1 
			;;
		\?) # all the rest.
			echo "\033[41m[Error].\033[0m Option $opt not valid\n" >&2
			usage
			exit 1	
			;;
	esac
done

# function
set_conda(){
	# Load the default environment
	echo 'Try to load Conda base.'
	. ~/.bash_profile; ec1=$?
	if [ $ec1 == 0 ] ; then
		echo '~/.bash_profile found'
	else 	
		source ~/.bashrc ; ec2=$?
		if [ $ec2 == 0 ] ; then
			echo "~/.bashrc found"
		else
			# eval "$(conda shell.bash hook)" # Seems to work also but I ahve to check with new install of conda
			source ~/*conda3/etc/profile.d/conda.sh ; ec3=$?
			if [ $ec3 == 0 ] ; then
				echo "~/*conda3/etc/profile.d/conda.sh found"
			else
				echo "[WARNing] No bashrc nor condarc nor profil found, it is going to be hard to find conda!"
			fi
		fi
	fi
	# Test if Conda is correctly set.
	conda info; ec1=$?
	if [ $ec1 == 0 ] ; then
		echo 'Conda is correctly set'
	else 	
		echo '[ERROR] : the command "conda info" return non-zero code => Conda is NOT correctly set: byebye'
		exit 1
	fi
}

########### MAIN ###########

# Change the list of packages to install if the -l option is given.
if [ $opt_list ]; then
   LIST_OF_PACKAGES="$LIST_OF_PACKAGES $val_list"
fi
opt_addPack=$opt_addPack' -l "'$LIST_OF_PACKAGES'"'
if [ $opt_file ]; then
   opt_lib2install=$opt_lib2install' -f'
fi 

#
echo "Execute the shell script to install all OS depedencies: $AddPackagesScript $opt_addPack"
eval "$AddPackagesScript $opt_addPack"

# Execute the python script to install all python modules.
echo "Execute the shell script to install Python Environment: $AddPythonEnvScript $opt_lib2install"
eval "$AddPythonEnvScript $opt_lib2install"

# Set conda env outside of the previous script if needed.
source "$FileConfInstall"
if [ "$PyEnvManger" == "conda" ]; then
	echo "source conda env!"
	set_conda
	echo "conda activate $ProjectName"
	conda activate $ProjectName
	conda info --envs
fi

# Check Python command.
CommandPython="python3"
PythonExist=`which python3`
if [ ! $PythonExist ]; then
  CommandPython="python"
fi

echo 'The python command detected is: "'$CommandPython'"'" with version `$CommandPython --version`"
echo "Execute the python script to install all depedencies: $CommandPython $AddPythonLibScript $opt_lib2install"
eval "$CommandPython $AddPythonLibScript $opt_lib2install"
if [ "$PyEnvManger" == "conda" ]; then
	echo "Visualize the final settup: conda info --envs ; conda list"
	conda info --envs
	conda list
fi


# Be nice and say goodbye.
echo "Installation of $ProjectName unix: End" #| tee -a $LogFile$LogExt
exit 0
#EoF
