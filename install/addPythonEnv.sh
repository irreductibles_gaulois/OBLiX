#!/bin/bash

# ==============================================================================
# NAME: addPythonEnv.sh
#
# DESCRIPTION:
#     A script to install a Python environment.
# 
# USAGE:
# 		- for interactive mode, you should give no argument:
# 			$ ./addPythonEnv.sh
# 		- c for conda installation:
# 			$ ./addPythonEnv.sh -c
# 		- p for pip installation:
# 			$ ./addPythonEnv.sh -p
# ==============================================================================


# Global variables
ProjectName="OBLiX"
ScriptName="addPythonEnv"
ScriptAuthors="Romain NOEL"
ScriptVersion="0.0.1"
ScriptDate="03 Nov 2021"

# AnacondaLinkPrefix=`wget -O - https://www.anaconda.com/distribution/ 2>/dev/null | sed -ne 's@.*\(https:\/\/repo\.anaconda\.com\/archive\/Anaconda3-.*-Linux-x86_64\.sh\)\">64-Bit (x86) Installer.*@\1@p'`
# AnacondaLinkPrefix=https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-
AnacondaLinkPrefix=https://repo.anaconda.com/miniconda/Miniconda3-py39_4.12.0-Linux-

# Check for Package Manager.
YUM_CMD=$(which yum)
APT_GET_CMD=$(which apt-get)
APK_CMD=$(which apk)
if [ ! -z $YUM_CMD ]; then
	PackageCommand="yum install"
	echo "$ScriptName: $PackageCommand package manager found."
elif [ ! -z $APT_GET_CMD ]; then
	PackageCommand="apt-get install"
	echo "$ScriptName: $PackageCommand package manager found."
elif [ ! -z $APK_CMD ]; then
	PackageCommand="apk add"
	echo "$ScriptName: $PackageCommand package manager found."
else
	echo "ERROR: can't find the Package Manager installed on your distribution."
	exit 1;
fi


# Function to install python from pip
add_python_pip(){
	# Install Pyhton
		
	# Check if Python 3 is already install
	if [ -x "$(command -v python)" ]; then
		echo 'python is already installed!'
	else
		sudo $PackageCommand python3
	fi
	# Check if PIP for Python 3 is already install
	if [ -x "$(command -v python)" ]; then
		echo 'PIP is already installed!'
	else
		sudo $PackageCommand install python3-pip
	fi
	# Update PIP
	echo 'Update PIP.'
	python3 -m pip install --user --upgrade pip==9.0.3
}

# Download Miniconda
dl_conda(){
	# Download Anaconda but first detect the latest executable available.
	MACHINE_TYPE=`uname -m`
	if [ ${MACHINE_TYPE} == 'x86_64' ]; then
		# 64-bit stuff here
		AnacondaLink=${AnacondaLinkPrefix}x86_64.sh
	elif [ ${MACHINE_TYPE} == 'x86' ]; then
		# 32-bit stuff here
		AnacondaLink=${AnacondaLinkPrefix}x86.sh
	else
		echo "$ScriptName: PROCESSOR_ARCHITECTURE is not recognized by the script, so it quits."
		exit 2
	fi
	echo "wget -O ./miniconda_linux.sh $AnacondaLink"
	wget -O ./miniconda_linux.sh $AnacondaLink
	# Check that the download went well
	echo "I should do verification: md5sum $AnacondaLink"
	# echo "${SHA256SUM}  miniconda_linux.sh" > miniconda.sha256
	# if ! sha256sum -cs miniconda.sha256; then exit 1; fi  
	md5sum ./miniconda_linux.sh
	echo "chmod +x ./miniconda_linux.sh"
	chmod +x ./miniconda_linux.sh
	# Install Anaconda
	echo "bash ./miniconda_linux.sh -b"
	bash ./miniconda_linux.sh -b
	eval "$($HOME/miniconda3/bin/conda shell.bash hook)"
	echo "conda init"
	conda init
	# Remove the unnecessary file
	rm ./miniconda_linux.sh
}

# function
set_conda(){
	# Load the default environment
	echo 'Try to load Conda base.'
	. ~/.bash_profile; ec1=$?
	if [ $ec1 == 0 ] ; then
		echo '~/.bash_profile found'
	else 	
		source ~/.bashrc ; ec2=$?
		if [ $ec2 == 0 ] ; then
			echo "~/.bashrc found"
		else
			# eval "$(conda shell.bash hook)" # Seems to work also but I ahve to check with new install of conda
			source ~/*conda3/etc/profile.d/conda.sh ; ec3=$?
			if [ $ec3 == 0 ] ; then
				echo "~/*conda3/etc/profile.d/conda.sh found"
			else
				echo "[WARNing] No bashrc nor condarc nor profil found, it is going to be hard to find conda!"
			fi
		fi
	fi
	# Test if Conda is correctly set.
	conda info; ec1=$?
	if [ $ec1 == 0 ] ; then
		echo 'Conda is correctly set'
	else 	
		echo '[ERROR] : the command "conda info" return non-zero code => Conda is NOT correctly set: byebye'
		exit 1
	fi
	# Update Conda
	echo 'Conda update!'
	conda update --yes conda
}


# function
create_conda_profil(){
	# Create the $ProjectName environment
	echo "Conda create $ProjectName."
	# python 3.7 has to be removed when vtk will be available for newer python dist.
	conda create --name "$ProjectName" -y python=3.9
	#conda create --name $ProjectName -y
	conda init
	# Load the $ProjectName environment
	conda activate $ProjectName; ec1=$?
	if [ $ec1 == 0 ] ; then
		echo "conda activate $ProjectName"
	else 	
		echo '[ERROR] : the command "'conda activate $ProjectName'" Failed, exit'
		exit 1
	fi
	echo 'install conda.API for python'
	conda install -y conda
	# echo 'add conda-forge'
	# conda config --add channels conda-forge
}


########### MAIN ###########


if [ $# -eq 0 ] ; then # no argument is given
	# Ask if the Python Libraries should be installed through a Conda environment or Python distribution
	read -p "Do you want to install Pyhton libraries through a Conda environment (recommanded) or in the Python distribution of the system with PIP or exit ? (Y/P/n) "  optionA
	echo "Answer recieved $optionA"

	if [ $optionA == "Y" ] || [ $optionA == "y" ]; then
	# Install Anaconda
		# Check if Anaconda is already install
		if [ -x "$(command -v conda)" ]; then
			echo 'Anaconda is already installed!'
		else
			# Ask if I should install Conda
			read -p "Conda command is not found, do you want to install it (recommanded) or do you prefer to exit ? (Y/n) "  optionB
			echo "Answer recieved $optionB"

			if [ $optionB == "Y" ] || [ $optionB == "y" ]; then
				dl_conda				

			elif [ $optionB == "n" ]; then
				echo "Installation of $ProjectName linux: I understand that you want to quit."
				exit 0
			else
				echo " Answer '$optionB' not valid"
				echo "Installation of $ProjectName linux: End"
				exit 1
			fi
		fi

		set_conda

		create_conda_profil


	elif [ $optionA == "P" ]; then
	
		# Use function defined earlier to install python.
		add_python_pip

	elif [ $optionA == "n" ]; then
		echo "$ScriptName: I understand that you want to quit."
		exit 0

	else
		echo " Answer '$optionA' not valid"
		echo "$ScriptName: End"
		exit 1
	fi

elif [ "$1" == "-c" ]; then # if the first argument is -c
	# Download Conda.
	dl_conda

	# Verify that Conda is set correctly.
	set_conda

	# Create a new Conda profil for the projet and activate it.
	create_conda_profil

elif [ "$1" == "-p" ]; then # if the first argument is -p
	# Use function defined earlier to install python.
	add_python_pip


else # no valid option are given.
	echo "$ScriptName.sh ERROR: the argument '$1' is unknown."
	echo "Please enter $ScriptName.sh without any argument to execute in interactive mode." 
	echo "\t -c to install and configure using (Ana|Mini)conda python environment (Recommanded)."
	echo "\t -p to install and configure using PIP python environment directly in your OS."
	exit 1
fi 
