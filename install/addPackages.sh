#!/bin/sh

# ==============================================================================
# NAME: addPackages.sh
#
# DESCRIPTION:
#     A script to install a list of packages from the package manager found on 
#     the OS.
# 
# USAGE:
# 		- l to define the list of packages to install:
# 			$ ./addPackages.sh -l "gcc cmake"
# 		- y to confirm the installation of packages :
# 			$ ./addPackages.sh -y -l "gcc cmake"
# ==============================================================================


# Global variables
LIST_OF_PACKAGES="wget"

ScriptName="addPackages"
ScriptAuthors="Romain NOEL"
ScriptVersion="0.0.1"
ScriptDate="03 Nov 2021"

# Be nice and say hello.
echo "\n	** `basename $0`: Start. ** \n"

# Test of pre-required.
# if [[ $UID = 0 ]]
# 	then 
# 		echo -e "$ScriptName: \033[41m[ERROR].\033[0m\n You should have super-user priviledge to run this script.\n" >&2
# 		exit 1
# fi

# The usage function.
usage()
{
	# Creation of a sub-shell to isolate.
	(
		# Print special messages.
		test -n "$*" && echo "`basename $0`: $* : on attent dans cette option"
		
		# Print the help message.
		echo "Usage: $> ./`basename $0` [-y] [-l <package_list>] \n"
		echo "\t -y \t yes : accept to install the packages without asking confirmation."
		echo "\t -l <package_list>\t list : specify a list of package to install."
	)
}

# Get options.
while getopts yl: opt
do
   case $opt in
		y) #
			opt_y="true" 
         ;;
		l) # 
			opt_l="true"
			val_l="$val_l $OPTARG" 
         ;;
		:) # when argument missing.
			echo "The option $opt requires an argument !" >&2
			exit 1 
         ;;
		\?) # all the rest.
			echo "\033[41m[Error].\033[0m Option $opt not valid\n" >&2
			usage
			exit 1	
         ;;
	esac
done

########### MAIN ###########

# List the Package Manager possible.
YUM_CMD=$(which yum) #redhat RPM
APT_GET_CMD=$(which apt-get) #debian DPKG
APK_CMD=$(which apk) #alpine
PACMAN_CMD=$(which pacman) #arch
WINGET_CMD=$(which winget) #windows
PKG_CMD=$(which pkg) #freeBSD
BREW_CMD=$(which brew) #macOS

# Test which of the possible Package Manager in the previous list is available.
if [ ! -z $YUM_CMD ]; then
   PackageCommand="yum"
   PackageCommandUpdate="yum update"
	PackageCommandInstall="yum"
   echo "$ScriptName: $PackageCommand package manager found."
elif [ ! -z $APT_GET_CMD ]; then
   PackageCommand="apt-get"
   PackageCommandUpdate="$PackageCommand update"
	PackageCommandInstall="$PackageCommand install"
   if [ $opt_y ]; then
      PackageCommandInstall="$PackageCommandInstall -y"
   fi
   echo "$ScriptName: $PackageCommand package manager found."
elif [ ! -z $APK_CMD ]; then
   PackageCommand="apk"
   PackageCommandUpdate="$PackageCommand update"
	PackageCommandInstall="$PackageCommand add"
   # if [ $opt_y ]; then
   #    PackageCommandInstall="$PackageCommandInstall -y"
   # fi
   echo "$ScriptName: $PackageCommand package manager found."
else
	echo "ERROR: can't find the Package Manager installed on your distribution."
	exit 1;
fi

# Change the list of packages to install if the -l option is given.
if [ $opt_l ]; then
   LIST_OF_PACKAGES="$val_l"
fi

# Execute the 
echo "List of packages to install: $LIST_OF_PACKAGES. \n"
echo "$PackageCommandUpdate \n"
$PackageCommandUpdate || sudo $PackageCommandUpdate
echo "$PackageCommandInstall $LIST_OF_PACKAGES \n"
$PackageCommandInstall $LIST_OF_PACKAGES || sudo $PackageCommandInstall $LIST_OF_PACKAGES


# Be nice and say goodbye.
echo "\n	** `basename $0`: exit normally. **\n"
exit 0
#EoF