#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# pylint: disable=invalid-name

"""
	NAME: libs2intall.py

	DESCRIPTION:
			A python script to install from conda or pip a list of modules to your
			python environment

	LICENSE:
  		Copyright (C) 2021-20** Romain NOEL

  	AUTHOR(S):
		Romain NOËL
			Mail: romain.noel@univ-eiffel.fr
 			Address: Allee des Ponts et Chaussees, 44340 Bouguenais, FRANCE
"""
# pylint: enable=invalid-name


# Import of Libraries-----------------------------------------------------------
import sys                   # Library for System-specific parameters and functions
import os                    # Library for Operating System

# Initialization of primary variables-------------------------------------------
AUTHORS_NAME="Romain NOËL"
VERSION_NUM='0.0.4'
VERSION_DATE='OBLiX'
PROJECT_NAME='01-Nov-2021'
SCRIPT_NAME='libsPy2install'
SCRIPT_DESCRIPTION='...'

# pylint: disable=invalid-name
pathInit = os.getcwd()
path2install = os.getcwd()
logFile = './install_'+PROJECT_NAME+'.log'
# To deal with version name, it is used the regex sourceCompressed * sourceCompressedExt
sourceCompressed='DirFile2Install'
sourceCompressedExt='.zip'
listLibFile='libsInfo.py'

listFile2MakeExecutable=[PROJECT_NAME+'_GUI_unix.desktop', PROJECT_NAME+'_GUI_win.lnk', \
	'./bin/batch_'+PROJECT_NAME+'.py', './bin/read_py_image.py']
# pylint: enable=invalid-name

# Initialization Arguments given or not ----------------------------------------
#print(sys.argv)
# pylint: disable=invalid-name
flagFile=False
flagConda=False
flagPip=False
for i in range(1,len(sys.argv)):
	if sys.argv[i]=='-p':
		flagPip=True
		try:
			from pip import main as pipmain
		except ImportError:
			from pip._internal import main as pipmain
	elif sys.argv[i]=='-c':
		flagConda=True
		import conda.cli
	elif sys.argv[i]=='-f':
		flagFile=True
	else :
		print(f"The positional argument received by libs2install is {sys.argv[1]} "\
			"which is not accepted." )
		print('The list of positional argument accepted is [-p, -c, -f]')
		sys.exit(2)
# pylint: enable=invalid-name


# Initialization of primary functions-------------------------------------------
class MyException( Exception ):
	"""This is my exception class for all kind of exception."""

def try2install(libIn):
	""" try2install(libIn)

		Description:
			Try to install a module or somthing else according to the option given to
			the script ie with conda or pip.
			In both case if the internal python API fails, the subprocess module is
			used to execute the command outside of the python script.
		Args:
			libIn ([type]): [description]
	"""

	if sys.argv[1]=='-p' or sys.argv[2]=='-p':
		try:
			logger.info( f"{SCRIPT_NAME}: trying to install" + f" {libIn} with pip" )
			retCode= pipmain(['install', libIn])
			logger.info( f"{SCRIPT_NAME}': "+f"{libIn} installed with pythons pip module" )
		except Exception as exceLoc :
			print(exceLoc) # To be removed when I will know the name of the exception
			# If the pipmain fails use the external commands
			cmdListLoc=[sys.executable, "-m", "pip", "install", libIn]
			retCode= subprocess.check_call(cmdListLoc, shell=True)
			logger.info( f"{SCRIPT_NAME}: " +f"{libIn} installed with pip through a subprocess" )
			# logger.warning(f"{SCRIPT_NAME}: " +f"exception: {exceLoc}" )
			raise MyException(exceLoc) from exceLoc
	elif sys.argv[1]=='-c' or sys.argv[2]=='-c':
		try:
			logger.info( f"{SCRIPT_NAME}: trying to install "+f"{libIn} with conda" )
			listlibIn=['install', '-y']+libIn.split() #['conda', 'install', '-y']
			retCode= conda.cli.main(*listlibIn)
			# Use the -y argument to avoid interactive questions
			logger.info( f"{SCRIPT_NAME}: "+f"{libIn} installed with pythons conda module" )
		except Exception as exceLoc :
			print(exceLoc) # To be removed when I will know the name of the exception
			# If the conda fails use the external commands
			cmdListLoc=['conda', 'install', '-y', libIn]
			retCode= subprocess.check_call(cmdListLoc, shell=True)
			logger.info( f"{SCRIPT_NAME}: "+ f"{libIn} installed with conda through a subprocess" )
			# logger.warning(f"{SCRIPT_NAME}: " +f"exception: {exceLoc}" )
			raise MyException(exceLoc) from exceLoc
	return retCode
# end function


def str2bool(strIn):
	""" str2bool(v)

		Description:
			x
		Args:
			strIn (string): [description]
		Returns:
			retBool (bool): [description]
	"""
	retBool=None
	if strIn.lower() in ("yes", "true", "t", "1"):
		retBool= True
	elif strIn.lower() in ("no", "false", "f", "O"):
		retBool= False
	else:
		logger.error( "str2bool: the input {strIn} is not a bool!" )
		# sys.exit(3)
	return retBool
# end function


########## MAIN #############
if __name__ == "__main__":

	# --------------------------------------------------------------------------
	# 0. Create a logging
	# --------------------------------------------------------------------------
	# import of logging lib---------------------------------------------------
	try:
		import logging 		# Library to manipulate loggers
	except ModuleNotFoundError as e :
		try2install('logging')
	except Exception as exce :
		print(exce)
		raise exce

	# Configuration of the logger---------------------------------------------------
	# Create a logger instance
	logger = logging.getLogger(__name__)
	logger.setLevel(logging.DEBUG)
	# Create and add a file management to logger
	formatter = logging.Formatter('%(asctime)s :: %(levelname)s :: %(message)s')
	fh = logging.FileHandler(logFile)
	fh.setLevel(logging.DEBUG)
	fh.setFormatter(formatter)
	logger.addHandler(fh)
	logger.info( f"{SCRIPT_NAME}: Start" ) # pylint: disable=logging-fstring-interpolation
	# Create and add a terminal management to logger
	stream_handler = logging.StreamHandler()
	stream_handler.setLevel(logging.DEBUG)
	logger.addHandler(stream_handler)
	logger.info( f"{SCRIPT_NAME}: python logging is set" ) # pylint: disable=logging-fstring-interpolation

	# ------------------------------------------------------------------------------
	# 1. Verify version of Python and import Subprocess for planB of installation.
	# ------------------------------------------------------------------------------
	# ----- Check for Python version
	if sys.hexversion < 0x020600F0:
		# Already installed in the install_debian.sh
		logger.error( f"{PROJECT_NAME} requires Python "+f"{3.5} or higher, sorry !" )
		sys.exit(2)

	# ----- import Subprocess lib for planB in the 'try2install' function
	try:
		import subprocess 		# Library to manipulate loggers
	except ModuleNotFoundError as e :
		try2install('subprocess')
	except Exception as exce :
		print(exce)
		raise exce

	# ------------------------------------------------------------------------------
	# 2. Installation of Python libs
	# ------------------------------------------------------------------------------
	# ----- Read the Libs to install from the file 'listLibFile'.
	try:
		ListOfFile=__import__(listLibFile[:-3])
	except (ImportError, KeyError):
		logger.error( f"File not found or invalid : {listLibFile}" ) # pylint: disable=logging-fstring-interpolation
		sys.exit(1)
	except Exception as exce :
		logger.error(exce)
		raise exce

	# Ask for option of installation like Windows, GUI, developers, test after install_PROJECT_NAME
	# pylint: disable=invalid-name
	flagWindows=False
	flagGUI=False
	flagDev=False
	flagTestInstall=False
	flagExtract=False
	givenKey='a'
	conditionKey=True
	# pylint: enable=invalid-name

	if flagFile :
		try:
			ConfigFile=__import__("config_install")
			flagWindows=str2bool(ConfigFile.WindowsOS)
			flagGUI=str2bool(ConfigFile.GUI)
			flagDev=str2bool(ConfigFile.DevEnv)
			flagTestInstall=str2bool(ConfigFile.TestAfterInstall)
			flagExtract=str2bool(ConfigFile.ArchiveExtraction)
			path2install=ConfigFile.PathExtration
		except (ImportError, KeyError):
			logger.error( "File not found or invalid: 'config_install'" )
			sys.exit(1)
		except Exception as exce :
			logger.error(exce)
			raise exce
			# sys.exit(1)
	else:
		while conditionKey:
			givenKey=input( f"""The default options for the installation are:
			[w] Windows OS={flagWindows}
			[g] GUI={flagGUI}
			[d] Developers={flagDev}
			[t] Test the installation after its end={flagTestInstall}
			[e] Extract sources from the archive file (False if download from Git)={flagExtract}
			Press a the associate key to switch the value, the [Y/n] to validate or quit. 
			""" )
			print( "Received key={givenKey}" )
			# pylint: disable=invalid-name
			if givenKey.lower()=='y':
				conditionKey=False
			elif givenKey.lower()=='n':
				conditionKey=False
				logger.info( f"{SCRIPT_NAME}: Key received to quit" ) # pylint: disable=logging-fstring-interpolation
				sys.exit(0)
			elif givenKey.lower()=='d':
				flagDev=not flagDev
			elif givenKey.lower()=='e':
				flagExtract= not flagExtract
			elif givenKey.lower()=='g':
				flagGUI= not flagGUI
			elif givenKey.lower()=='t':
				flagTestInstall= not flagTestInstall
			elif givenKey.lower()=='w':
				flagWindows= not flagWindows
			else :
				print('Unknown key, please press only keys in the detailed possibilities above.')
			# pylint: enable=invalid-name


	logger.info( f"{SCRIPT_NAME}: beginning of installation for the list of " \
		+ f"libraries with the option:  Windows OS={flagWindows}, GUI={flagGUI}," \
		+ f" Developers={flagDev}, Tests={flagTestInstall}, extract={flagExtract}." )

	# ----- Libs installation
	for lib in ListOfFile.lib_to_install:
		try:
			try2install(lib)
			# print(lib)
		except Exception as exce :
			logger.error(exce)
			raise exce
	# For Windows
	if flagWindows:
		for lib in ListOfFile.lib_windows:
			try:
				try2install(lib)
				# print(lib)
			except Exception as exce :
				logger.error(exce)
				raise exce
	# GUI
	if flagGUI:
		for lib in ListOfFile.lib_GUI:
			try:
				try2install(lib)
				# print(lib)
			except Exception as exce :
				logger.error(exce)
				raise exce
	# Dev
	if flagDev:
		for lib in ListOfFile.lib_dev:
			try:
				try2install(lib)
				# print(lib)
			except Exception as exce :
				logger.error(exce)
				raise exce

	# ------------------------------------------------------------------------------
 	# 3. Unpacking the Compressed Source folder & install extra tools
	# ------------------------------------------------------------------------------
	if flagExtract:
		# ----- Unpack
		import shutil 	# Library for High-level file operations

		if not flagFile:
			conditionKey=True # pylint: disable=invalid-name
			while conditionKey:
				givenKey=input( f""" Where do you want to extract/install the {PROJECT_NAME} files ?
					The current selected path is:{path2install}
					Press [Y/n] to validate or quit, or the new path to {PROJECT_NAME} installation.""" )
				print( f"Received key={givenKey}" )
				if givenKey.lower()=='y':
					conditionKey=False # pylint: disable=invalid-name
				elif givenKey.lower()=='n':
					conditionKey=False # pylint: disable=invalid-name
					logger.info( f"{SCRIPT_NAME}: Key received to quit" ) # pylint: disable=logging-fstring-interpolation
					sys.exit(0)
				else :
					if os.path.isdir(givenKey) and os.path.exists(givenKey):
						path2install=givenKey
					print('Unknown key, please press only keys in the detailed possibilities above.')

		logger.info( f"{SCRIPT_NAME}: beginning of installation for the list of libraries with "\
			+f"the option:  Windows OS={flagWindows}, GUI={flagGUI}, Developers={flagDev}, "\
			+f"Tests={flagTestInstall}, extract={flagExtract}." )


		try :
			listDirs=os.listdir('./')
			sourceFinal=[]
			for file in listDirs:
				if file.startswith(sourceCompressed) and file.endswith(sourceCompressedExt):
					sourceFinal.append(file)
			if len(sourceFinal)>1:
				logger.error( f"{SCRIPT_NAME}: Multiple archive file found" ) # pylint: disable=logging-fstring-interpolation
				logger.error( f"{SCRIPT_NAME}: file " +f"found {sourceFinal}" )
				sys.exit(2)
			if len(sourceFinal)==0:
				logger.error( f"{SCRIPT_NAME}: No archive file found in "+ f"folder: {os.getcwd()}")
				logger.error( f"{SCRIPT_NAME}: archive file should have the following pattern name: "\
					+f"startswith {sourceCompressed} and endswith {sourceCompressedExt}" )
				logger.error( f"{SCRIPT_NAME}: list of files " +f"found: {listDirs}" )
				sys.exit(2)
			logger.info( f"{SCRIPT_NAME}: Archive " +f"{sourceFinal[0]} found." )
			shutil.unpack_archive(sourceFinal[0], extract_dir=None,)
			logger.info( f"{SCRIPT_NAME}: " +f"{sourceFinal[0]} unpacked" )
			os.remove(sourceFinal[0])
			logger.info( f"{SCRIPT_NAME}: " +f"{sourceFinal[0]} removed" )
		except Exception as exce:
			logger.info(exce)
			raise exce

		# ----- To move installation objects
		logger.info( f"{SCRIPT_NAME}: move installation files "+f"into {path2install}/install/" )
		for f in ListOfFile.install_files:
			try:
				shutil.move(pathInit +'/'+ f, path2install + '/install/'+ f)
			except Exception as exce:
				logger.warning(exce)
				raise exce

		for f in ListOfFile.install_folders:
			try:
				shutil.rmtree(f)
			except Exception as exce:
				logger.warning(exce)
				raise exce

		# ----- To make the launcher executable
		os.chdir(path2install)
		for f in listFile2MakeExecutable :
			os.chmod(f, 0o774)

		for f in os.listdir(path2install+'/bin'):
			os.chmod(path2install+'/bin/'+f, 0o774)

	else : # if there is zip file to extract
		# Change the path path2install as the parent directory
		path2install=os.path.dirname(pathInit)

	# --------------------------------------------------------------------------
	# 4. Eventually test the resulting installation.
	# --------------------------------------------------------------------------
	if flagTestInstall:
		os.chdir(path2install+'/bin')
		logger.info( f"{SCRIPT_NAME}: test the installation." ) # pylint: disable=logging-fstring-interpolation
		if sys.platform.startswith('win'):
			shellOption=True # pylint: disable=invalid-name
		elif sys.platform.startswith('linux'):
			shellOption=False # pylint: disable=invalid-name
		else :
			shellOption=None # pylint: disable=invalid-name
			print('[ERROR]     OS not available... EXIT')
		cmdList=[sys.executable, './batch_'+PROJECT_NAME+'.py', '-i',
			'../configs/batch_conf_PTC_install.py' ]
		logger.info( f"{SCRIPT_NAME}: " +f"subprocess.check_call({cmdList})" )
		subprocess.check_call(cmdList, shell=shellOption)

	# Log the end & end of the script
	# pylint: disable=logging-fstring-interpolation
	if sys.platform.startswith('win'):
		logger.info( f"{SCRIPT_NAME}: Since the installer.bat was moved, at the end"\
			f" of this script, the message 'The batch file cannot be found.' is normal."\
			f" Do not pay attention to it." )
	logger.info( f"{SCRIPT_NAME}: End")
	# pylint: enable=logging-fstring-interpolation
	sys.exit(0)

# EoF
