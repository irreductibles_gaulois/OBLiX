:: # Script of installation of the CXS suite for Windows >= XP(2001).

:: # Turn off the display to show only the message on a clean line.
@ECHO OFF
SETLOCAL EnableDelayedExpansion

:: # Create a log file.
:: # script global variables
SET me=%~n0
SET log=./%me%.log
:: # if the file exist, remove it and create a new one
IF EXIST "%log%" (DEL "%log%")

ECHO Install_CXS_Win.bat: Start.
CALL :tee %me%: Start with log.

:: # Ask if the Python libs can be install through Conda or Pip or exit
::echo toto :: dont ask me why but if I delete this line it create a buggus.
ECHO %me%: Do you want to install Python libraries through a Conda environment (recommanded) or in the Python distribution of the system with PIP or exit ? (Y/P/n) >> "%log%"
SET /P optionA=%me%: Do you want to install Python libraries through a Conda environment (recommanded) or in the Python distribution of the system with PIP or exit ? (Y/P/n)
CALL :tee %me%: Answer recieved %optionA%
::echo toto :: dont ask me why but if I delete this line it create a buggus.


IF /I "%optionA%"=="Y" goto :AnswerAConda
IF /I "%optionA%"=="P" goto :AnswerAPip
IF /I "%optionA%"=="n" goto :AnswerNo
goto :AnswerUnknown

:AnswerAConda
   :: CALL :tee %me%: "goto AnswerAConda"
   SET "FoundConda=False"
   WHERE conda >NUL 2>NUL && SET "FoundConda=True"

   IF "%FoundConda%"=="False" (
      ::echo toto :: dont ask me why but if I delete this line it create a buggus.
      ECHO %me%: "The Conda command is not found, do you want to install it (recommanded) or do you prefer to exit ? (Y/n)"  >> "%log%"
      SET /P optionB="%me%: The Conda command is not found, do you want to install it (recommanded) or do you prefer to exit ? (Y/n)"
      CALL :tee %me%: Answer recieved !optionB!

      IF /I "!optionB!"=="Y" goto :AnswerBConda
      IF /I "!optionB!"=="n" goto :AnswerNo
      goto :AnswerUnknown
   ) ELSE (
      CALL :tee %me%: "Conda is already installed".
      goto :InstallLibsConda
   )
goto :endofscript


:AnswerAPip
   CALL :tee %me%: Is it Pip installed ?
   CALL :tee %me%: Do you want to install it ?
goto :InstallLibsPip
goto :endofscript


:AnswerBCondaOLD
ECHO AnswerBCondaOLD
   :: ######## Get the link to download.
   :: # Download the page with DYNAMIC option because it is not a file.
   BITSADMIN /TRANSFER myDownloadAnacondaPage /DYNAMIC  https://www.anaconda.com/distribution "%CD%\anaconda.txt"
   :: # reformate to deal with end lind characters
   TYPE anaconda.txt | MORE /P >anaconda-crlf.txt
   :: # Find the line containing a special pattern and store it in a variable
   :: FINDSTR /R "^.*https://repo.*Anaconda3.*x86_64\.exe.*>64-Bit.*$" anaconda-crlf.txt
   FOR /F "tokens=* USEBACKQ" %%g IN (`FINDSTR /R "^.*https://repo.*Anaconda3.*x86_64\.exe.*>64-Bit.*$" anaconda-crlf.txt`) DO (set "longString=%%g")
   CALL :tee %me%: The longString found is
   CALL :tee %me%: "%longString%"

      :: ### Extract the subtring of the previous line containing the link according to the tokens keywords.
      SETLOCAL EnableDelayedExpansion
      :: # Define subtrings token
      :: Set "subsA=href="" is more complicated so I will remove the extra characters after
      SET "subsA=a href"
      SET "subsB=>64-Bit Graphical"
      :: # Remove part before subsA
      SET "Result=!longString:*%subsA%=!"
      SET "Result=!result:~2!"
      ::SET result
      :: # Get the part after subsB and count this length
      SET "remove=!result:*%subsB%=!"
      ::SET remove
      CALL :strlen remove_len remove
      CALL :strlen subsB_len subsB
      SET /A remove_len+=subsB_len+1
      SET "result=!result:~0,-%remove_len%!"
      :: # Remove the trailling and preceeding spaces.
      SET CondaLink=!result: =!
      ::ECHO !CondaLink!
      CALL :tee %me%: The download link found is !result!
   :: Cleaning Temporary files.
   DEL "%CD%\anaconda-crlf.txt"
   DEL "%CD%\anaconda.txt"

   :: ######## Download the file
   :: # bitsadmin /transfer myDownloadJob /download /priority normal http://downloadsrv/10mb.zip c:\10mb.zip
   :: # https://repo.anaconda.com/archive/Anaconda3-2019.10-Windows-x86_64.exe
   BITSADMIN /TRANSFER myDownloadJob /DOWNLOAD /PRIORITY NORMAL %CondaLink% %UserProfile%\Downloads\anaconda3_windows.exe

   :: ######## Install Anaconda
   CALL :tee %me%: Installation of Conda in silent mode.
   START /WAIT "" %UserProfile%\Downloads\anaconda3_windows.exe /InstallationType=JustMe /RegisterPython=0 /S /D=%UserProfile%\Miniconda3

   :: ######## Set the environment variable permanently for the User !!!
   CALL :tee %me%: set the environment variable.
   setx PATH "%PATH%;%UserProfile%\AppData\Local\Continuum\anaconda3\;%UserProfile%\AppData\Local\Continuum\anaconda3\Scripts\"

   :: # Cleaning the anaconda file
   DEL "%UserProfile%\Downloads\anaconda3_windows.exe"
goto :InstallLibsConda


:AnswerBConda
ECHO AnswerBConda
   :: # Get the architecture
   SET archSuffix=unknown
   IF /I "%PROCESSOR_ARCHITECTURE%"=="AMD64" SET archSuffix=x86_64.exe
   IF /I "%PROCESSOR_ARCHITECTURE%"=="AMD32" SET archSuffix=x86.exe
   IF /I "%PROCESSOR_ARCHITECTURE%"=="x86" SET archSuffix=x86.exe
   IF "%PROCESSOR_ARCHITECTURE%"=="unknown" (
      CALL :tee %me%: PROCESSOR_ARCHITECTURE is not recognized by the script, so it quits
      EXIT /B 2
   )

   :: # Set miniconda link
   SET "CondaLink=https://repo.anaconda.com/miniconda/Miniconda3-latest-Windows-%archSuffix%"

   :: ######## Download the file
   :: # bitsadmin /transfer myDownloadJob /download /priority normal http://downloadsrv/10mb.zip c:\10mb.zip
   :: # https://repo.anaconda.com/archive/Anaconda3-2019.10-Windows-x86_64.exe
   BITSADMIN /TRANSFER myDownloadJob /DOWNLOAD /PRIORITY NORMAL %CondaLink% %UserProfile%\Downloads\miniconda3_windows.exe

   :: ######## Install Anaconda
   CALL :tee %me%: Installation of Conda in silent mode.
   START /WAIT "" %UserProfile%\Downloads\miniconda3_windows.exe /InstallationType=JustMe /RegisterPython=0 /S /D=%UserProfile%\AppData\Local\Continuum\miniconda3

   :: ######## Set the environment variable permanently for the User !!!
   CALL :tee %me%: set the environment variable.
   for /F "tokens=2* delims= " %%f IN ('reg query HKCU\Environment /v PATH ^| findstr /i path') do set OLD_SYSTEM_PATH=%%g
   setx PATH "%OLD_SYSTEM_PATH%;%UserProfile%\AppData\Local\Continuum\miniconda3\;%UserProfile%\AppData\Local\Continuum\miniconda3\Scripts\"
   REM setx PATH "%PATH%;%UserProfile%\AppData\Local\Continuum\miniconda3\;%UserProfile%\AppData\Local\Continuum\miniconda3\Scripts\"
   echo %PATH%
   :: ######## Set the environment variable for the User for the following of the script
   set "PATH=%PATH%;%UserProfile%\AppData\Local\Continuum\miniconda3\;%UserProfile%\AppData\Local\Continuum\miniconda3\Scripts\"
   :: # Cleaning the anaconda file
   DEL "%UserProfile%\Downloads\miniconda3_windows.exe"
goto :InstallLibsConda


:InstallLibsConda
   :: ######## To activate
   CALL :tee %me%: Activation of conda environment
   call activate.bat
   (call conda info && CALL :tee %me%: Conda is correctly set) || (CALL :tee %me%:Conda is NOT correctly set: byebye && exit /B 1)

   :: Update
   CALL :tee %me%: update of conda.
   call conda update --yes conda

   :: ######## To create the CXS environment
   CALL :tee %me%: Creation of conda CXS environment
   call conda create --name CXS -y
   CALL :tee %me%: conda activate CXS
   call conda activate CXS

   :: # installing basic tools
   CALL :tee %me%: install conda.API for python
   call conda install -y conda
   REM CALL :tee %me%: add conda-forge
   REM call conda config --add channels conda-forge

   :: # Launch the python script that install depencies with conda.
   CALL :tee %me%: "Execute the python script to install all depedencies: python3 ./CXS_libs2install.py -c"
   START /B /WAIT "" python ./CXS_libs2install.py -c
   CALL :tee %me%: "python ./CXS_libs2install.py -c return with code %ERRORLEVEL%"
GOTO :endofscript


:InstallLibsPip
   ECHO InstallLibsPIP
   CALL :tee %me%: "Execute the python script to install all depedencies: python3 ./CXS_libs2install.py -p"
   :: Do something
GOTO :endofscript

:: # Just in case
GOTO :endofscript


:: ############ Aborted Options ###################
:AnswerNo
CALL :tee %me%: I understand that you want to quit.
GOTO :endofscript

:AnswerUnknown
CALL :tee %me%: "Unvalid Answer"
ECHO "Wait 5s"
ping 127.0.0.1 -n 6 >NUL
GOTO :endofscript

:endofscript
CALL :tee %me%: Installation of CXS Windows: End.
ECHO End of the script
ENDLOCAL
EXIT /B

:: ############ FUNCTIONS ###################
:: # a function to write to a log file and write to stdout
:tee
ECHO %* >> "%log%"
ECHO %*
EXIT /B 0

:: # a function to count the lenght of a string
:strlen <resultVar> <stringVar>
(
    SETLOCAL ENABLEDELAYEDEXPANSION
    (SET^ tmp=!%~2!)
    IF defined tmp (
        SET "len=1"
        FOR %%P IN (4096 2048 1024 512 256 128 64 32 16 8 4 2 1) DO (
            IF "!tmp:~%%P,1!" NEQ "" (
                SET /a "len+=%%P"
                SET "tmp=!tmp:~%%P!"
            )
        )
    ) ELSE (
        SET len=0
    )
)
(
    ENDLOCAL
    SET "%~1=%len%"
    EXIT /B
)
::EoF
