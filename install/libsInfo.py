#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# pylint: disable=invalid-name
"""
	NAME : libsInfo.py

	DESCRIPTION :
		This module contains the libraries required and the name of
		installation objects.

	LICENSE :
		Copyright (C) 2016-20** Romain NOEL
"""
# pylint: enable=invalid-name

PROJECT_NAME='OBLiX'

# libs required, by alphabetic order
lib_to_install = [
	'vtk',
	#'argcomplete', # not really necessary
	#'argparse', # installed by default
	#'asyncio', # installed by default
	#'conda', # Should be installed before #####
	#'csv', # installed by default
	#'ctypes', # installed by default
	#'datetime', # installed by default
	#'inspect', # installed by default
	#'logging', # Should be installed before #####
	#'math', # installed by default
	#'multiprocessing', # installed by default
	#'os', # installed by default
	#'pip', # installed by default
	#'re', # installed by default
	#'shutil', # installed by default
	#'signals', # installed by default
	#'six', # installed by default
	#'subprocess', # Should be install before #####
	#'sys', # installed by default
	#'uuid', # not used actually.
	#'xml', # installed by default
]

# libs required, by alphabetic order
lib_windows = [
	# 'pythoncom',
	# 'win32com',
]

# libs required, by alphabetic order
lib_GUI = [
	# 'pyqt', # 'pyqt5'
	# 'pyqtgraph', #
]

# libs required, by alphabetic order
lib_dev = [
	# 'mock',
	'pylint', #
	'pytest', #
	'pytest-cov', #
	# '-c conda-forge pytest-qt', # TO INSTALL : conda install -c conda-forge pytest-qt
	# 'warnings', # installed by default
]

# Installation objects
install_files = [
	'libsPy2install.py',
	'libsInfo.py',
	'install_'+PROJECT_NAME+'_unix.sh',
	'addPackages.sh',
	'addPythonEnv.sh',
	'install_'+PROJECT_NAME+'_Win.bat',
	#'install_'+PROJECT_NAME*'.log',
	'INSTALL.ReadMe'
]
