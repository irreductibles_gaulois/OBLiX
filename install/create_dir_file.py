#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
	NAME: create_dir_file.py

	DESCRIPTION:
		Create a zip archive of the project folder.
		This project folder should contains a bin folder (used for recognition)
		and be the parent folder of the folder containing this script.
		The script should be run from its containing folder.
	Returns:
		[type]: [description]
	USAGE:
		$ ls ./
			./ ../ bin/ ...
		$ cd ./install
		$ ls ./
			./ ../ create_dir_file.py ...
		$ python3 ./create_dir_file.py
"""

# Import of Libraries-----------------------------------------------------------
import sys                   # Library for System-specific parameters and functions
import os                    # Library for Operating System
import shutil                # Library for High-level file operations

# Find chRootProject path.
# pylint: disable=no-else-break
_PREV_PATH = None
_CALLING_DIR = os.getcwd()
while True :
	if (os.path.exists('./OBLiX-README.md')) and (os.getcwd()!='/'):
		CHROOT_PATH=os.getcwd()
		break
	elif _PREV_PATH==os.getcwd() :
		print('ERROR: the chRootProject folder was not found...')
		os.chdir(_CALLING_DIR)
		sys.exit(3)
		break
	else:
		_PREV_PATH=os.getcwd()
		os.chdir('../')
os.chdir(_CALLING_DIR) # Go back to the call directory.
del _PREV_PATH
# del _callingDir
# pylint: enable=no-else-break

# Initialization of primary variables-------------------------------------------
AUTHORS_NAME=["Romain NOËL"]
VERSION_NUM='0.0.2'
VERSION_DATE='03-Dec-2019'
PROJECT_NAME="OBLiX"
SCRIPT_NAME="create_dir_file"
SCRIPT_DESCRIPTION='script python to .'

LIST_LIB_FILE='libsInfo.py'
ARCHIVE_NAME='/DirFile2Install'
DIR_FILE_PATH= os.path.abspath(os.path.join(CHROOT_PATH, os.pardir))

################################ 	FUNCTIONS 	########################################

def run():
	"""
		Description:
			x
		Args:
			x
		Returns:
			x
		Raises:
			x
		Examples:
			x
		Remarks: (refs, notes, ToDo, etc.)
			x
	"""

	# say hello
	print(SCRIPT_NAME+': Start')
	retCode=-1

	# Initialize path variables
	oldInstallDir= CHROOT_PATH+'/install'
	newInstallFolder= DIR_FILE_PATH+'/Install'
	nameVersion= os.path.basename(CHROOT_PATH)
	archiveNameVersion= ARCHIVE_NAME +'-' +nameVersion
	newInstallFolder= newInstallFolder +'-' +nameVersion

	# Import list of file to keep for the installation.
	print( SCRIPT_NAME+': Import list of file to keep for the installation.' )
	try:
		sys.path.append(oldInstallDir)
		listOfFiles=__import__(LIST_LIB_FILE[:-3])
	except ImportError as exce:
		print( f"[WARNing] import {LIST_LIB_FILE} fails" )
		print(exce)

	# Copy installation file.
	print( SCRIPT_NAME+': Create the destination directory.' )
	os.makedirs(newInstallFolder, exist_ok=True)
	print( SCRIPT_NAME+': Copying the installation files.' )
	try:
		for file in listOfFiles.install_files:
			#print(os.path.exists(oldInstallDir+'/'+file), file)
			if file == 'install_'+PROJECT_NAME+'.log' and \
				os.path.exists(oldInstallDir+'/'+file) is False :
				pass
			else:
				# shutil.move( source, destination )
				shutil.copy2(oldInstallDir+'/'+file, newInstallFolder+'/'+file)
				print( f"{SCRIPT_NAME}: Copy of file {file} is done." )
	except FileNotFoundError as exce:
		# It can happen, specially if install_PROJECT_NAME.log is not present.
		if file == 'install_'+PROJECT_NAME+'.log' :
			pass
		else :
			print(exce)
	except Exception as exce:
		print(exce)
		raise exce

	# Creation of the Archive file.
	os.chdir(CHROOT_PATH)
	outputFilename= newInstallFolder+archiveNameVersion
	dirName= CHROOT_PATH
	print( f"{SCRIPT_NAME}: creating an archive {dirName} -> {outputFilename}" )
	shutil.make_archive(outputFilename, 'zip', dirName)

	print(SCRIPT_NAME+': End')
	return retCode
# end function


################################ 	MAIN	 	########################################
if __name__ == "__main__":
	#
	retCodeMain= run() # pylint: disable=invalid-name

	sys.exit(retCodeMain)

# EoF
