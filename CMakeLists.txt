cmake_minimum_required (VERSION 3.20)

#------------------------------------------------------------------------------
# Project info

# Set parameters of the Project
set(PROJECT_NAME OBLiX)
set(PROJECT_VERSION 1.0.1.1)
set("PROJECT_DESCRIPTION" "A header-only C++ library for execution of object from a collection.")
set("PROJECT_HOMEPAGE_URL" "https://gitlab.com/irreductibles_gaulois/OBLiX")
set(PROJECT_EXECUTABLE_NAME OBLiXprog)
set(PROJECT_LIB_NAME OBLiX)
set(PROJECT_DATE "01.06.2022")

# Project setup, versioning stuff here, change when changing the version
# Note: keep the project name lower case only for easy linux packaging support
project(${PROJECT_NAME} VERSION ${PROJECT_VERSION})
site_name(VERSION_HOST) # read hostname to VERSION_HOST
set(VERSION_HOST "${VERSION_HOST}" CACHE STRING "host of build" FORCE)

message(STATUS "")
message(STATUS "    == ${PROJECT_NAME} Project configuration == " )
message(STATUS "")


#------------------------------------------------------------------------------
# General settings

# Check if this project is the master project or a slave.
if(PROJECT_SOURCE_DIR STREQUAL CMAKE_SOURCE_DIR)
    set(${PROJECT_NAME}_MASTER_PROJECT ON)
endif()

# We can use include() and find_package() for our scripts in there
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_CURRENT_SOURCE_DIR}/cmake)
include(projectSetting)


#------------------------------------------------------------------------------
# Included CMakeLists.txt

# Creates options to turn on sanitizers, see cmake/sanitizers.cmake
include(sanitizers)
addCheckingTargets()

# External resources/repositories are downloaded here
add_subdirectory(external)

# Images, databases and other data which needs to be installed for project
# add_subdirectory(data)

# Source code
add_subdirectory(src)

# binding with swig
if (${PROJECT_NAME}_SWIG_BINDING)
  add_subdirectory(bindings)
endif()

# Testing
if( ${PROJECT_NAME}_TESTING )
  enable_testing()
  if( ${PROJECT_NAME}_SWIG_BINDING )
    add_subdirectory(testing/bindings_tests)
  endif()
  if( ${PROJECT_NAME}_BUILD_HEADERS_ONLY )
    # add_subdirectory(testing/unit_tests/googletest)
    add_subdirectory(testing/unit_tests)
  endif()
  if ( ${PROJECT_NAME}_BUILD_EXECUTABLE )
    add_subdirectory(testing/integration_tests)
  endif()
endif()

# Packaging stuff (deb, rpm, windows installer)
# add_subdirectory(packaging)


# Documentation build
IF(${PROJECT_NAME}_BUILD_DOCUMENTATION)
  add_subdirectory(docs)
ENDIF(${PROJECT_NAME}_BUILD_DOCUMENTATION)

#-------------------------------------------------------------------------------
# Wrap up of settings printed on build
printRecap()
message(STATUS "")
message(STATUS "    == Final overview for ${PROJECT_NAME} == ")
message(STATUS "Version:            ${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}.${PROJECT_VERSION_PATCH}.${PROJECT_VERSION_TWEAK} @${VERSION_HOST}")
message(STATUS "Install to:         ${CMAKE_INSTALL_PREFIX}")
message(STATUS "Compiler:           ${CMAKE_CXX_COMPILER}")
message(STATUS "CMAKE_BUILD_TYPE:   '${CMAKE_BUILD_TYPE}'")
message(STATUS "  possible options: Debug Release RelWithDebInfo MinSizeRel")
message(STATUS "")

if(DISPLAY_CMAKE_VAR)
    message(STATUS "")
    message(STATUS "  ---- DISPLAY_CMAKE_VAR ---- ")
    message(STATUS " --- for Debug purpose only ---")
    message(STATUS "")
    get_cmake_property(_variableNames VARIABLES)
    foreach (_variableName ${_variableNames})
        message(STATUS "${_variableName}=${${_variableName}}")
    endforeach()
    message(STATUS "  ---- DISPLAY_CMAKE_VAR ---- ")
endif()

# EoF